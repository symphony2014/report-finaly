var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { a2tRoutes, A2tUiComponent, A2tSharedModule, } from './';
import { A2tSignInComponent } from './a2t-sign-in/a2t-sign-in.component';
import { A2tSignUpComponent } from './a2t-sign-up/a2t-sign-up.component';
import { A2tResetPasswordComponent } from './a2t-reset-password/a2t-reset-password.component';
import { A2tUpdatePasswordComponent } from './a2t-update-password/a2t-update-password.component';
var A2tUiModule = (function () {
    function A2tUiModule() {
    }
    return A2tUiModule;
}());
A2tUiModule = __decorate([
    NgModule({
        imports: [
            CommonModule,
            RouterModule,
            A2tSharedModule,
            a2tRoutes
        ],
        declarations: [
            A2tUiComponent,
            A2tSignInComponent,
            A2tSignUpComponent,
            A2tResetPasswordComponent,
            A2tUpdatePasswordComponent
        ]
    })
], A2tUiModule);
export { A2tUiModule };
//# sourceMappingURL=a2t-ui.module.js.map