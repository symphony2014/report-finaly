var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { A2tFormComponent, A2tFormFieldComponent } from './a2t-form';
import { A2tLinksComponent } from './a2t-links/a2t-links.component';
import { A2tErrorComponent } from './a2t-error/a2t-error.component';
import { A2tHeadlineComponent } from './a2t-headline/a2t-headline.component';
var A2tSharedModule = (function () {
    function A2tSharedModule() {
    }
    return A2tSharedModule;
}());
A2tSharedModule = __decorate([
    NgModule({
        imports: [
            CommonModule,
            ReactiveFormsModule,
            RouterModule
        ],
        declarations: [
            A2tFormFieldComponent,
            A2tFormComponent,
            A2tLinksComponent,
            A2tErrorComponent,
            A2tHeadlineComponent
        ],
        exports: [
            A2tFormComponent,
            A2tLinksComponent,
            A2tErrorComponent,
            A2tHeadlineComponent
        ]
    })
], A2tSharedModule);
export { A2tSharedModule };
//# sourceMappingURL=a2t-shared.module.js.map