import { NodeRemovedEvent, NodeRenamedEvent, NodeCreatedEvent, NodeSelectedEvent, NodeMovedEvent } from './tree.events';
import { RenamableNode } from './tree.types';
import { Tree } from './tree';
import { Subject, Observable } from 'rxjs/Rx';
import { ElementRef } from '@angular/core';
import { NodeDraggableService } from './draggable/node-draggable.service';
import { NodeDraggableEvent } from './draggable/draggable.events';
export declare class TreeService {
    private nodeDraggableService;
    nodeMoved$: Subject<NodeMovedEvent>;
    nodeRemoved$: Subject<NodeRemovedEvent>;
    nodeRenamed$: Subject<NodeRenamedEvent>;
    nodeCreated$: Subject<NodeCreatedEvent>;
    nodeSelected$: Subject<NodeSelectedEvent>;
    constructor(nodeDraggableService: NodeDraggableService);
    unselectStream(tree: Tree): Observable<any>;
    fireNodeRemoved(tree: Tree): void;
    fireNodeCreated(tree: Tree): void;
    fireNodeSelected(tree: Tree): void;
    fireNodeRenamed(oldValue: RenamableNode | string, tree: Tree): void;
    fireNodeMoved(tree: Tree, parent: Tree): void;
    draggedStream(tree: Tree, element: ElementRef): Observable<NodeDraggableEvent>;
}
