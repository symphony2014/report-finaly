/**
 * @file Common util methods.
 */
define_esl(function (require_esl) {

    var lib = {
        Set: require_esl('./lib/Set')
    };

    extend(
        lib,

        // helpers
        require_esl('./lib/base'),
        require_esl('./lib/json'),
        require_esl('./lib/dataDriven'),
        require_esl('./lib/objectAccess'),
        require_esl('./lib/objectOriented'),
        require_esl('./lib/model'),
        require_esl('./lib/event'),
        require_esl('./lib/enumeration'),
        require_esl('./lib/disable'),
        require_esl('./lib/number'),
        require_esl('./lib/throttle'),
        require_esl('./lib/htmlCleaner'),
        require_esl('./lib/codeStringify'),
        require_esl('./lib/others'),
        require_esl('./ui/tooltip')
    );

    /**
     * @inner
     * @throws {Error} If key duplicate
     */
    function extend(target) {
        for (var i = 1, len = arguments.length; i < len; i++) {
            var source = arguments[i];
            for (var key in source) {
                if (source.hasOwnProperty(key)) {
                    if (target[key]) {
                        // Check duplicate
                        throw new Error('Duplicate key: ' + key);
                    }
                    target[key] = source[key];
                }
            }
        }

        return target;
    }

    return lib;
});
