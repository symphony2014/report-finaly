const { root } = require('./helpers');
const path = require('path');
const { AotPlugin } = require('@ngtools/webpack');
const webpackMerge = require('webpack-merge'); // used to merge webpack configs
const commonConfig = require('./webpack.common.js'); // the settings that are common to prod and devprod
const { getAotPlugin } = require('./webpack.aot');
/**
 * This is a server config which should be merged on top of common config
 */
const ENV = process.env.ENV = process.env.NODE_ENV = 'server';
module.exports = {
    devtool: 'source-map',
    resolve: {
        extensions: ['.ts', '.js']
    },
    module: {
        rules: [
         { test: /\.ts$/, loader: '@ngtools/webpack' },
        { test: /\.css$/, loader: ['to-string-loader', 'css-loader'] },
        { test: /\.html$/, loader: 'html-loader' },
        { test: /\.scss$/, loaders: ['to-string-loader', 'css-loader', 'sass-loader'] },
        {
          test: /\.(jpg|png|gif)$/,
          use: 'file-loader'
        },
        { test: /\.(woff2?|ttf|eot|svg)$/, loader: 'url-loader?limit=10000' }
        ]
    },
      plugins: [
        new AotPlugin({
            tsConfigPath: './tsconfig.json',
            entryModule: path.join(__dirname, '/src/app/app.module.server#AppModule'),
            exclude: ['./**/*.browser.ts']
        })
  ],
    entry: {
        'main-server': root('./src/boot.server.ts')
    },
    output: {
        libraryTarget: 'commonjs',
        path: root('./server-dist'),
        filename: '[name].bundle.js',
    },
    target: 'node'
};
