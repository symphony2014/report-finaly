// Look in ./config folder for webpack.dev.js
switch (process.env.NODE_ENV) {
   case 'server':
    module.exports = require('./config/webpack.server')({env: 'server'});
    break;
  case 'prod':
  case 'production':
    module.exports = require('./config/webpack.prod')({env: 'production'});
    break;
  case 'dev':
  case 'development':
  default:
    module.exports = require('./config/webpack.dev')({env: 'development'});
}
