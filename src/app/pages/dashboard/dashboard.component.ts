import { Component, OnInit, OnChanges, DoCheck, AfterContentInit, AfterViewInit, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, Params, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot,NavigationEnd } from '@angular/router';

import {TemplateInfo,ComponentInfo,ResultDefaultData,ModuleType}  from '../../services/models/IpsosReport.ModelBiz';
import { ComponentService, LocalStorageService, BroadcastService } from '../../services/angular.service';

import { BaThemeSpinner } from '../../theme/services';
import { PageBase } from "../../../app/pages/page.base";
import { ComponentStyleType } from "../../../app/services/models";
import { Observable, Subscription, Subject, ReplaySubject, BehaviorSubject } from "rxjs";
import { ImageUtil } from "app/services/util.service";
import { FilterStartEvent, ComponentEvent } from "app/theme/components/ipsosComponent/component.event";
import { GlobalVars } from "app/services/global.vars";
@Component({
  selector: 'dashboard',
  styleUrls: ['./dashboard.scss'],
  templateUrl: './dashboard.html'
})
export class Dashboard  implements OnInit  {
 
  pjId:string;
  components:ReplaySubject<ComponentInfo[]>=new ReplaySubject<ComponentInfo[]>();  
  blockInPipe:BehaviorSubject<ComponentEvent>=new BehaviorSubject<ComponentEvent>(null);
  blockOutPipe:BehaviorSubject<ComponentEvent>=new BehaviorSubject<ComponentEvent>(null);
  chartEvent:BehaviorSubject<any>=new BehaviorSubject<any>(null);
  filterStarted:BehaviorSubject<boolean>=new BehaviorSubject<boolean>(false);
  config:any={OK:"确认",Reset:"重置"};
  moduleId:string;    
  moduleTitle:string;
  constructor(public _spinner: BaThemeSpinner,public route:ActivatedRoute,public router:Router,public componentService:ComponentService,public local:LocalStorageService){
  }
  ngOnInit(): void {
    this.route.params
    .do(()=>{
      this._spinner.show();
      this.filterStarted.next(false);
      
      //bad pattern
      //防止页面刷新后直接开始请求数据
      this.blockInPipe=new BehaviorSubject<ComponentEvent>(null);
      this.blockOutPipe=new BehaviorSubject<ComponentEvent>(null);
    })
    .switchMap(params=>this.componentService.GetComponentsNew(params.pjId,params.id))
    .subscribe(data=>{
      this.components.next(data);
      this.blockInPipe.asObservable()
      .filter(e=>!!e && e instanceof FilterStartEvent)
      .subscribe(_=>this.filterStarted.next(true));
      this._spinner.hide();
      this.moduleTitle=this.local.get("moduleTitle");
    })

  }
  protected isChart(type:string){
    let charts=["Cord","Pie","Radar","Gauge"];
    return charts.some(c=>c==type);
  }
  
  protected appendGlobal(event:FilterStartEvent):FilterStartEvent{
      event.filter.Pjid=this.pjId;
      event.filter.UserId= GlobalVars.getAuth().id;
      // event.filter.AlgorithmGroupId= this.component.ComponentId;
      return event;
  }
  get chartCount(): Observable<number> {
    return this.components.asObservable().map(c=>c.filter(c=>this.isChart(c.Type as string) ).length)
  } 
  ngOnDestroy() {

 }

} 