import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { NgaModule } from '../../theme/nga.module';

import { ModifyPasswordComponent } from './modify-password.component';
import { routing }       from './modify-password.routing';

import { AlgorithmService, AlertService } from "../../../app/services/angular.service";
import { GlobalUtil } from "app/services/util.service";
import { FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    routing,
  ],
  declarations: [
    ModifyPasswordComponent,
  ],
  providers:[
    AlgorithmService,
    AlertService,
    GlobalUtil
  ]
})
export class ModifyPasswordModule {

}
