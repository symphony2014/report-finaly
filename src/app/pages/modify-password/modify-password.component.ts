import { Component, OnInit,Input } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { ElementRef } from '@angular/core';
import { AuthService } from 'app/services/angular.service';
import { GlobalVars } from 'app/services/global.vars';
import { Observable } from 'rxjs/Observable';
import { BaThemeSpinner } from 'app/theme/services';


@Component({
  selector: 'app-modify-password',
  templateUrl: './modify-password.component.html',
  styleUrls: ['./modify-password.component.scss']
})
export class ModifyPasswordComponent implements OnInit, OnDestroy {
  @Input() quit = 'none';
  oldPassword:string;
  newPassword:string;
  renewPassword:string;
  equal:boolean=false;
  
  message:Observable<any>;

	onquit(v){
		console.log(v)
		this.quit = v;
	}
  constructor(public _spinner: BaThemeSpinner,private er : ElementRef,private authService:AuthService) { }

  ngOnInit() {
  }
  ngOnDestroy() {
    //do nothing
  }
  show(e) {
    let div = e.target.parentNode;
    let l = div.children.length - 1;
    div = div.children[l];
    if (div.innerHTML) {
      div.style.visibility = 'visible';
    }
  }
  hidden(e) {
    let div = e.target.parentNode;
    let l = div.children.length - 1;
    div.children[l].style.visibility = 'hidden';
    e.target.focus();
  }
  hiddenSelf(e) {
    e.target.style.visibility = 'hidden';
    e.target.parentNode.children[1].focus();
  }
  valueChange(e) {
    let div = e.target.parentNode;
    let s = e.target.placeholder + '!';
    let l = div.children.length - 1;
    div = div.children[l];
    if (e.target.value) {
      div.innerHTML = '';
    } else {
      div.innerHTML = s;
    }
  }
 
  submit(e) {
    if(this.newPassword!=this.renewPassword)
    {
      this.equal=false;
      return ;
    }else{
      this.equal=true;
    }

    var userId=GlobalVars.getAuth().id;
    this._spinner.show();
    this.message= this.authService.UpdatePassword(userId,this.oldPassword,this.newPassword)
    .publish().refCount();
    this.message.subscribe(_=>this._spinner.hide());
  }
}
