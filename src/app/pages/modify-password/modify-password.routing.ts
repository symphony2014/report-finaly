import { Routes, RouterModule }  from '@angular/router';

import { ModifyPasswordComponent} from './modify-password.component';
import { ModuleWithProviders } from '@angular/core';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
  {
    path: '',
    component:ModifyPasswordComponent 
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
