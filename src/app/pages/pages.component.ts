import { Component } from '@angular/core';
import { Routes, Router, ActivatedRoute } from '@angular/router';
import { BaMenuService } from '../theme';
import { MENU } from '../app.menu';
import { LocalStorageService } from "app/services/angular.service";

@Component({
  selector: 'pages',
  styleUrls:["./pages.scss"],
  template: `
   <ba-top-menu></ba-top-menu> 
    <div class="al-main">
      <div class="al-content">
        <router-outlet></router-outlet>
      </div>
    </div>
    <app-footer></app-footer>
    <ba-back-top position="200"></ba-back-top>
    `
})
export class Pages {
 
  constructor(private route:ActivatedRoute,private router:Router,private _menuService: BaMenuService,private local:LocalStorageService) {

  }
  ngOnInit() {
    this._menuService.updateMenuByRoutes(<Routes>MENU);
  }
}
