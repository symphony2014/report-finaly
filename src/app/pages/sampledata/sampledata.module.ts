import { NgModule } from "@angular/core";
import { SampleDataComponent } from "./sampledata.component";
import { routing } from "./sampledata.routing";
import { SuggestionsModule } from "./suggestions/suggestions.module";
import { LabelModule } from "./suggestions-label/label.module";
import { SuggestionsService, SuggestionsLabelService, VariableService ,PagingService} from "../../services/angular.service";
import { PagingModule } from "../../paging"

@NgModule({
    declarations:[SampleDataComponent],
    imports:[routing, SuggestionsModule, LabelModule],
    providers: [SuggestionsService, PagingService, SuggestionsLabelService, VariableService],
})

export class SampleDataModule{

}