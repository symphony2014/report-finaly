import { NgModule } from "@angular/core";
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LabelComponent } from "./label.component";
import { RouterModule } from '@angular/router';

@NgModule({
    declarations:[LabelComponent],
    imports:[CommonModule, FormsModule,RouterModule]
})

export class LabelModule{

}