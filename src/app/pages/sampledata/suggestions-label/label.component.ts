import { Component } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { SuggestionsService, SuggestionsLabelService, VariableService} from "../../../services/angular.service";

@Component({
    selector: 'suggestions-label',
    templateUrl: './label.html',
    styleUrls:['./label.scss']
})

export class LabelComponent{

    public RespDatas: any;

    public Suggestions: any[];

    public labelWithSuggestions: any;

    public variables: any[];

    constructor(private router: ActivatedRoute, private _sService: SuggestionsService, private _slService: SuggestionsLabelService, private _vService: VariableService){

        this.router.params.switchMap((param: Params) => {
            let pid:number = param['pid']
            let rid: number = param['rid'];

            _vService.GetVariables(pid.toString()).subscribe(variables => {
                this.variables = variables;
            })

            _slService.GetLabelsByPjId(pid).subscribe(labelWithSuggestions => {
                this.labelWithSuggestions = labelWithSuggestions;
            })

            return _sService.GetRespDatas(pid, rid);
        }).subscribe (res => {
            this.RespDatas = res;
        })
    }

    public getValue(VarId: string): string{
        let Variable = this.variables.find(r => r.VarId == VarId);
        if (!Variable) return "";
        let Data = this.RespDatas.Datas.find(r => r.VarCode == Variable.ReportCode);
        return Data ? Data.Value : "";
    }

}