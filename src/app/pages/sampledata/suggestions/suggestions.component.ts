import { Component, OnInit } from "@angular/core";
import { LocalStorageService, SuggestionsService, ComponentService } from "../../../services/angular.service";
import { Router, ActivatedRoute } from '@angular/router';
import { PageBase } from "app/pages/page.base";
import { ModuleType } from "app/services/models";
import { FilterStartEvent } from "app/theme/components/ipsosComponent/component.event";
import { BaThemeSpinner } from "app/theme/services";

export class Button {
    constructor(public index: number, public isSelect: boolean = false) { }
}

@Component({
    selector: 'suggestions',
    templateUrl:'./suggestions.html',
    styleUrls:['./suggestions.scss'],
    
   
})

export class SuggestionsComponent extends PageBase implements OnInit{

    public ResultRespDatas: any[];

    public filter: any = {};

    public Header: any[] = [];

    public headCodeOrder: any[] = [];

    public Buttons: Button[] = [];

    public selectButton: Button = {index: 0, isSelect: true};

    public totalPage: number;

    public ShowButtons: Button[] = [];

    public ShowButtonsSize = 15;

    public IsShowDetail: boolean;

    constructor(public spinner:BaThemeSpinner,private _lService: LocalStorageService, private _sService: SuggestionsService,public componentService:ComponentService,private active:ActivatedRoute, private router: Router) {
        super(spinner,active,componentService,_lService); 
        this.filter.Pjid = _lService.get('id');
       
        _sService.IsShowDetail(this.filter.Pjid).subscribe(IsShowDetail => {
            this.IsShowDetail = IsShowDetail;
        })
        
        this._sService.GetSuggestionListHeader(this.filter.Pjid).subscribe(res => {
            this.Header = res;
            this.Header.forEach(r => {
                this.headCodeOrder.push(r.VarCode);
            });
        })


        this.filter.Page = 1;
        this.filter.PageSize = 20;
        this.filter.UserId = _lService.getAuth().id;

        this.toLoadRemoteSource(this.filter);

        this.loadCount();
    }

    private loadCount(){
        this.Buttons=[];
        this.ShowButtons=[];
        this._sService.GetRespCount(this.filter).subscribe(size => {

            let totalSize = Math.floor(size / this.filter.PageSize) + (size % this.filter.PageSize == 0 ? 0 : 1);
            let b: Button = null;
            for (let i = 0; i < totalSize; i++) {
                b = new Button(i, i == 0);
                this.Buttons.push(b);
            }

            this.ShowButtons = this.Buttons.slice(0, this.ShowButtonsSize);
            this.selectButton = this.ShowButtons[0];

            this.totalPage = size;
        })

    }
     ngOnInit(): void {

      this.componentService.GetComponentsNew(this.pjId,"170816191422553742").subscribe(res=>{

        this.components.next(res);
      });
      this.blockInPipe.asObservable()
                      .filter(e=>!!e)
                      .do((event:FilterStartEvent)=>Object.assign(this.filter,event.filter))
                      .do(()=>this.loadCount())
                      .subscribe((event:FilterStartEvent)=>{this.paging({index: 0, isSelect: true})});
     }
    public getDataTextByVarCode(ResultRespData: any, VarCode: string): string{
        let Datas: any[] = ResultRespData.Datas;
        let Data = Datas.find(r => r.VarCode == VarCode);
        return Data.Value;
    }

    public ViewDetail(ResultRespData: any): void{
        this.router.navigate(['/pages/sampledata/suggestionslabel', {pid: this.filter.Pjid, rid: ResultRespData.RespId}]);
    }

    /**
     * 选择一页
     * @param btn 
     */
    paging(btn: Button) {
        if (this.selectButton === btn) return

        this.selectButton.isSelect = false;
        this.selectButton = btn;
        btn.isSelect = true;

        this.filter.Page = this.selectButton.index + 1;
        this.toLoadRemoteSource(this.filter);
    }

    /**
     * 4个分页导航按钮
     * @param val 
     */
    dirPage(val:number):void{

        var b: Button = this.selectButton;
        b.isSelect = false;
        var index: number = 0;

        switch (val) {
            case 0: // 首页
                index = 0;
                break;
            case 1: // 上一页
                index = b.index - 1;
            break;
            case 2: // 下一页
                index = b.index + 1;
            break; 
            case 3: // 末页
                index = this.Buttons.length - 1;
            break;
        }
        this.selectButton = this.Buttons[index];
        this.selectButton.isSelect = true;
        
        this.filter.Page = this.selectButton.index + 1;

        this.toLoadRemoteSource(this.filter);
    }

    /**
     * 从远程服务器获取数据
     * @param btn 
     */
    private toLoadRemoteSource(filter: any): void{

         this._sService.GetSuggestionList(filter).subscribe(res => {
            this.ResultRespDatas = res;
         })
    }

    public Next(): void{
        let start = this.ShowButtons.pop().index + 1;
        let end = start + this.ShowButtonsSize;
        this.ShowButtons = this.Buttons.slice(start, end);
    }

    public Previous(): void{
        let end = this.ShowButtons[0].index;
        let start = end - this.ShowButtonsSize;
        if (start < 0 || end < 0) return;
        this.ShowButtons = this.Buttons.slice(start, end);
    }
}