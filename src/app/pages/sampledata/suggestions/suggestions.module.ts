import { NgModule } from "@angular/core";
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SuggestionsComponent } from "./suggestions.component";
import { NgaModule } from "app/theme/nga.module";

@NgModule({
    declarations:[SuggestionsComponent],
    imports:[
        CommonModule, 
        NgaModule,
        FormsModule,
    ]
})

export class SuggestionsModule{

}