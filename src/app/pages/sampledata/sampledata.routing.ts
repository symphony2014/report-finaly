import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { SuggestionsComponent } from "./suggestions/suggestions.component";
import { LabelComponent } from "./suggestions-label/label.component";

export const routes: Routes = [
    {
        path:'',
        component: SuggestionsComponent,
    },

    {
        path:'suggestionslabel',
        component: LabelComponent,
    }
]

export const routing: ModuleWithProviders = RouterModule.forChild(routes);