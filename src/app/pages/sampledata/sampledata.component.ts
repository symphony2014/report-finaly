import { Component } from "@angular/core";

@Component({
    selector: 'sample-data',
    template:`<router-outlet></router-outlet>`
})

export class SampleDataComponent{

}