import { Routes, RouterModule }  from '@angular/router';
import { Pages } from './pages.component';
import { ModuleWithProviders } from '@angular/core';

export const routes: Routes = [
  {
    path: 'pages',
    component: Pages,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
        { path: 'password', loadChildren: 'app/pages/modify-password/modify-password.module#ModifyPasswordModule' },
        { path: 'dashboard', loadChildren: 'app/pages/dashboard/dashboard.module#DashboardModule' },
        { path: 'closedloop', loadChildren: 'app/pages/closedloop/shell.module#ClosedLoopShellModule'},
        { path: 'datasearch', loadChildren: 'app/pages/datasearch/datasearch.module#DataSearchModule'},
     ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
