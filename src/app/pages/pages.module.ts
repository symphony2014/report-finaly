import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { routing }       from './pages.routing';
import { NgaModule } from '../theme/nga.module';
import {SlimLoadingBarModule} from 'ng2-slim-loading-bar';
import { Pages } from './pages.component';

import { ModuleService } from "app/services/angular.service";

@NgModule({
  imports: [CommonModule, NgaModule, routing,SlimLoadingBarModule],
  declarations: [Pages],
  providers:[
    ModuleService
   ]
})
export class PagesModule {
   
}
