import { ComponentInfo, RequestParam } from "app/services/models";
import { BroadcastService, ComponentService, LocalStorageService } from "app/services/angular.service";
import { ActivatedRoute } from "@angular/router";
import { Observable, Subscription, BehaviorSubject, Subject,ReplaySubject } from "rxjs";
import { ComponentEvent, FilterStartEvent } from "app/theme/components/ipsosComponent/component.event";
import { GlobalVars } from "app/services/global.vars";
import { BaThemeSpinner } from "app/theme/services";
export class PageBase {
      pjId:string;
      components:ReplaySubject<ComponentInfo[]>=new ReplaySubject<ComponentInfo[]>();  
      blockInPipe:BehaviorSubject<ComponentEvent>=new BehaviorSubject<ComponentEvent>(null);
      blockOutPipe:BehaviorSubject<ComponentEvent>=new BehaviorSubject<ComponentEvent>(null);
      chartEvent:BehaviorSubject<any>=new BehaviorSubject<any>(null);

      moduleId:string;    

      protected isChart(type:string){
      let charts=["Cord","Pie","Radar","Gauge"];
      return charts.some(c=>c==type);
      }

   protected appendGlobal(event:FilterStartEvent):FilterStartEvent{
    event.filter.Pjid=this.pjId;
    event.filter.UserId= GlobalVars.getAuth().id;
   // event.filter.AlgorithmGroupId= this.component.ComponentId;
    return event;
  }
     get chartCount(): Observable<number> {
       return this.components.asObservable().map(c=>c.filter(c=>this.isChart(c.Type as string) ).length)
     }
    constructor( public _spinner: BaThemeSpinner,public route:ActivatedRoute,public componentService:ComponentService,public local:LocalStorageService,moduleId?:string){
         this.pjId=local.get("id");
        //  _spinner.show()
        //  this.componentService.GetComponents(this.pjId,moduleId)
        //                       .do(res=>this.components.next(res))
        //                       .subscribe(()=>this._spinner.hide());
  
     }
}