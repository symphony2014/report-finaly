import { Component } from "@angular/core";
import { Http, URLSearchParams, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BaThemeSpinner } from "app/theme/services";
import { NzModalService } from 'ng-zorro-antd';
import { DataSearchService, LocalStorageService, AlertService, ProjectService} from "app/services/angular.service";
import { GlobalVars } from "app/services/global.vars";

@Component({
    selector:'data-search',
    templateUrl: './datasearch.component.html',
    styleUrls: ['./datasearch.scss'],
})

export class DataSearchComponent{

    API :string;
   
    HEADERS = new Headers({ 'Content-Type': 'application/json' });

    AllDatas: Array<any> = [];

    TempDatas: Array<any> = [];

    ShowDatas: Array<any> = [];

    PageSize: number = 10;

    TotalSize: number = 0;

    currentModal: any;

    uid: string;

    THeaders: Array<any> = [];

    // 弹出框变量
    Basics: Array<any> = [];

    Tables: Array<any> = [];

    Lines: Array<number> = [];

    constructor(private http: Http, 
                private _spinner: BaThemeSpinner, 
                private modalService: NzModalService,
                private _dService: DataSearchService,
                private _lService: LocalStorageService,
                private _aService: AlertService,
                private _pService: ProjectService){
                    this.uid = _lService.getAuth().id;
                    this.API = GlobalVars.apiDataSearchUrl;
                }

    public searchKey: any;
    public searchData(): void{
        var param = {keyValue: this.searchKey};
        this._spinner.show();
        this.http.post(this.API, JSON.stringify(param), { headers: this.HEADERS })
			.map(responce => <any>responce.json())
			.catch(error => {
				return Observable.throw(error)
			}).finally(()=>{})
            .subscribe(result => {
                if (result.Result == 1) {
                    this.AllDatas = [];
                    this.TempDatas = [];
                    this._spinner.hide();
                    return;
                }

                if (!result.data || !result.data.SysSurveyListItems || result.data.SysSurveyListItems.length == 0) {
                    this.AllDatas = [];
                    this.TempDatas = [];
                    this._spinner.hide();
                     return;
                }
               

                result.data.SysSurveyListItems.forEach(r => {
                    r.ColumnDatas = new Array<any>();
                    this.THeaders.forEach(t => {
                        r.ColumnDatas.push({Value:"-"});
                    })
                });

                this.AllDatas = result.data.SysSurveyListItems;
                this.TempDatas = this.AllDatas;
                this.setShowDatas();

                this._dService.GetTHead(this.uid).subscribe(headers => {
                    this._spinner.hide();
                    this.THeaders = headers == null ? [] : headers;

                    this.AllDatas.forEach(r => {
                        this.getColumnData(r);
                    })
                })



            });
    }

    public indexChange(index: number): void{
        let start = (index - 1) * this.PageSize;
        let end = start + this.PageSize;
        this.ShowDatas = this.TempDatas.slice(start, end);
    }

    public hidden(e): void{
        e.target.style.display = "none";  	
        let node = e.target.parentNode.children;
        node[node.length-1].style.display = 'block';
    }

    public searchColumn(name: string, keyword: string, index?: number): void{

        if (!keyword) {
            this.TempDatas = this.AllDatas;
        } else {
             switch (name) {
                case "id":
                    this.TempDatas = this.AllDatas.filter(r => r.SampleId.includes(keyword));
                    break;
                case "name":
                    this.TempDatas = this.AllDatas.filter(r => r.SampleId.includes(keyword));
                    break;
                case "date":
                    this.TempDatas = this.AllDatas.filter(r => r.SurveyListItemInfo[0].PushLinkDate.includes(keyword));
                    break;
                case "num":
                    this.TempDatas = this.AllDatas.filter(r => r.SampleId.includes(keyword));
                    break;
                case "projectname":
                    this.TempDatas = this.AllDatas.filter(r => r.SurveyListItemInfo[0].ProjectName.includes(keyword));
                    break;
                case "other":
                    this.TempDatas = this.AllDatas.filter(r => r.ColumnDatas[index] && r.ColumnDatas[index].Value && r.ColumnDatas[index].Value.includes(keyword));
                    break;
                default:
                    break;
            }
        }

        this.setShowDatas();
    }

    public setShowDatas(): void{
        this.TotalSize = this.TempDatas.length;
        this.ShowDatas = this.TempDatas.slice(0, this.PageSize);
    }

    public getColumnData(data: any): boolean{

        if (data.SurveyListItemInfo[0].FinishStatus == "0") {
            return;
        }

        console.log(data);

        let index = data.SurveyListItemInfo[0].SurveyLink.lastIndexOf("/");
        let sccode =  data.SurveyListItemInfo[0].SurveyLink.substring(index + 1, data.SurveyListItemInfo[0].SurveyLink.length);

        this._dService.GetColumnDatas(data.SurveyListItemInfo[0].ProjectId, sccode, "search_column").subscribe(res => {
            if (res) {
                data.ColumnDatas = res;
            }
        })
        // this._dService.GetColumnDatas("170731191703693001", "170915104903000001").subscribe(res => {
        //     if (res) {
        //         data.ColumnDatas = res;
        //     }
        // })
         return false;
    }

    public showModalForTemplate(contentTpl, footerTpl, data: any) {
        this._spinner.show()

        let index = data.SurveyListItemInfo[0].SurveyLink.lastIndexOf("/");
        let sccode =  data.SurveyListItemInfo[0].SurveyLink.substring(index + 1, data.SurveyListItemInfo[0].SurveyLink.length);

        // 获取基本信息
        // this._dService.GetColumnDatas("170627141406621677", "170811104746000001", "basic")

        this._dService.GetColumnDatas(data.SurveyListItemInfo[0].ProjectId, sccode, "basic").subscribe(basics => {
            this.Basics = basics ? basics : [];
            if (this.Basics.length > 0) {
                let lines = (this.Basics.length / 2) + (this.Basics.length % 2);
                this.Lines = [];
                for (var i = 0; i < lines; i++) {
                    this.Lines[i] = i;
                }
            }
        });

        // 问卷还原信息
        // this._aService.GetRespAlertInfo("170627141406621677", "170811104746000001", "question")
        this._dService.GetColumnDatas(data.SurveyListItemInfo[0].ProjectId, sccode, "question").subscribe(tables => {
            this.Tables = tables ? tables : [] ;
            this._spinner.hide();

            this.currentModal = this.modalService.open({
                content     : contentTpl,
                footer      : false,
                maskClosable: true,
                width : '76%'
            });
        });
    }

    public exportExcel(): void{
        this._spinner.show();
        this._dService.ExportExcel(this.THeaders, this.AllDatas, this.searchKey).subscribe(res => {
            this._spinner.hide();
            alert(res ? "导出成功": "导出失败");
        })
    }

}