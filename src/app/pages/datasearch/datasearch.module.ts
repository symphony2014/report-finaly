import { NgModule } from "@angular/core";
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { DataSearchComponent } from "./datasearch.component";
import { routing } from "./datasearch.routing";
import { NzPaginationModule } from 'ng-zorro-antd';
import { DataSearchService, AlertService, ProjectService} from "app/services/angular.service";

@NgModule({
    declarations:[DataSearchComponent],
    imports:[CommonModule,
             NgaModule,
             FormsModule,
             routing,
             NzPaginationModule,
             ],
    providers:[
        DataSearchService,
        AlertService,
        ProjectService,
    ]

})

export class DataSearchModule{}