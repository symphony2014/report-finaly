import { Routes, RouterModule }  from '@angular/router';

import { DataSearchComponent } from './datasearch.component';
import { ModuleWithProviders } from '@angular/core';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
  {
    path: '',
    component: DataSearchComponent,
    children: [
      //{ path: 'treeview', component: TreeViewComponent }
    ]

  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
