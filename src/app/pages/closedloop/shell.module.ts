import { NgModule } from "@angular/core";
import { ClosedLoopShellComponent } from "./shell.component";
import { CommonModule }  from '@angular/common';
import { routing }       from './shell.routing';
import { NgaModule } from '../../theme/nga.module';
import { ClosedLoopHomeModule } from "./alert/home/home.module";
import { ClosedLoopListModule } from "./alert/list/list.module";
import { ClosedLoopDetailModule } from "./alert/detail/detail.module";
import { DealLogComponent } from "./alert/detail/deal-log/deal-log.component";
import { MdDialogRef,MD_DIALOG_DATA  } from "@angular/material";


@NgModule({

    declarations:[ClosedLoopShellComponent],
    imports:[
        CommonModule, 
        routing, 
        NgaModule, 
        ClosedLoopHomeModule,
        ClosedLoopListModule, 
        ClosedLoopDetailModule,
        ],
    providers: [
        {provide: MD_DIALOG_DATA, useValue: {} },
        {provide: MdDialogRef, useValue: {} },
    ],
    entryComponents:[DealLogComponent]
})

export class ClosedLoopShellModule{


}