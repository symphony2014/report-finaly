import { Routes, RouterModule }  from '@angular/router';

import { CloseLoopList } from './alert/list/list.component';
import { CloseLoopDetail } from './alert/detail/detail.component';
import { CloseLoopHome } from './alert/home/home.component';
import { ModuleWithProviders } from '@angular/core';

export const routes: Routes = [
  {
    path: '',
    component: CloseLoopHome
  },

  {
    path: 'home',
    component: CloseLoopHome
  },

  {
    path: 'list',
    component: CloseLoopList
  },

  {
    path: 'detail',
    component: CloseLoopDetail
  },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);