import { NgModule } from "@angular/core";
import { CloseLoopHome } from "./home.component";
import { CommonModule }  from '@angular/common';
import { NgaModule } from 'app/theme/nga.module';
import { RouterModule }  from '@angular/router';
import { AlertService } from "app/services/angular.service";

@NgModule({

    declarations:[CloseLoopHome,],
    imports:[CommonModule, NgaModule, RouterModule],
    providers:[AlertService,],
})

export class ClosedLoopHomeModule{

}