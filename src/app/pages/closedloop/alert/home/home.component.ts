import { Component, SimpleChange, } from "@angular/core";
import { LocalStorageService, AlertService, ComponentService } from "app/services/angular.service";
import { RequestParam, RequestParamFilter } from "app/services/models/IpsosReport.ModelBiz";
import { ActivatedRoute } from "@angular/router";
import { PageBase } from "app/pages/page.base";
import { FilterStartEvent, ComponentEvent } from "app/theme/components/ipsosComponent/component.event";
import { BaThemeSpinner } from "app/theme/services";
import { GlobalVars } from "app/services/global.vars";
import { Observable, Subscription, Subject, ReplaySubject, BehaviorSubject } from "rxjs";
import { ComponentInfo } from "app/services/models";
@Component({
    selector:'closed-loop-home',
    templateUrl:'./home.component.html',
    styleUrls:['./home.scss']
})

export class CloseLoopHome {

    public AlertStatistics: Array<any> = [];

    public AlertSituation: any;

    public start: string;

    public end: string;

    public config:any = {OK:"确认", Reset:"重置"};

    // 虚拟项目的pjid
    get pjId(){
        return location.href.match(/pjId=(\d{18})/)[1];
    };
    get moduleid(){
        return location.href.match(/id=(\d{18})/)[1];
    };
    // 真是项目的pjid
    private pId: string;

    public title: string = "酒店";

    private HOME_KEY: string = "closeloop-home";

    private LIST_KEY: string = "closeloop-list";

    private param: any;

   
    components:ReplaySubject<ComponentInfo[]>=new ReplaySubject<ComponentInfo[]>();  
    blockInPipe:BehaviorSubject<ComponentEvent>=new BehaviorSubject<ComponentEvent>(null);
    blockOutPipe:BehaviorSubject<ComponentEvent>=new BehaviorSubject<ComponentEvent>(null);
    constructor(public _spinner: BaThemeSpinner, public route:ActivatedRoute,public componentService:ComponentService, private _lService: LocalStorageService, private _aService: AlertService) {
        
        this.title = GlobalVars.title;

        this.param = _lService.get(this.HOME_KEY);
        if (this.param) {
            this.getAlertSituation(this.param);
            this.getAlertStatistics(this.param);
            this._lService.remove(this.HOME_KEY);
            this._lService.remove(this.LIST_KEY);
        }
        
    }

    public ngOnInit(): void{

        this.blockInPipe.asObservable()
                .do(e=> {
                    console.log(e);
                })
                .filter(e=>!!e && e instanceof FilterStartEvent)
                .map((e : FilterStartEvent)=>{this.param = e.filter; return this.param})
                .do(e => {
                    this.goList();
                    this.getAlertSituation(e);
                })
                .do(e => {
                   this.getAlertStatistics(e);
                }).subscribe();

         this.route.params
                   .switchMap(params=>{
                       this.AlertStatistics = [];
                       this.AlertSituation = null;
                       return this.componentService.GetComponentsNew(this.pjId, this.moduleid)
                    })
                   .subscribe(data=>{
                     this.components.next(data);
                   })
    }

    public getAlertSituation(requestParam: any): void{
        this.controlLoading();
        this.isLoadingSituation = true;
        this._aService.GetAlertSituation(requestParam).subscribe(data => {
            this.isLoadingSituation = false;
            this.AlertSituation = data ? data : [];
        })
    }

    public getAlertStatistics(requestParam: any): void{
        this.isLoadingStatistics = true;
        this._aService.GetAlertStatistics(requestParam).subscribe(data=>{
            this.isLoadingStatistics = false;
            this.AlertStatistics = data ? data : [];

            if (this.AlertStatistics.length < 2) {
                this.pId = "0";
                return;
            }

            this.pId = this.AlertStatistics[0].VarValue.PjId;

        })
    }

    public isLoadingSituation: boolean = false; 
    public isLoadingStatistics: boolean = false; 

    public controlLoading(): void{
        let instance: CloseLoopHome = this;
        instance._spinner.show();
        var id = setInterval(function(){
            if (!instance.isLoadingSituation && !instance.isLoadingStatistics) {
                instance._spinner.hide();
                clearInterval(id);
            }
        }, 1000);
    }

    public goList(): void{
        this._lService.set(this.HOME_KEY, this.param);
    }
}