import { NgModule } from "@angular/core";
import { CloseLoopList } from "./list.component";
import { RouterModule }  from '@angular/router';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from 'app/theme/nga.module';
import { NzSelectModule, NzPaginationModule } from 'ng-zorro-antd';
import { FRelationService , VariableService} from "app/services/angular.service";
import { MdInputModule, MdInputContainer, MdSelectModule, MdButtonModule, MdCheckboxModule, MdOptionModule, MdDialogModule, MdIconModule, MdSnackBarModule, MdSnackBar, MdCardModule, MdSlideToggleModule } from '@angular/material';

@NgModule({
    declarations:[CloseLoopList,],
    imports:[RouterModule, 
            CommonModule, 
            FormsModule,
            NgaModule,
            MdSelectModule,
            MdCheckboxModule,
            MdOptionModule,
            MdDialogModule,
            MdButtonModule,
            MdInputModule ,
            MdIconModule,
            MdSnackBarModule,
            MdCardModule,
            MdSlideToggleModule,
            NzSelectModule,
            NzPaginationModule,
            ],
    providers:[FRelationService,
               VariableService],
})

export class ClosedLoopListModule{

}