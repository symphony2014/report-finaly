import { Component } from "@angular/core";
import { LocalStorageService, AlertService, VariableService, FRelationService } from "app/services/angular.service";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MdDialog, MdDialogRef, MD_DIALOG_DATA, MdSnackBar } from '@angular/material';
import { RequestParam, RequestParamFilter } from "app/services/models/IpsosReport.ModelBiz";
import { BaThemeSpinner } from "app/theme/services";
import { GlobalVars } from "app/services/global.vars";

@Component({
    selector:'closed-loop-list',
    templateUrl:'./list.component.html',
    styleUrls:['./list.scss']
})

export class CloseLoopList{

    public size: string = 'default';

    // 虚拟项目的pjid
    get PjId(){
        return location.href.match(/pjId=(\d{18})/)[1];
    }

    // 真实项目的pjid
    get pId() {
        return location.href.match(/pId=(\d{18})/)[1];
    }

    get moduleid() {
        return location.href.match(/id=(\d{18})/)[1];
    }

    public UserId: number;

    public AlertTimeType: string;

    public searching: boolean = true;

    // 终极题
    public Var: any;

    // 用户可见的终极题选项
    public UserOptions: Array<any> = [];

    // 分组题目作为分值帅选添加
    public GroupVars: Array<any> = [];

    public Headers: Array<any> = [];

    public DataList: Array<any> = [];

    public Param: RequestParam;

    public VarFilter: RequestParamFilter = {};

    public ScoreFilter: RequestParamFilter = {};

    public StatusFilter: RequestParamFilter = {};

    public TimeFilter: RequestParamFilter = {};

    public PJIDFilter: RequestParamFilter = {};

    public Class2Filter: RequestParamFilter = {};

    public totalPage: number;

    public IsShowSearch:boolean;

    public HomeRequestParam: any;

    private HOME_KEY: string = "closeloop-home";

    private LIST_KEY: string = "closeloop-list";

    private IsFromBack: boolean = false;
    
    private title: string;

    constructor(private _lService: LocalStorageService, 
                private _aService: AlertService,  
                private router: ActivatedRoute,
                private _fService: FRelationService,
                private _vService: VariableService,
                private _router: Router,
                public _spinner: BaThemeSpinner) {

        this.title = GlobalVars.title;
        this.UserId = this._lService.getAuth().id;
        this.HomeRequestParam = this._lService.get(this.HOME_KEY);
        this.Param = this._lService.get(this.LIST_KEY);
        this.VarFilter.ParamName = "VarValueId";
        this.ScoreFilter.ParamName = "Question";
        this.StatusFilter.ParamName = "AlertStatus";
        this.PJIDFilter.ParamName = "CustomPjId";
        this.VarFilter.OptionValues = [];
        this.ScoreFilter.OptionValues = [];
        this.StatusFilter.OptionValues = [];
        this.PJIDFilter.OptionValues = [];
        this.PJIDFilter.OptionValues[0] = this.PjId;
        this.Class2Filter.OptionValues = [];

        this.Class2Filter = this.HomeRequestParam.Filters.find(r => r.ParamName == "class2");

        if (this.Param) {
            this.IsFromBack = true;
            this._lService.remove(this.LIST_KEY);
            
            this.VarFilter = this.Param.Filters.find(r => r.ParamName == this.VarFilter.ParamName);
            this.ScoreFilter = this.Param.Filters.find(r => r.ParamName == this.ScoreFilter.ParamName);
            this.StatusFilter = this.Param.Filters.find(r => r.ParamName == this.StatusFilter.ParamName);
            this.PJIDFilter = this.Param.Filters.find(r => r.ParamName == this.PJIDFilter.ParamName);
            if (this.Class2Filter) {
                this.Class2Filter = this.Param.Filters.find(r => r.ParamName == this.Class2Filter.ParamName);
            }
        } else {
            this.Param = {};
            this.Param.Filters = [];
            this.Param.Pjid = this.pId.toString();
            this.Param.UserId = this.UserId.toString();
            this.Param.PageSize = 15;
            this.Param.Page = 1;

            this.Param.Filters.push(this.VarFilter);

            this.Param.Filters.push(this.ScoreFilter);

            this.Param.Filters.push(this.StatusFilter);

            this.Param.Filters.push(this.PJIDFilter);

            if (this.Class2Filter) {
                this.Param.Filters.push(this.Class2Filter);
            }
        }
        // 时间默认当月
        this.TimeFilter.ParamName = "Datetime";
        this.TimeFilter.ShowType = "DateTime";
        this.Param.Filters.push(this.TimeFilter);

    }

    public ngOnInit(): void{

        this._aService.GetTHead(this.pId).subscribe(headers => {
            this.Headers = headers;
        });

        let status: string;
        let varValueId: string;
        this.router.params.switchMap((param: Params) => {
            if (!this.IsFromBack) {
                varValueId = param['varValueId'];
                status = param['status'];

                if (varValueId != "0") {
                    this.VarFilter.OptionValues[0] = varValueId;
                }
                
                // 状态筛选条件
                this.StatusFilter.OptionValues[0] = status;
            }
             return this._vService.GetUltimateQuestionAndUserToOption(this.pId, this.UserId)
             
        }).subscribe(result => {
            this.Var = result.Var;
            this.UserOptions = result.VarValues;

            // 题筛选条件
            this.VarFilter.VarId = this.Var.VarId;
            

            // 上一页面传递过来的时间参数
            var TimeFilter = this.HomeRequestParam.Filters.find(r => r.ShowType == "datetime");
            this.TimeFilter.DateStart = TimeFilter.DateStart;
            this.TimeFilter.DateEnd =TimeFilter.DateEnd;

            this.search();
            this.loadCount();
        })

        this._fService.GetGroupVariables(this.pId).subscribe(groupVars => {
            if (groupVars) {
                groupVars.forEach(r => {
                    this.GroupVars.push(r.GroupName);
                    r.Vars.forEach(v => this.GroupVars.push(v));
                });
            }
        })

    }

    public getDataTextByVarCode(respAlertListViewInfo: any, varCode: string): string{
        let Datas: any[] = respAlertListViewInfo.Datas;
        return Datas[varCode];
    }

    
    public timeChange(): void{

        let date: Date = new Date();
        let endYear = date.getFullYear();
        let endMonth = date.getMonth() + 1;
        let endDate = date.getDate();
        let startYear: number = endYear;
        let startMonth: number = endMonth;
        let startDate: number = 1;
        switch(this.AlertTimeType){
            case "PastSixMonth": // 过去六个月
                if (endMonth <= 5)
                {
                    startMonth = endMonth - 6 + 12 + 1;
                    startYear = endYear - 1;
                } else {
                    startMonth = endMonth - 6 + 1;
                }
                break;
            case "PastThreeMonth": // 过去三个月
                if (endMonth <= 2)
                {
                    startMonth = endMonth - 3 + 12 + 1;
                    startYear = endYear - 1;
                } else {
                    startMonth = endMonth - 3 + 1;
                }
                break;
            case "PastTwoMonth": // 过去两个月
                if (endMonth == 1) {
                    startMonth = 12;
                    startYear = endYear - 1;
                } else {
                    startMonth = endMonth - 1;
                }
                break;
            case "PastOneMonth": // 过去一个月
                startMonth = endMonth;
                break;
            case "PastOneWeek": // 过去一周
                let edate = new Date(date.getTime() - 6 * 24 * 60 * 60 * 1000)
                startYear = edate.getFullYear();
                startMonth = edate.getMonth() + 1;
                startDate = edate.getDate();
                break;
            case "PastOneDay": // 过去一天
                startYear = endYear;
                startMonth = endMonth;
                startDate = endDate;
                break;
        }

        this.TimeFilter.DateStart = startYear + "/" + startMonth + "/" + startDate + " 00:00:00";
        this.TimeFilter.DateEnd = endYear + "/" + endMonth + "/" + endDate + " 23:59:59";
    }

    public search(): void{
        this._spinner.show();
        this.searching = true;

        this.loadCount();

        this._aService.GetAlertList(this.Param).subscribe(list => {
            this.DataList = list ? list : [];
            this.searching = false;
            this._spinner.hide();
        })
    }

    private loadCount(){
        let varvalueid = this.VarFilter.OptionValues[0] ? this.VarFilter.OptionValues[0] : 0;
        this._aService.GetAertRespCount(this.Param).subscribe(size => {
            this.totalPage = size;
        })

    }

    public indexChange(index: number): void{
        if (index == undefined || (this.DataList && this.DataList.length == 0)) return;
        this.Param.Page = index;
        this.search();
    }

    public IsNormalClosded = false;
    public back(): void{
        this.IsNormalClosded = true;
        this._lService.remove(this.LIST_KEY);
        this._router.navigate(["/pages/closedloop/home", {pjId:this.PjId, id: this.moduleid}]);
    }

    public ngOnDestroy(): void{
        if (this.IsNormalClosded) return;
        this._lService.remove(this.HOME_KEY);
    }

    public goDetail(): void{
        this.IsNormalClosded = true;
        this._lService.set(this.LIST_KEY, this.Param);
    }

}