import { Component } from "@angular/core";
import { LocalStorageService, AlertService, FRelationService } from "app/services/angular.service";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MdDialog, MdDialogRef, MD_DIALOG_DATA, MdSnackBar, MdDialogConfig } from '@angular/material';
import { DealLogComponent } from './deal-log/deal-log.component';
import { NzModalService } from 'ng-zorro-antd';
import { SmsSyatemService } from "app/services/angular.service";
import { BaThemeSpinner } from "app/theme/services";

@Component({
    selector:'closed-loop-detail',
    templateUrl:'./detail.component.html',
    styleUrls: ['./detail.scss']
})

export class CloseLoopDetail{

    // 虚拟项目的pjid
    get PjId(){
        return location.href.match(/pjId=(\d{18})/)[1];
    }

    // 真实项目的pjid
    get pId() {
        return location.href.match(/pId=(\d{18})/)[1];
    }

    get moduleid() {
        return location.href.match(/id=(\d{18})/)[1];
    }

    public RespId: number;

    public VarValueId: number;

    public RespStatus: string;

    public Basic: Array<any> = [];

    public Table: Array<any> = [];

    public Lines: Array<number> = [];

    public UserId: number;

    public UserName: string;

    public UserRights: Array<string> = [];

    public UserHasOperatorRight: boolean = false;

    public AssignableUsers: Array<any> = [];

    public DealLogs: Array<any> = [];

    public AssignStatus: Array<string> = ["New", "Process"];

    private currentModal: any;

    public Log: any;

    public Concerns: Array<any> = [];

    public concern : string = "请选择";

    constructor(private _lService: LocalStorageService, 
                private _aService: AlertService,  
                private router: ActivatedRoute,
                private _fService: FRelationService,
                public dialog: MdDialog,
                public nrouter: Router,
                private _nzmService: NzModalService,
                private _smsService: SmsSyatemService,
                private _spinner: BaThemeSpinner) {
        this.UserId = this._lService.getAuth().id;
        this.UserName = this._lService.getAuth().realName;
    }

    public ngOnInit(): void{
         this.router.params.switchMap((param: Params) => {
            this.RespId =  param['respId'];

            if (!this.RespId) return;
            
            this._spinner.show();

            // 获取报表信息
             return this._aService.GetRespAlert(this.pId, this.RespId);
        }).subscribe(alertResp => {
            if (!alertResp) {
                alert("样本不存在");
                this._spinner.hide();
                return;
            }

            this.RespStatus = alertResp.AlertStatus;
            this.VarValueId = alertResp.VarValueId;

            this._aService.GetRespAlertInfo(this.pId.toString(), this.RespId.toString(), "question").subscribe(table => {
                this.Table = table ? table : [] ;

            // 获取用户权限
            this._fService.GetUserRights(this.pId, this.UserId).subscribe(rights => {
                this.UserRights = rights ? rights : [];

                // 获取可分配下级
                if(this.checkRight("assign", this.AssignStatus)){
                    this._fService.GetUsersByFid(this.pId, this.UserId, this.VarValueId).subscribe(users => {
                        this.AssignableUsers = users ? users : [];
                    })
                }
            })

            // 判断用户是否有处理权限
            this._aService.HasOperationToResp(this.pId, this.UserId, this.RespId).subscribe(res => {
                this.UserHasOperatorRight = res;
            })

            // 获取基本信息
            this._aService.GetRespAlertInfo(this.pId.toString(), this.RespId.toString(), "basic").subscribe(basic => {
                this._spinner.hide();
                 this.Basic = basic ? basic : [];
                 if (this.Basic.length > 0) {
                    let lines = Math.floor((this.Basic.length / 2)) + (this.Basic.length % 2);
                    for (var i = 0; i < lines; i++) {
                       this.Lines[i] = i;
                    }
                 }
            })

            // 获取处理日志
            this.getProcessLog();
            });
            
        })
    }

    public checkRight(right: string, status: Array<string>): boolean{
        let index1 = this.UserRights.findIndex(r => r == right);
        let index2 = status.findIndex(r => r == this.RespStatus);
        return index1 >= 0 &&  index2 >= 0;
    }

    public showOperationBtn(): boolean{
        if (this.UserRights.length == 0) return false;

        if (this.RespStatus == "Complate" || this.RespStatus == "Close") return false;

        let index = this.UserRights.findIndex(r => r == "view");

        if (index < 0) return false;

        let index2 = this.UserRights.findIndex(r => r == "operation")
        return this.UserHasOperatorRight || index2 >= 0;
    }

    public showDealLog(): void{
        let config: MdDialogConfig = {};
        config.data = {};
        config.data.Basics = this.Basic;
        config.data.PjId = this.pId;
        config.disableClose = true;
        config.width = "50%";
        this.dialog.open(DealLogComponent, config).afterClosed().subscribe(log => {
            if (!log) return;
            log.OperationType = "Proess";
            this.saveLog();
        })
    }
   
    public chooseConcern(concern: string): void{
        this.concern = concern;
        this.Log.Concern = concern;
    }

    public showModalForTemplate(contentTpl, footerTpl) {
		this.Log = {};
        this.Log.ApprovedType = "InProcess";
        this._fService.GetConcernTypes(this.pId).subscribe(concerns => {
            this.Concerns = concerns? concerns : [];
        })
    
	    this.currentModal = this._nzmService.open({
			content     : contentTpl,
			footer      : false,
			maskClosable: false,
			width : '670px',
		});
	}

    public close(): void{
        this.concern = "请选择";
         this.currentModal.destroy();
    }

    public radioChanged(value: string): void{
        switch(value){
            case "Satisfying":
            case "Dissatisfied":
            case "InProcess":
                this.Log.ApprovedType = value;
                this.Log.OperationType = "";
                break;
            case "Close":
                this.Log.ApprovedType = "";
                this.Log.OperationType = value;
                break;
        }
    }

    public handleSave(): void{
        if (!this.Log) return;
        this.Log.OperationType = this.Log.OperationType ?  this.Log.OperationType : "Proess";
        this.saveLog();
        this.close();
    }

    public saveLog(): void{

        this.Log.PjId = this.pId;
        this.Log.UserPassportId = this.UserId;
        this.Log.RespId = this.RespId;
        this._aService.SaveLog(this.Log).subscribe(alog => {
            if (!alog) {
                alert("处理保存失败");
                return;
            }
           this.DealLogs.unshift(alog);

           // 更新样本状态
           let status: string;
           switch (alog.Log.OperationType)
                {
                    case "Assign":
                        status = "Process";
                        break;
                    case "Proess":
                        status = "Process";
                        // this.UserHasOperatorRight = false;
                        break;
                    case "Complete":
                        status = "Complate";
                        break;
                    case "Close":
                        status = "Close";
                        break;
                }

           this._aService.UpdateAlertRespStatus(this.pId, this.RespId, status).subscribe(res => {
                if (res) {
                    // 将新的状态，赋值给当前的样本状态
                    this.RespStatus = status;
                    this.showOperationBtn();
                    this.checkRight("close", this.AssignStatus);
                    if (this.Log.OperationType == "Assign") {
                        // TODO 发送邮件
                    }
                }
           });
        })
    }

    public getProcessLog(): void{
        this._aService.GetProcessLogs(this.pId, this.RespId).subscribe(logs => {
            this.DealLogs = logs ? logs : [];
        })
    }

    public assign(AssignedUserId: number): void{

        let userToAlertResp: any = {};

        userToAlertResp.PjId = this.pId;
        userToAlertResp.RespId = this.RespId;
        userToAlertResp.UserPassportId = AssignedUserId;

        this._aService.AddUserToAlertResp(userToAlertResp).subscribe(res => {
            if (!res) {
                alert("任务指派失败");
                return;
            }

             // 判断用户是否有处理权限
            this._aService.HasOperationToResp(this.pId, this.UserId, this.RespId).subscribe(res => {
                this.UserHasOperatorRight = res;
            })

            let user = this.AssignableUsers.find(r => r.UserPassportId == AssignedUserId);
            this.Log = {};
            this.Log.OperationType = "Assign";
            this.Log.ApprovedType = "InProcess";

            this.Log.Comment = `样本由【${this.UserName}】 指派给【${user.UserName}】处理`;

            this.saveLog();
        })

        
    }

    public finish(operatorType: string):void{
        this.Log = {};
        this.Log.OperationType = operatorType;
        this.Log.ApprovedType = "InProcess";

        if (operatorType == "Complete") {
            this.Log.Comment = `样本顺利处理，由【${this.UserName}】关闭`;
        } else {
            this.Log.Comment = `样本无法处理，由【${this.UserName}】关闭`;
        }

        this.saveLog();
    }

    public back(): void{
		this.nrouter.navigate(['/pages/closedloop/list',{pId:this.pId, pjId:this.PjId, id: this.moduleid, varValueId:this.VarValueId, status:this.RespStatus}]);
	}

    public getSubString(text: string): string{
        if (text.length <= 6) return text;

        return text.substring(0,5) + "...";
    }

}