import { Component, Inject} from "@angular/core";
import { MdDialogRef,MD_DIALOG_DATA , MdDialogConfig } from "@angular/material";
import { AlertService, FRelationService, LocalStorageService } from "app/services/angular.service";

@Component({
    selector: 'deal-log',
    templateUrl:'./deal-log.component.html',
    styleUrls: ["../detail.scss"],
})  

export class DealLogComponent{

    public Basics: Array<any> = [];

    public Concerns: Array<any> = [];

    public Log: any;

    public PjId: number;

    constructor(@Inject(MD_DIALOG_DATA)private data: any, 
                public dialogRef: MdDialogRef<DealLogComponent>,
                private _aService: AlertService, 
                private _fService: FRelationService){
        this.Basics = data.Basics;
        this.PjId = data.PjId;
        this.Log = {};
    }

    public ngOnInit(): void{
        this._fService.GetConcernTypes(this.PjId).subscribe(concerns => {
            this.Concerns = concerns? concerns : [];
        })
    }

}