import { NgModule } from "@angular/core";
import { CloseLoopDetail } from "./detail.component";
import { RouterModule }  from '@angular/router';
import { CommonModule }  from '@angular/common';
import { NgaModule } from 'app/theme/nga.module';
import { FormsModule } from '@angular/forms';
import { NzDropDownModule, NzModalService, NzRadioModule, NzMenuModule, NzCheckboxModule } from 'ng-zorro-antd';
import { MdInputModule, MdInputContainer, MdSelectModule, MdButtonModule, MdCheckboxModule, MdOptionModule, MdDialogModule, MdIconModule, MdSnackBarModule, MdSnackBar, MdCardModule, MdSlideToggleModule } from '@angular/material';
import { DealLogComponent } from "./deal-log/deal-log.component";
import { SmsSyatemService } from "app/services/angular.service";

@NgModule({

    declarations:[CloseLoopDetail, DealLogComponent] ,
    imports:[RouterModule, 
            NgaModule,
            FormsModule,
            CommonModule,
            MdSelectModule,
            MdCheckboxModule,
            MdOptionModule,
            MdDialogModule,
            MdButtonModule,
            MdInputModule ,
            MdIconModule,
            MdSnackBarModule,
            MdCardModule,
            MdSlideToggleModule,
            NzDropDownModule,
            NzRadioModule,
            NzMenuModule,
            NzCheckboxModule,
            ],
    providers:[
        NzModalService,
        SmsSyatemService,
    ],
})

export class ClosedLoopDetailModule{

}