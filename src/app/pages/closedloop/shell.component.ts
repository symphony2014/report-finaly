import { Component } from "@angular/core";

@Component({
    selector:'closed-loop-shell',
    template: `
        <router-outlet></router-outlet>
    `,
})

export class ClosedLoopShellComponent{

}