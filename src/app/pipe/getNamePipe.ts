import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'getName'})
export class GetNamePipe implements PipeTransform {
  transform(id: number, list: any[]): string {
        let item: any = list.find(r => r.id == id)
        if (item == null) return "";

        let name: string = item.name;
        if (!name) return "";

        return name;
  }
}