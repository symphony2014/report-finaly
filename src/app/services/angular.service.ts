﻿
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';

import { Injectable, EventEmitter } from '@angular/core';
import { Http, URLSearchParams, Headers } from '@angular/http';
import { Params, PageData, Project, ModuleType, ComponentInfo, RequestParam, RequestParamFilter, ResultDataItem, ResultDataItems, ResultDefaultData, AlgorithmPropertyShowStyle, AlgorithmPropertyType, AlgorithmPropertyViewInfo, AlgorithmType, AlgorithmViewInfo, ValueType, ComponentTree } from './models/IpsosReport.ModelBiz';
import { AdminStatusType, Admins } from './models/IpsosReport.ModelSYS';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { GlobalVars } from "./global.vars";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import * as _ from "lodash";
import { Result } from 'app/services/models';

@Injectable()
export class BroadcastService{
	public broadcast:BehaviorSubject<RequestParam> =new BehaviorSubject<RequestParam>(null);
	public cast(request:RequestParam):void{
		 this.broadcast.next(request);	
	}
}
@Injectable()
export class LocalStorageService{
  public setAuth(res:any){
     localStorage.setItem("Auth",JSON.stringify(res)); 
  }
  
  public getAuth(): any{
    return JSON.parse(localStorage.getItem("Auth"));
  }

  public remove(key: string){
	localStorage.removeItem(key);
  }

  public set(key:string,value:any){
	localStorage.setItem(key,JSON.stringify(value));
  }

  public get(key:string):any{
	let value=localStorage.getItem(key);
	if(value && value.indexOf('{')>-1){
		return JSON.parse(value);
	}else{
		return eval(value);
	}
  }
  
  public setResumeData(res: any) {
	localStorage.setItem("Resume", JSON.stringify(res));
  }

  public getResumeData(): any{
	  return JSON.parse(localStorage.getItem("Resume"));
  }
 
  public removeResumeData(): void{
	  this.remove("Resume");
  }
}



@Injectable()
export class SService{
	 _apiUrl:string;
	// 接口的controller
	private key:string;

	// post请求的header
	protected headers = new Headers({ 'Content-Type': 'application/json' });
	
	/**
	 * 构造方法
	 * @param http 初始化http服务
	 * @param key 初始化接口controller
	 */
	constructor(protected slimService:SlimLoadingBarService, protected http: Http , key: string) {
		this.key = key;
		this._apiUrl=GlobalVars.apiUrl;
		this.slimService.interval=20;
	 }

	/**
	 * 用controller和方法拼凑请求api
	 * @param method Controller下的方法名
	 */
	protected apiUrl(method: string) {
		return this._apiUrl + this.key + '/' + method;
	}

	/**
	 * get请求
	 * @param method 请求方法名
	 * @param params 请求参数
	 */
	protected get(method: string, params : URLSearchParams) : Observable<any> {
		this.slimService.start(r=>console.log(r));
		return this.http.get(this.apiUrl(method), { search: params })
			.map(responce => responce.json())
			.catch(error => {
				return Observable.throw(error)
			}).finally(()=>this.slimService.complete())
	}
	/**
	 * get请求
	 * @param method 请求方法名
	 * @param params 请求参数
	 */
	protected delete(method: string, params : URLSearchParams) : Observable<any> {
		    return this.http.delete(this.apiUrl(method), { search: params })
			.map(responce => responce.json())
			.catch(error => {
				return Observable.throw(error)
			});
	}

	/**
	 * post请求
	 * @param method 请求方法名
	 * @param obj    提交的对象
	 */
	protected post(method: string, obj: any) : Observable<any> {

		this.slimService.start();
		return this.http.post(this.apiUrl(method), JSON.stringify(obj), { headers: this.headers })
			.map(responce => <any>responce.json())
			// .map(response=>{
			// 	if(!response || (!_.isObject(response) && response[0]==null) || response[0]==null)
			// 		return [{"AlgorithmId":"170502211219638001","Text":"0算法","XAxis":["2017-08","2017-07","2017-06","2017-05","2017-04","2017-03"],
			// 	"Legend":["0算法"],
			// 	"Series":[{"Name":"2017-03","Values":["85.43"],"Count":136,"Index":6}]},
			// 	{"AlgorithmId":"170502211219638001","Text":"0算法","XAxis":["2017-08","2017-07","2017-06","2017-05","2017-04","2017-03"],
			// 	"Legend":["0算法"],
			// 	"Series":[{"Name":"2017-03","Values":["85.43"],"Count":136,"Index":6}]},
			// 	{"AlgorithmId":"170502211219638001","Text":"0算法","XAxis":["2017-08","2017-07","2017-06","2017-05","2017-04","2017-03"],
			// 	"Legend":["0算法"],
			// 	"Series":[{"Name":"2017-03","Values":["85.43"],"Count":136,"Index":6}]}]
			// 	else
			// 		return response;
			// })//Debug
			.catch(error => {
				return Observable.throw(error)
			}).finally(()=>this.slimService.complete());
	}

	/**
	 * Add 方法
	 * @param obj add对象
	 */
	Add(obj: any): Observable<Boolean> {
		return this.post("Add", obj);
	}

	/**
	 * update 方法
	 * @param obj update对象
	 */
	Update(obj: any): Observable<Boolean> {
		return this.post("Update", obj);
	}

	/**
	 * delete 方法
	 * @param obj delete 对象
	 */
	Delete(obj: any): Observable<Boolean>{
		return this.post("Delete", obj);
	}

	/**
	 * getAll 方法
	 */
	GetAll(): Observable<any> {
		let params = new URLSearchParams();
		return this.get("GetAll", params);
	}

	/**
	 * 分页方法
	 * @param pageIndex 需要获取的页的序号
	 * @param rowSize   每页放置数据条数
	 * @param clientId  客户Id， 只有在获取项目的时候需要提供该参数
	 */
	Paging(pageIndex: number, rowSize: number, _params: Params): Observable<any> {
		let params = new URLSearchParams();
		params.set("pageIndex", pageIndex.toString());
		params.set("rowSize", rowSize.toString());
		if (_params.IsSA != undefined) {
			params.set("isSA", _params.IsSA.toString());
		}
		if (_params.ClientId) {
			params.set("clientId", _params.ClientId.toString());
		}

		if (_params.AdminId) {
			params.set("adminId", _params.AdminId.toString());
		}

		return this.get("Paging", params);
	}

	/**
	 * 
	 * @param pageIndex 需要获取的页的序号
	 * @param rowSize   每页放置数据条数
	 * @param keyWords  搜索关键字
	 * @param clientId  客户Id， 只有在获取项目的时候需要提供该参数
	 */
	Search(pageIndex: number, rowSize: number, keyWords: string, _params: Params): Observable<any> {
		let params = new URLSearchParams();
		params.set("pageIndex", pageIndex.toString());
		params.set("rowSize", rowSize.toString());
		params.set("keyWords", keyWords);
		if (_params.IsSA != undefined) {
			params.set("isSA", _params.IsSA.toString());
		}

		if (_params.ClientId) {
			params.set("clientId", _params.ClientId.toString());
		}

		if (_params.AdminId) {
			params.set("adminId", _params.AdminId.toString());
		}

		return this.get("Search", params);
	}


}
@Injectable()
export class TempService  {
	constructor(private http:Http){}
	getTemp(key:string){
		return this.http.get(`/assets/templates/${key}.json`);
	}
}
@Injectable()
export class AdminService extends SService{


	constructor(protected slimService:SlimLoadingBarService,protected http: Http ) {
		super(slimService,http, "Admin");
	 }

	DeleteAdmin(adminId: string): Observable<Boolean> {
		let params = new URLSearchParams();
		params.set("adminId", adminId);
		return this.get("DeleteAdmin", params);
	}

	IsExist(adminLoginName: string): Observable<Boolean> {
		let params = new URLSearchParams();
		params.set("adminLoginName", adminLoginName);
		return this.get("IsExist", params);
	}

	Login(loginName: string, pwd: string): Observable<any> {
		let params = new URLSearchParams();
		params.set("loginName", loginName);
		params.set("pwd", pwd);
		return this.get("Login", params);
	}

	SetAdminStatus(adminId: string, status: AdminStatusType): Observable<Boolean> {
		let params = new URLSearchParams();
		params.set("adminId", adminId);
		params.set("status", status.toString());
		return this.get("SetAdminStatus", params);
	}

	SetAdminInClient(adminId: string, clientId: string): Observable<Boolean> {
		let params = new URLSearchParams();
		params.set("adminId", adminId);
		params.set("clientId", clientId);
		return this.get("SetAdminInClient", params);
	}

	SetAdminInProject(adminId: string, pjid: string): Observable<Boolean> {
		let params = new URLSearchParams();
		params.set("adminId", adminId);
		params.set("pjid", pjid);
		return this.get("SetAdminInProject", params);
	}

	DelAdminInClient(adminId: string, clientId: string): Observable<Boolean> {
		let params = new URLSearchParams();
		params.set("adminId", adminId);
		params.set("clientId", clientId);
		return this.get("DelAdminInClient", params);
	}

	DelAdminInProject(adminId: string, pjid: string): Observable<Boolean> {
		let params = new URLSearchParams();
		params.set("adminId", adminId);
		params.set("pjid", pjid);
		return this.get("DelAdminInProject", params);
	}

	
	UpdateAdminInClient(adminId:number, clients: any[]) :Observable<Boolean> {
		let AdminInClient: any[] = [];

		clients.forEach(e => {
			AdminInClient.push({AdminId: adminId, ClientId: e.ClientId});
		});

		let param : {[key:number] : any[] } = {};
		param[adminId] = AdminInClient;

		return this.post("UpdateAdminInClient", param);
	}

	UpdateAdminInProject(adminId:number, projects: any[]) :Observable<Boolean> {
		let AdminInPjId: any[] = [];

		projects.forEach(e => {
			AdminInPjId.push({AdminId: adminId, PjId: e.PjId});
		});

		let param : {[key:number] : any[] } = {};
		param[adminId] = AdminInPjId;

		return this.post("UpdateAdminInProject", param);
	}
}
@Injectable()
export class IndicatorClassifyService extends SService{


	constructor(protected slimService:SlimLoadingBarService,protected http: Http ) {

		super(slimService,http, "IndicatorClassify");
	 }
	 GetIndicators(pjid:string,moduleid:string,parentid:string): Observable<ComponentTree> {
		let params = new URLSearchParams();
		params.set("pjid", pjid.toString());
		params.set("moduleid", moduleid.toString());
		params.set("parentid", parentid.toString());
		return this.get("GetIndicators", params);;
	 }

}

@Injectable()
export class ClientService extends SService{


	constructor(protected slimService:SlimLoadingBarService,protected http: Http ) {

		super(slimService,http, "Client");
	 }
	  GetClientByAdminId(adminId:number): Observable<any> {
		let params = new URLSearchParams();
		params.set("adminId", adminId.toString());
		return this.get("GetClientByAdminId", params);
	 }

	 GetClientById(clientId: any): Observable<any> {
		let params = new URLSearchParams();
		params.set("clientId", clientId.toString());
		return this.get("GetClientById", params);
	 }
}

@Injectable()
export class UserService extends SService {



	constructor(protected slimService:SlimLoadingBarService,protected http: Http ) {

		super(slimService,http, "User");
	 }
	GetUsersForProject(PjId: number): Observable<any>{
		let params = new URLSearchParams();
		params.set("PjId", PjId.toString());
		return this.get("GetUsersForProject", params);
	}

	SetUserFilter(KeyValuePair: any): Observable<boolean>{
		return this.post("SetUserFilter", KeyValuePair);
	}

	GetUserFilter(PjId: number): Observable<any[]> {
		let params = new URLSearchParams();
		params.set("PjId", PjId.toString());
		return this.get("GetUserFilter", params);
	}
}
@Injectable()
export class AuthService extends SService {



	constructor(protected slimService:SlimLoadingBarService,protected http: Http ) {
		super(slimService,http, "Auth");
	 }
	 SendEmail(userName:string){
		let params = new URLSearchParams();
		params.set("userName",userName);
		 return this.get("SendEmail",params);
	 }
	 UpdatePassword(userId:string,oldPassword:string,newPassword:string){
		let params = new URLSearchParams();
		params.set("userId",userId);
		params.set("oldPassword",oldPassword);
		params.set("newPassword",newPassword);
		return this.http.get(this.apiUrl("UpdatePassword"),{search:params}).catch(err=>Observable.of(err))	;
	 }
}

@Injectable()
export class VariableService extends SService{

	constructor(protected slimService:SlimLoadingBarService,protected http: Http ) {

		super(slimService,http, "Variable");
	 }
	  GetVariables(pjId:string): Observable<any> {
		let params = new URLSearchParams();
		params.set("pjId", pjId.toString());
		return this.get("GetVariables", params);
	 }

	 GetVariablesAndValues(PjId: number): Observable<any> {
		 let params = new URLSearchParams();
		 params.set("PjId", PjId.toString());
		 return this.get("GetVariablesAndValues", params);
	 }

	 Update(variablesAndValue: any): Observable<Boolean>{
		 return this.post("Update", variablesAndValue);
	 }


	GetUltimateQuestionAndUserToOption(pjid: any, userpassportid: number): Observable<any>{
		 let params = new URLSearchParams();
		 params.set("pjid", pjid.toString());
		 params.set("userpassportid", userpassportid.toString());
		 return this.get("GetUltimateQuestionAndUserToOption", params);
	 }

}

@Injectable()
export class SuggestionsService extends SService{

	constructor(protected slimService:SlimLoadingBarService,protected http: Http ) {

		super(slimService,http, "Suggestions");
	 }
	 GetSuggestionsByPjId(PjId: number): Observable<any> {
		  let params = new URLSearchParams();
		  params.set("PjId", PjId.toString());
		 return this.get("GetSuggestionsByPjId", params);
	 }

	 GetSuggestionsWithoutLabel(PjId: number): Observable<any>{
		 let params = new URLSearchParams();
		 params.set("PjId", PjId.toString());
		 return this.get("GetSuggestionsWithoutLabel", params);
	 }

	 Save(suggestins: any[]): Observable<boolean>{
		 return this.post("Save", suggestins);
	 }

	 GetSuggestionList(filter: any): Observable<any>{
		 return this.post("GetSuggestionList", filter);
	 }

	 GetSuggestionListHeader(PjId: number):Observable<any> {
		 let params = new URLSearchParams();
		 params.set("PjId", PjId.toString());
		 return this.get("GetSuggestionListHeader", params);
	 }

	 GetRespDatas(pjid: number, respId: number): Observable<any>{
		  let params = new URLSearchParams();
		  params.set("pjid", pjid.toString());
		  params.set("respId", respId.toString());
		  return this.get("GetRespDatas", params);
	 }

	 GetRespCount(filter: any): Observable<number> {
		 return this.post("GetRespCount", filter);
	 }

	 IsShowDetail(pjid: number): Observable<boolean> {
		 let params = new URLSearchParams();
		 params.set("pjid", pjid.toString());
		 return this.get("ShowDetail", params);
	 }
}

@Injectable()
export class SuggestionsLabelService extends SService{



	constructor(protected slimService:SlimLoadingBarService,protected http: Http ) {

		super(slimService,http, "SuggestionsLabel");
	 }
	 GetLabelsByPjId(PjId: number): Observable<any> {
		 let params = new URLSearchParams();
		 params.set("PjId", PjId.toString());
		 return this.get("GetLabelsByPjId", params);
	 }

	 AddLabel(label: any, suggestions: any[]): Observable<any>{
		 let params: {Key:any, Value:any} = {Key: label, Value: suggestions};
		 return this.post("AddLabel", params);
	 }

	 UpdateLabel(label: any, suggestions: any[]): Observable<any>{
		 let params: {Key:any, Value:any} = {Key: label, Value: suggestions};
		 return this.post("UpdateLabel", params);
	 }

}

@Injectable()
export class AlgorithmService extends SService{
	private _key = "Algorithm";

	

	constructor(protected slimService:SlimLoadingBarService,protected http: Http ) {

		super(slimService,http,"Algorithm");
	 }
    SaveAlgorithmView(views:AlgorithmViewInfo[]){
		return this.post("SaveAlgorithmView",views);
	}
	Delete(algorithmId:string){
			let params = new URLSearchParams();
		    params.set("algorithmId", algorithmId);
		    return this.delete("Delete",params);
	}
	GetResults(filter: RequestParam): Observable<Result> {
		return this.post("GetResults",filter);
	}
GetResult(filter: RequestParam): Observable<ResultDefaultData> {
		return this.post("GetResult",filter);
	}
	GetAlgorithmViews(groupId: string): Observable<AlgorithmViewInfo[]> {
		let params = new URLSearchParams();
		params.set("groupId", groupId);
		return this.http.get(this.apiUrl("GetAlgorithmViews"), { search: params })
			.map(responce => <AlgorithmViewInfo[]>responce.json())
			.catch(error => {
				return Observable.throw(error)
			});
	}
   GetAlgorithmView(algorithmId: string): Observable<AlgorithmViewInfo[]> {
		let params = new URLSearchParams();
		params.set("algorithmId", algorithmId);
		return this.http.get(this.apiUrl("GetAlgorithmView"), { search: params })
			.map(responce => <AlgorithmViewInfo[]>responce.json())
			.catch(error => {
				return Observable.throw(error)
			});
	}
	GetAlgorithmNewView(pjid: string, groupId: string, algType: number,algorithmId:string): Observable<AlgorithmViewInfo> {
		let params = new URLSearchParams();
		params.set("pjid", pjid);
		params.set("groupId", groupId);
		params.set("algType", algType.toString());
		params.set("algorithmId", algorithmId);
		return this.http.get(this.apiUrl("GetAlgorithmNewView"), { search: params })
			.map(responce => <AlgorithmViewInfo>responce.json())
			.catch(error => {
				return Observable.throw(error)
			});
	}

	GetAlgorithmTypeList(): Observable<String[]> {
		let params = new URLSearchParams();
		return this.http.get(this.apiUrl("GetAlgorithmTypeList"), { search: params })
			.map(responce => <String[]>responce.json())
			.catch(error => {
				return Observable.throw(error)
			});
	}
	GetAlgorithmPropertyTypeList(): Observable<String[]> {
		let params = new URLSearchParams();
		return this.http.get(this.apiUrl("GetAlgorithmPropertyTypeList"), { search: params })
			.map(responce => <String[]>responce.json())
			.catch(error => {
				return Observable.throw(error)
			});
	}
}

@Injectable()
export class ComponentService extends SService {
	private _key = "Component";


	constructor(protected slimService:SlimLoadingBarService,protected http: Http ) {

		super(slimService,http, "Component");
	 }
	AddComponent(component: ComponentInfo):Observable<string>{
		return this.post("AddComponent",component);
	}

	// GetComponents(pjid: string,moduleId:string): Observable<ComponentInfo[]> {
	// 	let params = new URLSearchParams();
	// 	params.set("pjid", pjid);
	// 	params.set("moduleId", moduleId);

	// 	return this.http.get(this.apiUrl("GetComponents"), { search: params })
	// 		.map(responce => <ComponentInfo[]>responce.json())
	// 		.catch(error => {
	// 			return Observable.throw(error)
	// 		});
	// }
    GetComponentsNew(pjId:string,moduleId: string): Observable<ComponentInfo[]> {
		let params = new URLSearchParams();
		params.set("moduleId", moduleId);
		params.set("pjId", pjId);
		return this.http.get(this.apiUrl("GetComponentsNew"), { search: params })
			.map(responce => <ComponentInfo[]>responce.json())
			.catch(error => {
				return Observable.throw(error)
			});
	}
	UpdateComponent(component: ComponentInfo): Observable<Boolean> {
		return this.http.post(this.apiUrl("UpdateComponent"), JSON.stringify(component), { headers: this.headers })
			.map(responce => <Boolean>responce.json())
			.catch(error => {
				return Observable.throw(error)
			});
	}
	GetComponentsFront(pjid: string,type:ModuleType): Observable<ComponentInfo[]> {
		let params = new URLSearchParams();
		params.set("pjid", pjid);
		params.set("type", type.toString());
		return this.get("GetComponentsFront",params);
	}
}

@Injectable()
export class TaskService extends SService{


	constructor(protected slimService:SlimLoadingBarService,protected http: Http ) {

		super(slimService,http, "Task");
	 }
	 Add(filter: RequestParam): Observable<any> {
		return this.post("AddExportDataTask",filter);
	  }
	  AddGetResult(filter: RequestParam): Observable<any> {
		  return this.post("AddGetResultTask",filter);
		}
	  GetNewTaskLog(taskId: string):Observable<any>{
		  let params = new URLSearchParams();
		  params.set("taskId", taskId);
		  return this.get("GetNewTaskLog", params);
	  }
	  Get(taskId:string):Observable<any>{
		  let params = new URLSearchParams();
		  params.set("taskId", taskId);
		  return this.get("Get",params);
	  }
	
}
@Injectable()
export class ProjectService extends SService{
	constructor(protected slimService:SlimLoadingBarService,protected http: Http ) {

		super(slimService,http, "Project");
	 }

	GetPjIdByExternalId(externalid: number): Observable<string>{
		let params = new URLSearchParams();
		params.set("externalid", externalid.toString());
		return this.get("GetPjIdByExternalId", params);
	}
	
	GetProject(pjid: string): Observable<any> {
		let params = new URLSearchParams();
		params.set("pjid", pjid);
		return this.get("GetProject", params);
	}

	GetProjects(clientId: number):Observable<any>{
		let params = new URLSearchParams();
		params.set("clientId", clientId.toString());
		return this.get("GetProjects", params);
	}
	
	GetUserProjects(uid: number): Observable<any> {
		let params = new URLSearchParams();
		params.set("uid", uid.toString());
		return this.get("GetUserProjects", params);
	}
	GetUserProjectsFront(uid: number): Observable<any> {
		let params = new URLSearchParams();
		params.set("uid", uid.toString());
		return this.get("GetUserProjectsFront", params);
	}
	ProjectsInClient(adminId: number, clients: any[]):Observable<any> {
		let ClientIds: string[] = [];
		clients.forEach(c => {
			ClientIds.push(c.ClientId);
		});

		let param : {[key:number] : string[] } = {};
		param[adminId] = ClientIds;
		
		return this.post("ProjectsInClient", param);
	}

	GetProjectHasModules(module: ModuleType): Observable<any> {
		let param = new URLSearchParams();
		param.set("module", module == null? "0" : module.toString());
		return this.get("GetProjectHasModules", param);
	}

	GetModules(): Observable<any>{
		return this.get("GetModules", null);
	}
}

@Injectable()
export class ExpressionService extends SService{


	constructor(protected slimService:SlimLoadingBarService,protected http: Http ) {

		super(slimService,http, "Expression");
	 }
	GetGlobalFilter(pjId: number, type: string): Observable<any>{
		let params = new URLSearchParams();
		params.set("pjId", pjId.toString());
		params.set("type", type);
		return this.get("GetGlobalFilter", params);
	}

	GetExpressions(ExpGroupId: number): Observable<any>{
		let params = new URLSearchParams();
		params.set("ExpGroupId", ExpGroupId.toString());
		return this.get("GetExpressions", params);
	} 

	GetGlobalFilterExp(pjId: number, type: string):Observable<any> {	
		let params = new URLSearchParams();
		params.set("pjId", pjId.toString());
		params.set("type", type);
		return this.get("GetGlobalFilterExp", params);
	}
	
	AddGlobalFilter(filter: any, expressions: any[]): Observable<boolean>{
		let params: {Key:any, Value:any} = {Key: filter, Value: expressions};
		return this.post("AddGlobalFilter", params);
	}

	UpdateGlobalFilter(filter: any, expressions: any[]): Observable<any>{
		let params: {Key:any, Value:any} = {Key: filter, Value: expressions};
		return this.post("UpdateGlobalFilter", params);
	}

	DelGlobalFilter(filter: any): Observable<Boolean>{
		return this.post("DelGlobalFilter", filter);
	}

	AddUserFilter(filter: any, expressions: any[]): Observable<Boolean>{
		let params: {Key:any, Value:any} = {Key: filter, Value: expressions};
		return this.post("AddUserFilter",params);
	}

	UpdateUserFilter(filter: any, expressions: any[]): Observable<Boolean>{
		let params: {Key:any, Value:any} = {Key: filter, Value: expressions};
		return this.post("UpdateUserFilter",params);
	}

	GetUserFilter(PjId: number):Observable<any>{
		let params = new URLSearchParams();
		params.set("PjId", PjId.toString());
		return this.get("GetUserFilter", params);
	}

}


@Injectable()
export class ReportService {
	private _key = "Report";
	private headers = new Headers({ 'Content-Type': 'application/json' });
	private apiUrl(method: string) {
		return GlobalVars.apiUrl + this._key + '/' + method;
	}
	constructor(private http: Http) {

	}
}

@Injectable()
export class AlertService extends SService{
	constructor(protected slimService:SlimLoadingBarService,protected http: Http ) {
		super(slimService,http, "Alert");
	 }

	GetAertRespCount(params: any): Observable<number> {
		return this.post("GetAertRespCount", params);
	}

 	GetRespAlertInfo(PjId: string, RespId: string, InfoType: string): Observable<Array<any>> {

		let params = new URLSearchParams();
		params.set("PjId", PjId.toString());
		params.set("RespId", RespId.toString());
		params.set("InfoType", InfoType);
		return this.get("GetRespAlertInfo", params);
	 }

	 GetAlertSituation(param: RequestParam): Observable<any> {
		return this.post("GetAlertSituation", param);
	 }
	 GetAlertSituationForJRJHome(param: RequestParam): Observable<any> {
		return this.post("GetAlertSituationForJRJHome", param);
	 }
 	GetAlertList(param: RequestParam): Observable<Array<any>>{
		return this.post("GetAlertList", param);
	 }
	 GetTHead(PjId: any): Observable<Array<any>>{
		let params = new URLSearchParams();
		params.set("PjId", PjId.toString());
		return this.get("GetTHead", params);
	 }

	 GetRespAlert(pjid: any, respid: number):Observable<any> {
		let params = new URLSearchParams();
		params.set("pjid", pjid.toString());
		params.set("respid", respid.toString());
		return this.get("GetRespAlert", params);
	}

	HasOperationToResp(pjid: any, userid: number, respid: number): Observable<boolean>{
		let params = new URLSearchParams();
		params.set("pjid", pjid.toString());
		params.set("userid", userid.toString());
		params.set("respid", respid.toString());
		return this.get("HasOperationToResp", params);
	}

	SaveLog(log: any): Observable<any>{
		return this.post("SaveLog",log);
	}

	UpdateAlertRespStatus(pjid: any, respid: number, status: string):Observable<boolean> {
		let params = new URLSearchParams();
		params.set("pjid", pjid.toString());
		params.set("respid", respid.toString());
		params.set("status", status.toString());
		return this.get("UpdateAlertRespStatus", params);
	}

	GetProcessLogs(pjid: any, respid: number): Observable<Array<any>>{
		let params = new URLSearchParams();
		params.set("pjid", pjid.toString());
		params.set("respid", respid.toString());
		return this.get("GetProcessLogs", params);
	}

	AddUserToAlertResp(userToAlertResp: any): Observable<boolean> {
		return this.post("AddUserToAlertResp", userToAlertResp);
	}

	GetAlertStatistics(param: RequestParam): Observable<Array<any>> {
		return this.post("GetAlertStatistics", param);
	}
}

@Injectable()
export class FRelationService extends SService{
	constructor(protected slimService:SlimLoadingBarService,protected http: Http ) {
		super(slimService,http, "FRelation");
	 }

	GetUltimateQuestion(PjId: number): Observable<any> {
		let params = new URLSearchParams();
		params.set("PjId", PjId.toString());
		return this.get("GetUltimateQuestion", params);
	}

	GetUserToOptions(PjId: number, UserPassportId: number): Observable<any[]>{
		let params = new URLSearchParams();
		params.set("PjId", PjId.toString());
		params.set("UserPassportId", UserPassportId.toString());
		return this.get("GetUserToOptions", params);
	}

	GetGroupVariables(pjid: any):Observable<Array<any>>{
		let params = new URLSearchParams();
		params.set("pjid", pjid.toString());
		return this.get("GetGroupVariables", params);
	}

	GetUserRights(PjId: any, UserPassportId: number): Observable<string[]>{
		let params = new URLSearchParams();
		params.set("PjId", PjId.toString());
		params.set("UserPassportId", UserPassportId.toString());
		return this.get("GetUserRights", params);
	}

	GetConcernTypes(pjid: any): Observable<Array<any>>{
		let params = new URLSearchParams();
		params.set("pjid", pjid.toString());
		return this.get("GetConcernTypes", params);
	}

	GetUsersByFid(PjId: any, Fid: number, varvalueid: number): Observable<Array<any>>{
		let params = new URLSearchParams();
		params.set("PjId", PjId.toString());
		params.set("Fid", Fid.toString());
		params.set("varvalueid", varvalueid.toString());
		return this.get("GetUsersByFid", params);

	}

	HasOperationToResp(pjid: number, userid: number, respid: number): Observable<boolean>{
		let params = new URLSearchParams();
		params.set("pjid", pjid.toString());
		params.set("userid", userid.toString());
		params.set("respid", respid.toString());
		return this.get("HasOperationToResp", params);
	}

	GetUserToAlertResp(pjid: number, respid: number): Observable<Array<any>>{
		let params = new URLSearchParams();
		params.set("pjid", pjid.toString());
		params.set("respid", respid.toString());
		return this.get("GetUserToAlertResp", params);
	}

	GetProcessLogs(pjid: number, respid: number): Observable<Array<any>>{
		let params = new URLSearchParams();
		params.set("pjid", pjid.toString());
		params.set("respid", respid.toString());
		return this.get("GetProcessLogs", params);
	}

	SaveLog(log: any): Observable<any>{
		return this.post("SaveLog",log);
	}

	UpdateAlertRespStatus(pjid: number, respid: number, status: string):Observable<boolean> {
		let params = new URLSearchParams();
		params.set("pjid", pjid.toString());
		params.set("respid", respid.toString());
		params.set("status", status.toString());
		return this.get("UpdateAlertRespStatus", params);
	}

	AddUserToAlertResp(userToAlertResp: any): Observable<boolean> {
		return this.post("AddUserToAlertResp", userToAlertResp);
	}

	GetAertRespCount(pjid: string, varvalueid: number, status: string): Observable<number> {
		let params = new URLSearchParams();
		params.set("pjid", pjid);
		params.set("varvalueid", varvalueid.toString());
		params.set("status", status.toString());
		return this.get("GetAertRespCount", params);
	}

	GetRespAlert(pjid: number, respid: number):Observable<any> {
		let params = new URLSearchParams();
		params.set("pjid", pjid.toString());
		params.set("respid", respid.toString());
		return this.get("GetRespAlert", params);
	}
}

@Injectable()
export class PagingService{

    private paged:Subject<any> = new Subject<any>();
    paged$ = this.paged.asObservable()
    onPaged(res : any){
        this.paged.next(res);
    }

    private initPaging:Subject<any> = new Subject();
    initPaging$ = this.initPaging.asObservable();
    onInitPaging(res : any){
        this.initPaging.next(res);
    }
}

@Injectable()
export class SmsSyatemService extends SService {
	constructor(public slimService:SlimLoadingBarService,protected http:Http ) {
		super(slimService,http, "SmsSyatem");	
	}

	SendAssignEmail(userid: number, pjid: number): Observable<any>{
		let params = new URLSearchParams();
		params.set("userid", userid.toString());
		params.set("pjid", pjid.toString());
		return this.get("SendAssignEmail", params);
	}
}

@Injectable()
export class ModuleService extends SService{
	constructor(public slimService:SlimLoadingBarService,protected http:Http ) {
		super(slimService,http, "Module");	
	}

	GetModules(clientid: number): Observable<Array<any>>{
		let params = new URLSearchParams();
		params.set("clientid", clientid.toString());
		return this.get("GetModules", params);
	}

	GetProjectModules(pjid: string, userid: string): Observable<Array<any>> {
		let params = new URLSearchParams();
		params.set("pjid", pjid);
		params.set("userid", userid);
		return this.get("GetProjectModules", params);
	}
    GetProjectModulesFront(pjid: string): Observable<Array<any>> {
		let params = new URLSearchParams();
		params.set("pjid", pjid);
		return this.get("GetProjectModulesFront", params);
	}
	Add(amodule: any): Observable<any>{
		return this.post("Add", amodule);
	}

	GetAllContentType(): Observable<Array<any>>{
		return this.get("GetAllContentType", null);
	}
}

@Injectable()
export class DataSearchService extends SService{
	constructor(public slimService:SlimLoadingBarService,protected http:Http ) {
		super(slimService,http, "DataSearch");	
	}

	GetTHead(userid: string): Observable<Array<any>>{
		let params = new URLSearchParams();
		params.set("userid", userid);
		return this.get("GetTHead", params);
	}

	GetColumnDatas(externalid: any, sccode: any, infotype: string): Observable<any>{
		let params = new URLSearchParams();
		params.set("externalid", externalid.toString());
		params.set("sccode", sccode.toString());
		params.set("infotype", infotype.toString());
		return this.get("GetColumnDatas", params);
	}

	ExportExcel(headers: any, datas: any, searchKey: string): Observable<boolean> {
		let data = {Headers:headers, SysSurveyListItems: datas, SearchKey: searchKey};
		return this.post("ExportExcel", data);
	}
}




