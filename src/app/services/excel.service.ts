// tslint:disable-next-line:import-destructuring-spacing
import { Injectable , ElementRef} from '@angular/core';
import * as FileSaver from 'file-saver';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class ExcelService {

  constructor() { }

  public exportAsExcelFile(table: any,excelFileName: string): void {
     let uri = 'data:application/vnd.ms-excel;base64,'
                //, template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">{table}</html>'
                , base64 = function (s) { return window.btoa((window as any).unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }

                var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
                let result = this.excelExportHtml(table.innerHTML, true);
                result.done(function (data) {
                    //window.location.href = uri + base64(data);
                    let blob = new Blob([data], {
                        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                    });
                    FileSaver.saveAs(blob, 'Report.xls');
                });
  }
    private excelExportHtml (table: ElementRef, includeCss: boolean) {
            if (includeCss) {
                let styles = [];

                //grab all styles defined on the page
                $("style").each(function (index, domEle) {
                    styles.push($(domEle).html());
                });

                //grab all styles referenced by stylesheet links on the page
                let ajaxCalls = [];
                $('[rel=stylesheet]').each(function () {
                    ajaxCalls.push($.get(this.href, '', function (data) {
                        styles.push(data);
                    }));
                });

                return $.when.apply(null, ajaxCalls)
                        .then(function () {
                            return "<html><style type='text/css'>" + styles.join('') + '</style>' + table + '</html>';
                        });
            }
            else {
                return $.when({ owcHtml: table })
                        .then(function (result) {
                            return '<html>' + result.owcHtml + '</html>';
                        });
            }
        }
}
