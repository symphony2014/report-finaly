    export const enum AdminStatusType {
        Usable = 1,
        Disabled = 0
    }
    export interface Admins {
        LoginName: string;
        AdminId: string;
    }
    export interface Project{
        PjId:number;
        Name:string;
        FWDate:string;
    }
    export interface Theme {
        CreateDate: Date;
        Description: string;
        InUsed: boolean;
        JsonContext: string;
        LastUpdate: Date;
        ThemeId: number;
        ThemeLabel: string;
        ThemeType: number;
    }