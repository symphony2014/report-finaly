﻿	import {Observable,Subscription } from "rxjs";
	export const enum FormatType{
		Number,
		String
	}
	export const enum AlgorithmPropertyShowStyle {
		Multi = 0,
		Single = 1,
		Number = 2,
		Period = 3
	}
	export const enum AlgorithmPropertyType {
		None = 0,
		MapVar = 1,
		CountVar = 2,
		GruopVar = 3,
		ConditionVar = 4,
		ShowQuantity = 5,
		ShowDateUnitType = 6
	}
	export const enum AlgorithmType {
		None = 0,
		QuestionMap = 1,
		Top3 = 2,
		NPS = 3,
		NPSTrend = 4
	}
	export const enum ComponentStyleType {
		Blcok = 1,
		Chart = 2,
		Table = 3,
		Filter = 4
	}
	export const enum ValueShowType {
	    Text,
        Option,
        Datetime,
        SysDatePeriod
	}
	export const enum ModuleType {
		Summary = 1,
		Dashboard = 2,
		Overview = 4,
		SampleData = 8,
		Export = 16,
		ClosedCycle = 32
	}
	export const enum ValueType {
		Code = 0,
		Char = 1,
		Numerical = 2,
		Date = 3
	}
    export const enum ProjectStatusType{
		Error = -99,
        Delete = -2,
        Close = -1,
        Init = 0,
        Complete = 1
	}

export	interface AlgorithmPropertyViewInfo {
		AlgPropertyType?: AlgorithmPropertyType;
		ShowStyle?: AlgorithmPropertyShowStyle;
		Values?: string[];
	}
export interface Project{
	PjId:number,
	Name:string,
	Status:ProjectStatusType,
     Modules:ModuleType
}
export interface Variable{
	VarId?:string;
	ReportCode?:string;
}
	export interface AlgorithmViewInfo {
		AlgorithmGroupId?: string;
		AlgorithmId?: string;
		AlgorithmProperties?: AlgorithmPropertyViewInfo[];
		AlgorithmType?: AlgorithmType;
		Pjid?: string;
		Text?: string;
		Type?: string;
		MapKey?:string;
	}
	export interface ComponentInfo {
		BindStrategy?:string;
   		AlgorithmGroupId?:string;
		ChildComponents?: ComponentInfo[];
		ComponentId?: string;
		ShowText?: string;
		Templates?: TemplateInfo[];
		Schema?:string;
		Style?:string;
		Type?:ComponentStyleType|string;
		Pjid?:string;
		ModuleType?:ModuleType;
		Show?:boolean;
		AsyncUrl?:string;
		cancel:Subscription;
	}
	export interface SingleRequestParam {
		Filters?: RequestParamFilter[];
		Page?: number;
		PageSize?: number;
		Pjid?: string;
		UserId?: string;
		AlgorithmId?:string;
	}
	export interface RequestParam {
		Filters?: RequestParamFilter[];
		Page?: number;
		PageSize?: number;
		Pjid?: string;
		UserId?: string;
		AlgorithmGroupId?:string;
	}
   export interface ComponentTree{
	   ShowText?:string;
	   ComponentId?:string;
	   HasChildren?:boolean;
	   Children?:ComponentTree[];
   }
	export interface RequestParamFilter {
		Default?:any[];
		DateEnd?: string | Date;
		DateStart?: string | Date;
		OptionValues?: any[] | number;
		ShowType?:string;
		TextValue?: string;
		VarId?: string;
		ParamName?:string;
		options?:{key:string,value:string}[];
		async?:{Items:any};
		change?:any;
		Multiple?:boolean;
		IsSearch?:boolean;
		NotNull?:boolean;
		status?:string;
		dependency?:any;
		NotCrossYear?:boolean;
	}
	export interface TemplateInfo {
		AlgorithmView?: AlgorithmViewInfo;
		ComponentId?: number;
		CreateDate?: Date;
		Index?: number;
		JsonText?: string;
		LastUpdate?: Date;
		Pjid?: number;
		TemplateId?: number;
	}
	export interface ResultDataItem {
		Name?: string;
		Value?: string;
	}
	export interface ResultDataItems {
		Name?: string;
		Values?: string[];
	}
	export interface ResultDefaultData {
		AlgorithmId?: string;
		Text?: string;
		Legend?: string[];
		Series?: ResultDataItems[];
		XAxis?: string[];
	}
	export interface Result{
		Id:string;
		Text:string;
		Datas:ResultDefaultData[];
	}
   export interface PageData<T>{
		list:T[];
		totalRecord:number;
		totalPage:number;
   }
export class Params {
    constructor(public IsSA?: boolean, public AdminId?: number, public ClientId?:number){}
}