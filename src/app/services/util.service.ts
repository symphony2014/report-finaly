import { FilterStartEvent } from "app/theme/components/ipsosComponent/component.event";
import { Injectable, EventEmitter } from '@angular/core';
import { LocalStorageService } from "app/services/angular.service";
import { GlobalVars } from "app/services/global.vars";
export class JsonUtil {
    static format(text:string):string{
        // if(text){
        //    return text.replace(/([\u4E00-\u9FA5\uF900-\uFA2D\w\%]+)(?=\s*:)/g,"\"$1\"")
        //               .replace(/\'/g,"\"")
        //               .replace(/，/g,',')
        //               //.replace(/(\n)*/g,"");
        // }
        return text;
    }
}
export class DateFormat{
    static dateFix(date:Date):string{
            return `${date.getFullYear()}/${date.getMonth()}/${date.getDay()}`;
    }
    static toLocalString(date:Date):string{
           return date.toISOString().slice(0,10); 
    }
    static IEFixedDate(date:Date):string{
        return date.toLocaleDateString().replace("年","/").replace("月","/").replace("日","").replace(/[\u200E]/g, "");
    }
}
export class Format{

    static percent(value:string):string{
        try {
          if (value ){
          return (value+"0").match(/\d{2}/g)[0].replace(/0(?=\d)/g,"")+"%";
          }
        } catch (error) {
         return "非数字"; 
        }

    }
    static keep2Digit(num:any){
        var sign = num?num<0?-1:1:0;
        num = num  * sign + ''; // poor man's absolute value
        var dec = num.match(/\.\d+$/);
        var int = num.match(/^[^\.]+/);
        
        var formattedNumber = (sign < 0 ? '-' : '') + ("0" + int).slice(-2) + (dec !== null ? dec : '');
        return formattedNumber
    }
}
export class ImageUtil{
    static text2Image (str:string):any {
                var canvas = document.createElement("canvas");
                var contx = canvas.getContext("2d");
                contx.canvas.width = contx.measureText(str).width*5;
                contx.fillStyle = "#fff";
                contx.font = ' 20pt Calibri';
                contx.fillRect(0, 0, contx.canvas.width*5, contx.canvas.height);
                contx.fillStyle = "#000";
                contx.fillText(str, 0, 22);
                return contx.canvas.toDataURL("image/jpeg");
            }
}
@Injectable()
export class GlobalUtil{
    constructor(private local:LocalStorageService){}
 public appendGlobal(event:FilterStartEvent,componentId?:string):FilterStartEvent{
    event.filter.Pjid=this.local.get("id");
    event.filter.UserId= GlobalVars.getAuth().id;
    event.filter.AlgorithmGroupId= componentId;
    return event;
  }
}