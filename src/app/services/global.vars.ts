import { Injectable } from '@angular/core';
import { ValueShowType } from "app/services/models";
import { SlimLoadingBarService } from "ng2-slim-loading-bar";

export  class GlobalVars{
 
    static get apiUrl(){
      return (document.getElementById("apiUrl") as any).content;
    }
    static get apiHost(){
      return (document.getElementById("apiHost") as any).content;
    }

    static get apiDataSearchUrl() {
       return (document.getElementById("apiDataSearchUrl") as any).content;
    }

    static get title() {
      return (document.getElementById("title") as any).content;
    }
  

  static moduleTitle="test";

   static filterShowType:string[]=["text","option","datetime","sysdateperiod"];
  static componentTypes:string[]=["Cord","Filter","Table","Pie","Gauge","Map","Radar","Heatmap","Graphic","Html","Customize"];

  
  static userId:number;

 

  static setAuth(res:any){

     localStorage.setItem("Auth",JSON.stringify(res)); 
  }
  static getAuth():any{
    return JSON.parse(localStorage.getItem("Auth"));
  }
   
  
}
