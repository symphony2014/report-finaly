import { NgModule, ApplicationRef, enableProdMode } from '@angular/core';
import {ModalModule} from 'ng2-bootstrap/modal';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { removeNgStyles, createNewHosts, createInputTransfer } from '@angularclass/hmr';
import { Angular2TokenService } from 'angular-token-report';
import { GlobalVars } from "app/services/global.vars";

import { AdminService, LocalStorageService,  AlgorithmService } from "./services/angular.service";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
//import { COMPILER_PROVIDERS } from '@angular/compiler';


/*
 * Platform and Environment providers/directives/pipes
 */
import { ENV_PROVIDERS } from './environment';
import { routing } from './app.routing';

// App is our top level component
import { App } from './app.component';
import { AppState, InternalStateType } from './app.service';
import { GlobalState } from './global.state';
import { NgaModule } from './theme/nga.module';
import { PagesModule } from './pages/pages.module';
import { LoginModule } from './login/login.module';
import { GetNamePipe } from "./pipe";


import { NgZorroAntdModule } from 'ng-zorro-antd';
import { DialogZoom } from "app/theme/components/ipsosComponent/components/dialog.zoom.component";
import { DialogAlert } from 'app/theme/components/filter/components/alert.component';
import { SimpleDialogAlert } from 'app/theme/components/filter/components/simple.alert.component';


// Application wide providers
const APP_PROVIDERS = [
  AppState,
  GlobalState
];

export type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [App],
  declarations: [
    App,
    GetNamePipe,
    DialogZoom,
      DialogAlert,
      SimpleDialogAlert
  ],
  imports: [ // import Angular's modules
    ModalModule.forRoot(),
    BrowserModule,
    HttpModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgaModule.forRoot(),
    NgZorroAntdModule.forRoot(),
    PagesModule,
    LoginModule,
    routing,
    HttpModule,
    BrowserAnimationsModule,
  ],
  providers: [ // expose our Services and Providers into Angular's dependency injection
    Angular2TokenService,
    AdminService,
    LocalStorageService,
    AlgorithmService,

    ENV_PROVIDERS,
    APP_PROVIDERS,
    //COMPILER_PROVIDERS,

  

    ENV_PROVIDERS,
    APP_PROVIDERS,



  ],
  entryComponents:[DialogZoom,DialogAlert,SimpleDialogAlert]
     
})

export class AppModuleShared {

  constructor(private _tokenService:Angular2TokenService, public appRef: ApplicationRef, public appState: AppState) {
    this._tokenService.init({
      apiBase:GlobalVars.apiHost,
      signInPath:"token",
      signOutPath:"/api/auth/delete",
      globalOptions: {
            headers: {
                'Content-Type':'application/json',
                'Accept':      'application/json',
            }
        }
    });
  }

  hmrOnInit(store: StoreType) {
    if (!store || !store.state) return;
    console.log('HMR store', JSON.stringify(store, null, 2));
    // set state
    this.appState._state = store.state;
    // set input values
    if ('restoreInputValues' in store) {
      let restoreInputValues = store.restoreInputValues;
      setTimeout(restoreInputValues);
    }
    this.appRef.tick();
    delete store.state;
    delete store.restoreInputValues;
  }

  hmrOnDestroy(store: StoreType) {
    const cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
    // save state
    const state = this.appState._state;
    store.state = state;
    // recreate root elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // save input values
    store.restoreInputValues = createInputTransfer();
    // remove styles
    removeNgStyles();
  }

  hmrAfterDestroy(store: StoreType) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }
}
