import { Component, Input } from '@angular/core';
import { SService } from '../services/angular.service';
import { PagingService } from '../services/angular.service';
import { Params } from '../services/models/IpsosReport.ModelBiz';

export class Button {
    constructor(public index: number, public isSelect: boolean = false) { }
}

@Component({
    selector: 'paging',
    templateUrl: './paging.html',
    styleUrls: ['./paging.scss'],
})
export class PagingComponent {
    // 实现分页接口的实例
    // private iPaging: IPaging;

    public keyWords: string;

    // 总页数
    public totalPage: number;
    // 总记录
    public totalRecord: number;
    // 所有页也按钮
    public pages: Button[] = [];
    // 当前选中的页
    private selectButton: Button;
    // 页的index
    private pageIndex:number;
    // 每页的行数
    private rowSize;

    private _sService: SService;

    private param: Params;

    /**
     * 构造器
     * @param adminService 
     */
    constructor(private _pService:PagingService) {
        
        _pService.initPaging$.subscribe((res) => {
            if (res == null) {
                this.totalPage = 0
                return;
            }
            this._sService = res.service;
            this.keyWords = res.keyWords;
            this.rowSize = res.rowSize;
            this.totalPage = res.totalPage;
            this.totalRecord = res.totalRecord;
            this.param = res.params;
            this.pageIndex = res.pageIndex;
            this.pages = [];
            if (this.totalRecord <= 0) return;
            this.setPages();
            this.selectButton = this.pages[this.pageIndex - 1];
            this.selectButton.isSelect = true;
        });
    }

     /**
    * 把总页变成一个数组，在页面能够循环出分页按钮
    */
    private setPages(): void {
        for (var i = 0; i < this.totalPage; i++) {
            this.pages.push(new Button(i, false));
        }
    }

    /**
     * 选择一页
     * @param btn 
     */
    paging(btn: Button) {
        if (this.selectButton === btn) return

        this.selectButton.isSelect = false;
        this.selectButton = btn;
        btn.isSelect = true;

        this.toLoadRemoteSource(btn);
    }

    /**
     * 4个分页导航按钮
     * @param val 
     */
    dirPage(val:number):void{

        var b: Button = this.selectButton;
        b.isSelect = false;
        var index: number = 0;

        switch (val) {
            case 0: // 首页
                index = 0;
                break;
            case 1: // 上一页
                index = b.index - 1;
            break;
            case 2: // 下一页
                index = b.index + 1;
            break; 
            case 3: // 末页
                index = this.pages.length - 1;
            break;
        }
        this.selectButton = this.pages[index];
        this.selectButton.isSelect = true;
        
        this.toLoadRemoteSource(this.selectButton);
    }

    /**
     * 从远程服务器获取数据
     * @param btn 
     */
    private toLoadRemoteSource(btn : Button): void{

        let pageIndex = btn.index + 1;

        let observable: any = this.keyWords ? this._sService.Search(pageIndex, this.rowSize, this.keyWords, this.param)
                                            : this._sService.Paging(pageIndex, this.rowSize, this.param);
                                            
        observable.subscribe((res: any) => {
            res.pageIndex = pageIndex;
            this._pService.onPaged(res);
        })
    }

}