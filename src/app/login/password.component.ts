import { Component, ViewChild,Input, ViewContainerRef, OnInit, Inject, OnChanges } from '@angular/core';
import {AuthService} from "app/services/angular.service";
import {Observable,ReplaySubject} from "rxjs";
import { GlobalVars } from "app/services/global.vars";
import { MdDialogRef,MD_DIALOG_DATA  } from "@angular/material";

@Component({
  selector: 'dialog-password',
  template:`
  <div *ngIf="loading;else complete" >
  发送邮件:
   <md-progress-bar mode="indeterminate"></md-progress-bar>
  </div>
  <ng-template #complete>
  {{messageReplay | async}}
  {{error | async}}
  <br/>
  请查收邮件最新密码，然后登录！({{count | async}})
  </ng-template>
   `
})
export class DialogPassword implements OnInit  {
    loading:boolean=true;
    message:Observable<string>;
    error:Observable<string>;
    count:Observable<number>;
    init:number=6;
    messageReplay:ReplaySubject<string>=new ReplaySubject<string>();
    constructor(private auth:AuthService,@Inject(MD_DIALOG_DATA)private data:string,public dialogRef: MdDialogRef<DialogPassword>){
    }
    ngOnInit(){
     this.message=this.auth.SendEmail(this.data).publish().refCount();
     this.message.subscribe(m=>this.messageReplay.next(m));
     this.messageReplay.subscribe(()=>this.loading=false);
     this.count = Observable.timer(0,1000).take(this.init).map(()=>{ 
       if(this.init==1){
         this.dialogRef.close();
       }
      return --this.init
      });
    }
}
