import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../theme/nga.module';

import { HttpModule } from '@angular/http';
import { Login } from './login.component';
import { routing }       from './login.routing';
import { DialogProjects } from "app/login/projects/dialog.projects.component";
import {MdProgressBarModule, MdDialogModule,MdAutocompleteModule,MdInputModule } from "@angular/material";
import {  ProjectService ,AuthService } from "app/services/angular.service";
import {DialogPassword} from "./password.component";

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgaModule,
    routing,
    HttpModule,
    MdDialogModule,
    MdAutocompleteModule,
    MdInputModule,
    MdProgressBarModule
  ],
  declarations: [
    Login,
    DialogProjects,
    DialogPassword
  ],
  providers:[
    ProjectService,
    AuthService,
],
   entryComponents:[DialogProjects,DialogPassword],
})
export class LoginModule {}
