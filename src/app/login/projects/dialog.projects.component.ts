import { Component, ViewChild, ViewContainerRef, OnInit, Inject, OnChanges } from '@angular/core';
import { VariableService, AlgorithmService, ProjectService, LocalStorageService } from "../../../app/services/angular.service";
import { MdDialogRef,MD_DIALOG_DATA  } from "@angular/material";
import { Variable, AlgorithmViewInfo, ComponentInfo } from "app/services/models";
import { Project } from "../../../app/services/models/IpsosReport.ModelSYS";
import { GlobalVars } from "../../../app/services/global.vars";
import { Router } from "@angular/router";
import { FormControl } from "@angular/forms";
import {BehaviorSubject,Observable} from 'rxjs';

@Component({
  selector: 'dialog-projects',
  templateUrl: './dialog.projects.html',
  styleUrls:["./dialog.projects.scss"]
})
export class DialogProjects implements OnInit {
 
    stateCtrl: FormControl;
    filteredStates: any;
    projects:Observable<Project[]>=new Observable<Project[]>(null);
    constructor(public dialogRef: MdDialogRef<DialogProjects>,private _pService:ProjectService,private local:LocalStorageService,private router: Router){
   this.stateCtrl = new FormControl();
  }
    ngOnInit(): void {
     this.projects=this._pService.GetUserProjects(GlobalVars.getAuth().id).publish().refCount();

     
     this.filteredStates = this.stateCtrl.valueChanges.startWith("")
                         .combineLatest(this.projects)
                         .map((combine,index) => combine[0] ? combine[1].filter(s => new RegExp(`^${combine[0]}`, 'gi').test(s.Name)): combine[1]);  


    this.projects.filter(projects=>projects.length==1)
                 .map(projects=>projects[0])
                 .subscribe(project=>{
                    this.local.set("id",project.PjId);
                    this.local.set("FWDate",project.FWDate);
                    this.router.navigate(['/pages',{id:project.PjId}]).then(()=>{
                    this.dialogRef.close();
                    })
               });
  }
  
  public GoProject(project:Project): void{
     this.local.set("id",project.PjId);
     this.local.set("FWDate",project.FWDate);
     this.router.navigate(['/pages']).then(()=>{
       this.dialogRef.close();
     })
  }
}

