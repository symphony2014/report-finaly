import { Component, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BaImageLoaderService, BaThemePreloader, BaThemeSpinner } from '../theme/services';
import { Angular2TokenService, A2tUiModule } from 'angular-token-report';
import { AdminService, LocalStorageService, AuthService, ProjectService, ModuleService } from "../services/angular.service";
import 'style-loader!./login.scss';
import { GlobalVars } from "../services/global.vars"; 
import { MdDialog } from "@angular/material";
import { DialogProjects } from "./projects/dialog.projects.component";
import { MdDialogRef,MD_DIALOG_DATA  } from "@angular/material";
import {DialogPassword} from "./password.component";
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'login',
  templateUrl: './login.html',
})
export class Login {

  public form: FormGroup;
  public name: AbstractControl;
  public password: AbstractControl;
  public submitted: boolean = false;
  public hasError:boolean=false;
  constructor(public dialog: MdDialog,
              private _aService: AdminService,
              private _tokenService: Angular2TokenService, 
              private router: Router,
              fb: FormBuilder,
              private _spinner: BaThemeSpinner,
              private _lService: LocalStorageService,
              private _pService:ProjectService,
              private _moduleService:ModuleService,
              private auth:AuthService) {
      this.form = fb.group({
      'name': ['', Validators.compose([Validators.required, Validators.minLength(1)])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(1)])]
      });

    this.name = this.form.controls['name'];
    this.password = this.form.controls['password'];
    localStorage.clear();
  }

  public onSubmit(values: any): void {
    this.submitted = true;
    if(this.form.valid){
      this._spinner.show();
      this._tokenService.signIn({username:values.name,password:values.password,email:""})
                        .map(auth=>auth.json())
                        .do(auth=>GlobalVars.setAuth(auth))
                        .switchMap((auth:any)=>this._pService.GetUserProjectsFront(auth.id))
                        .do(projects=>{
                          this._lService.set("id",projects[0].PjId);
                          this._lService.set("moduleTitle",projects[0].Name);
                          this._lService.set("projectName",projects[0].Name);
                          this._lService.set("projects",projects);
                        })
                        .switchMap(projects=>this._moduleService.GetProjectModules(projects[0].PjId, GlobalVars.getAuth().id))
                        .do(modules=>{
                          this.router.navigate(['/pages/dashboard',{pjId:this._lService.get("id"),id:modules[0].Id}]);
                        })
                        .catch(err=>{
                        this.hasError=true;
                        this._spinner.hide();                            
                         return null;
                        })
                        .subscribe();
    }
  }
  sendEmail(){
      this.dialog.open(DialogPassword,{width:"400px",data:this.form.controls['name'].value});
  }
}
