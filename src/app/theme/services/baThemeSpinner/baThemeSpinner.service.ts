import {Injectable} from '@angular/core';

@Injectable()
export class BaThemeSpinner {

  private _selector:string = 'preloader';
  private _element:HTMLElement;
  private _body:HTMLElement;
  private _html:HTMLElement;
  constructor() {

    this._element = document.getElementById(this._selector);
    this._body=document.getElementsByTagName("body")[0];
    this._html=document.getElementsByTagName("html")[0];
  }

  public show():void {
    this._element.style['display'] = 'block';
    this._body.style["overflow"]="hidden";
    this._html["scrollTop"]=0;
  }

  public hide(delay:number = 0):void {
    setTimeout(() => {
    this._element.style['display'] = 'none';
    this._body.style["overflow"]="auto";    
    }, delay);
  }
}
