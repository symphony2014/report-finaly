import { Component, Input, EventEmitter, Output, AfterViewInit, OnInit } from '@angular/core';
import {LocalStorageService} from "app/services/angular.service";
import { ResultDefaultData } from "app/services/models";
import { BehaviorSubject, Observable, Subject } from "rxjs/Rx";
import * as _ from "lodash";
import { ExportEvent, ExportType, ExportCompleteEvent, ComponentEvent } from "app/theme/components/ipsosComponent/component.event";
import { ImageUtil } from "app/services/util.service";
let jsPDF = require('jspdf');
@Component({
  selector: 'ipsos-export',
  styleUrls:["./export.scss"],
  templateUrl: './export.html',
})
export class Export implements OnInit  {
  @Output() change: EventEmitter<ExportEvent> = new EventEmitter<ExportEvent>();
   @Input()inputStream:Subject<ComponentEvent>=new Subject<ComponentEvent>();
   @Input()blockData:Observable<any>;
   
   completedCount:Subject<number>=new Subject<number>();
   pdf=new jsPDF();
   chartHeight:number=50;
   textWidth:number=20;
   textHeight:number=20;
   get config(){return {pageCount:4}};
   constructor(){
      this.completedCount.subscribe(e=>console.log(e))
   }
   ngOnInit():void{
          this.inputStream.asObservable()
                          .filter(event=>event && !!event.constructor)
                          .takeWhile(event=>event instanceof ExportCompleteEvent)
                          .combineLatest(this.blockData)
                          .map((data,index)=>({completeEvent:data[0],count:data[1],index:index} as any))
                          .do(e=>{
                              let newIndex=e.index%e.count;
                              this.completedCount.next((newIndex+1)*100/e.count);
                              if(newIndex<e.count)
                              {
                                  if (newIndex != 0 && (newIndex % this.config.pageCount == 0)) {
                                     this.pdf.addPage();
                                  }
                                  //add title
                                  this.pdf.addImage(ImageUtil.text2Image(e.completeEvent.uniqueId), 'JPEG', 15,10+(newIndex%this.config.pageCount)*(22+this.chartHeight),6.8*this.textWidth*e.completeEvent.uniqueId.length/18,this.textHeight);
                                  this.pdf.addImage(e.completeEvent.data.imgData,'JPEG', 15,  (newIndex%this.config.pageCount)*(22+this.chartHeight)+ 22, e.completeEvent.data.ratio*this.chartHeight, this.chartHeight);
                                  if(newIndex==(e.count-1))
                                  {
                                    this.pdf.save("report.pdf");
                                  }
                              }
                            })
                          .subscribe(()=>console.log("pdf.."));
  }
  pdfExport(){
    this.change.emit(new ExportEvent("",ExportType.PDF))
  }

  excelExport(){
    this.change.emit(new ExportEvent("",ExportType.EXCEL));
  }
}

