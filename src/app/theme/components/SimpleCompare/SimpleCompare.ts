import { Component, OnInit, Input } from '@angular/core';
import * as echarts from 'echarts';
import { ComponentInfo, ModuleType, RequestParam } from "app/services/models";
import { ActivatedRoute,Router,NavigationEnd } from "@angular/router";
import { ComponentService, LocalStorageService, BroadcastService, AlertService } from "app/services/angular.service";
import { PageBase } from "app/pages/page.base";
import { Observable, Subscription, BehaviorSubject, Subject } from "rxjs";
import { FilterStartEvent, ComponentEvent, FilterCompleteEvent } from "app/theme/components/ipsosComponent/component.event";
import { GlobalUtil } from "app/services/util.service";
import { Http } from "@angular/http";
@Component({
  selector: 'SimpleCompare',
  templateUrl: './SimpleCompare.html',
  styleUrls: ['./SimpleCompare.scss'],
})
export class SimpleCompare  {
      schema:any; 
      status:BehaviorSubject<string>=new BehaviorSubject<string>("init");  // init : not visiable, loading:show the loading icon, data:show the data
      @Input() inputStream:BehaviorSubject<ComponentEvent>;
      
        
     constructor(private http:Http,private util:GlobalUtil, private alertSerice:AlertService, public route:ActivatedRoute,public router:Router,public componentService:ComponentService,public local:LocalStorageService){
     }
        ngOnInit(): void {
        this.inputStream.asObservable()
                     .filter(e=>!!e && e instanceof FilterCompleteEvent)
                     .do(_=>this.status.next("data"))
                     .map((c:FilterCompleteEvent)=>c.data)
                     .subscribe(e=>this.schema=e);
        this.inputStream.asObservable()
                     .filter(e=>!!e && e instanceof FilterStartEvent)
                     .subscribe(_=>{
                       this.status.next("loading");
                       this.schema=null
                      });
       }
       ngOnDestroy() {
        this.inputStream.unsubscribe();
       }
 }
