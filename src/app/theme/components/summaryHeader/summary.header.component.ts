import { Component, Input, EventEmitter, Output, AfterViewInit } from '@angular/core';
import {LocalStorageService} from "app/services/angular.service";
import { ResultDefaultData } from "app/services/models";
import { BehaviorSubject,Observable } from "rxjs/Rx";
import * as _ from "lodash";
import {Format} from "app/services/util.service";
import { FilterCompleteEvent } from "app/theme/components/ipsosComponent/component.event";
@Component({
  selector: 'summary-header',
  templateUrl: './summary.header.html',
  styleUrls: ['./summary.header.scss'],
})
export class SummaryHeader  {
  optionAsync:Observable<any>= new Observable<any>();
  @Input()inputStream:BehaviorSubject<FilterCompleteEvent>;
  User:any;
  constructor(private _lService: LocalStorageService) {
    this.User = _lService.getAuth();
  }
 ngOnInit(): void {
    this.optionAsync=this.inputStream.asObservable()
                                      .filter(e=>!!e && e instanceof FilterCompleteEvent)
                                      .map(e=>e.data);
  
  }
  format(dataFormat:any,value:any){
      switch(dataFormat)
      {
        case "number":
        return value;
        case "percent":
        return Format.percent(value);
      }
  }
}

