
import { ComponentInfo, RequestParam, ModuleType } from "app/services/models";
import { BroadcastService, ComponentService, LocalStorageService } from "app/services/angular.service";
import { ActivatedRoute } from "@angular/router";
import { Observable, Subscription, BehaviorSubject, Subject,ReplaySubject } from "rxjs";
import { ComponentEvent } from "app/theme/components/ipsosComponent/component.event";
import { Component, Input, OnInit } from "@angular/core";
@Component({
  selector: 'block',
  templateUrl: './block.html',
  styleUrls:['./block.scss']
})
export class Block implements OnInit {
      @Input() moduleType: ModuleType;
      pjId:string;
      components:ReplaySubject<ComponentInfo[]>=new ReplaySubject<ComponentInfo[]>();  
      blockInPipe:BehaviorSubject<ComponentEvent>=new BehaviorSubject<ComponentEvent>(null);
      blockOutPipe:BehaviorSubject<ComponentEvent>=new BehaviorSubject<ComponentEvent>(null);
      chartEvent:BehaviorSubject<any>=new BehaviorSubject<any>(null);
      config:any={cols:2,gutterSize:1,rowHeight:"100%",style:"col-lg-20 center"};

      protected isChart(type:string){
      let charts=["Cord","Pie","Radar","Gauge"];
      return charts.some(c=>c==type);
      }

    constructor(public route:ActivatedRoute,public componentService:ComponentService,public local:LocalStorageService){
     }
  ngOnInit(): void {
      // this.componentService.GetComponents("").do(res=>this.components.next(res)).subscribe();
  }
      
}