import { Component, Input, EventEmitter, Output, AfterViewInit,OnInit } from '@angular/core';

import {  FormatType } from "app/services/models";
import { BehaviorSubject,Observable } from "rxjs/Rx";
import { FilterCompleteEvent } from "app/theme/components/ipsosComponent/component.event";
import { Format } from "app/services/util.service";

@Component({
  selector: 'ipsos-table',
  templateUrl: './table.html',
  styleUrls:['./table.scss']
})
export class Table  implements OnInit {
   @Input()inputStream:BehaviorSubject<FilterCompleteEvent>;
   optionAsync:Observable<any>;
   empty:boolean=false;
  constructor() {
  }
  ngOnInit(){
    this.optionAsync= this.inputStream.asObservable()
                      .filter(e=>!!e && e instanceof FilterCompleteEvent )
                      .map(filterCompleteEvent=>({array:filterCompleteEvent.data} as any))
                      .do(data=>{
                        if(data.array.every(a=>a==undefined)){
                           this.empty=true;
                        }
                      })
  }
  format(value:string,type:FormatType):string{
    switch(type){
       case FormatType.String:
       return value;
       default:
       return  Format.percent(value);
    }
  }
}

