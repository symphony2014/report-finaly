import { Component, Input, EventEmitter, Output, AfterViewInit } from '@angular/core';
import {LocalStorageService} from "app/services/angular.service";
import 'style-loader!./html.scss';
import { ResultDefaultData } from "app/services/models";
import * as _ from "lodash";
import { ComponentEvent, FilterCompleteEvent } from "app/theme/components/ipsosComponent/component.event";
import { Route, Router } from "@angular/router";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
@Component({
  selector: 'ipsos-html',
  templateUrl: './html.html',
})
export class Html  {

  
  optionAsync: ECharts.EChartOption;
  User:any;
  pjId:any;
   @Input()inputStream:BehaviorSubject<FilterCompleteEvent>;
  constructor(private route:Router,private _lService: LocalStorageService) {
   this.pjId=_lService.get("id");
    this.User = _lService.getAuth();
  }
 ngOnInit(): void {

   this.inputStream.asObservable().filter(e=>!!e).subscribe(e=>{
     this.optionAsync=e.data;
   });
 }
 avg(val):string{
   let color="";
   if(this.optionAsync) {
     var mean= _.mean(this.optionAsync.series[0].data.map(d=>Number(d[1])));
     if(Number(val) < mean)
     color=this.optionAsync.series[0].lowAvgColor;
  }
  return color;
 }
}

