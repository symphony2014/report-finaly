import { Injectable, EventEmitter } from "@angular/core";
import { Http,URLSearchParams } from "@angular/http";
import { RequestParam, RequestParamFilter, ResultDefaultData } from "app/services/models";
import * as _ from "lodash";
import { DateFormat } from "app/services/util.service";
import { LocalStorageService, AlgorithmService } from "app/services/angular.service";
import { MdDialog } from "@angular/material";
import { DialogAlert } from "app/theme/components/filter/components/alert.component";
import { GlobalVars } from "app/services/global.vars";
import { BehaviorSubject, Observable, ReplaySubject, Subject, AsyncSubject, Subscription } from "rxjs/Rx";
import { SimpleDialogAlert } from "app/theme/components/filter/components/simple.alert.component";
export class Dependency{
    item:RequestParamFilter;
    changeEvent:EventEmitter<any>;
    stream:Observable<ResultDefaultData>;
    ChoosedText?:Observable<string>;
}
@Injectable()
export class FilterService{

  
     getResult:(RequestParam,any?)=>Observable<any>=(filter,api)=>{
       if(api){
        let params = new URLSearchParams();
        api.paramFormat(filter).forEach(element => {
          params.set(element.key,element.value);
        });
         return this.http.get(GlobalVars.apiUrl+api.apiUrl,{search:params}).map(r=>api.resultFormat(r.json()));
       }else{
         return this.algorithmService.GetResult(filter);
       }
      };

    constructor(public http:Http,public dialog: MdDialog,private local:LocalStorageService,private algorithmService:AlgorithmService){}

    //varify "notNull" option was choosed.
    varifyAndTriggerNotNull(option:RequestParam){
      var result= option.Filters.filter(filter=>filter.NotNull).every(f=>(f.OptionValues as any[]).length>0); 
         if(!result)
         {
          this.dialog.open(DialogAlert,{
            data:option.Filters
                        .filter(filter=>filter.NotNull&&(filter.OptionValues as any[]).length==0)
                        .map(f=>f.TextValue)})
          return;
         }
         //验证时间是否跨年
         var notCrossYear=option.Filters.filter(filter=>filter.NotCrossYear).every(filter=>new Date(filter.DateStart as string).getFullYear()==new Date(filter.DateEnd as string).getFullYear())
         if(!notCrossYear){
          this.dialog.open(SimpleDialogAlert);
          return;
         }
         return result;
    }
  
  dependenciesAdapter(filter,reset:BehaviorSubject<any>){
                                       var dependency:Dependency[]=[];
                                       filter.async.Items.forEach((f,i) => {
                                         let event=new EventEmitter<any>();
                                         if(i==0)  
                                         {
                                           var stream=(f.Options && f.Options.length>0) ? Observable.of({XAxis:[],Series:(f.Options).map(o=>({Name:o.text,Values:[o.value]}))}):this.getResult({Pjid:this.local.get("id"),AlgorithmGroupId:f.AlgorithmId,UserId:this.local.getAuth()!.id},f.api);
                                           var first:Dependency={
                                             item:{change:f.Change,NotNull:f.NotNull,ParamName:f.ParamName,OptionValues:f.OptionValues||[],TextValue:f.OptionText,IsSearch:f.IsSearch,ShowType:f.ShowType,Default:f.Default},
                                             changeEvent:event,
                                             stream:stream
                                             };
                                             first.ChoosedText=first.changeEvent
                                                                    .combineLatest(first.stream)
                                                                    .map(com=>com[1].Series.find(s=>s.Values[0]==com[0]).Name+">")
                                                                    .merge(reset);
                                           dependency.push(first);
                                         }
                                         else{
                                         var prev=dependency[i-1];
                                         var current:Dependency={
                                         item:{change:f.Change,ParamName:f.ParamName,OptionValues:f.OptionValues||[],TextValue:f.OptionText,IsSearch:f.IsSearch,VarId:null,ShowType:f.ShowType,Default:f.Default},
                                         changeEvent:event,
                                         stream: prev.changeEvent
                                                     .startWith(0)
                                                     .combineLatest(prev.stream)
                                                     .do(_=>
                                                      {
                                                        prev.item.VarId=_[1]?_[1].XAxis[1]:null
                                                        //设置前一个默认值
                                                       // if(!(prev.item.OptionValues as any[] ) .length)
                                                      //  prev.item.OptionValues=[_[1].Series[0].Values[0]];
                                                    })
                                                     .map(prevData=>({Pjid:this.local.get("id"),Filters:[{ParamName:prev.item.ParamName,VarId:prevData[1].XAxis[1],OptionValues:[prevData[0]]}], AlgorithmGroupId:f.AlgorithmId,UserId:this.local.getAuth()!.id} as RequestParam))
                                                     .switchMap(subfilter=>this.getResult(subfilter,f.api))
                                                     .publish()
                                                     .refCount()
                                         };
                                         // current.ChoosedText=current.change
                                         //                           .combineLatest(current.stream,reset.distinctUntilChanged())
                                         //                           .map(com=>{
                                         //                             console.log(com[2]);
                                         //                            // return com[1].Series.find(s=>s.Values[0]==com[0]).Name;
                                         //                           }
                                         //                           );
                                            current.ChoosedText= current.changeEvent
                                                                   .combineLatest(current.stream)
                                                                   .filter(com=>!!com[1].Series)
                                                                   .map(com=>{
                                                                     var serie=com[1].Series.find(s=>s.Values[0]==com[0]);
                                                                     if(serie){
                                                                       return serie.Name+">";
                                                                     }else{
                                                                       return null;
                                                                     }
                                                                   })
                                                                   .merge(reset)
                                                                   .do(e=>console.log(e));
                                                                   
                                         current.stream.do(_=>{if(_)current.item.VarId=_.XAxis[1]}).subscribe();
                                         dependency.push(current);
                                         } 
                                     }); 
                                      
                                     return dependency;
}
  public sysdateAdapter(option:RequestParam):any{
      let today=new Date();
      let result=_.cloneDeep(option);
      result.Filters.map(o=>{
        if(o.ShowType =='datetime'){
          
          if(Number.isInteger(o.OptionValues as number)){
            o.DateEnd=DateFormat.IEFixedDate(today)+" 23:59:59";
          switch (o.OptionValues) {
            case 0:
           today.setMonth(0);
           today.setDate(1); 
              break;
            case 3:
            today.setMonth(today.getMonth()-5);
            today.setDate(1);
            break;
            case 2:
            today.setMonth(today.getMonth()-2);
            today.setDate(1);
            break;
            case 1:
            today.setDate(1);
            default:
            //today=new Date(this.local.get("FWDate"));
              break;
          }
          o.DateStart=today?DateFormat.IEFixedDate(today):"";
         }else{
           o.DateEnd=DateFormat.IEFixedDate(o.DateEnd as Date)+ " 23:59:59";
           o.DateStart=DateFormat.IEFixedDate(o.DateStart as Date);
         }
        
        }
      });

      result.Filters=result.Filters.filter(f=>{
        return  f.TextValue!="项目" && (f.OptionValues as string[]).length>0 || ((f.OptionValues as string[]).length===undefined && f.OptionValues) || (f.DateStart && f.DateEnd)
      });
      return result;
  }
  public mapDate(event:any,filter:RequestParamFilter){
     let today=new Date();
     let past=new Date();
       filter.DateEnd=today;
       filter.DateStart=past;


       
    switch (event){
      case 0:
      past.setMonth(0);
      past.setDate(1); 
       break;
     case 3:
     past.setMonth(past.getMonth()-5);
     past.setDate(1);
     break;
     case 2:
     past.setMonth(past.getMonth()-2);
     past.setDate(1);
     break;
     case 1:
     past.setDate(1);
     default:
     today=null;
       break;
          }
  }
    
}