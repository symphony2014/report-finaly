import { Component, ViewChild, ViewContainerRef, OnInit, Inject, OnChanges } from '@angular/core';
import { VariableService, AlgorithmService } from "app/services/angular.service";
import { MdDialogRef,MD_DIALOG_DATA  } from "@angular/material";
import { Variable, AlgorithmViewInfo, ComponentInfo } from "app/services/models";

@Component({
  selector: 'alert',
  
  template: `
  <div class="main">
  <h1 md-dialog-title></h1>
  <div md-dialog-content>
      <div class='main item'>
      
      时间不允许跨年! 
      </div>
  </div>
  <div md-dialog-actions>
  <button class="btn btn-primary" (click)="dialogRef.close()" tabindex="-1">确定</button>
  </div>
  </div>
  `,
  styles:[
      ` .main{
            padding:1em;
        } 
        ul{
            padding:1em
        }
        .item{
            padding:1.5em;
            font-size:18px;
        }
        button{
            width:100%;
        }
      `
  ]
})
export class SimpleDialogAlert implements OnInit {
  

    constructor(@Inject(MD_DIALOG_DATA)private data:string[],public dialogRef: MdDialogRef<SimpleDialogAlert>){
    }


    ngOnInit(): void {

    }
    
}

