import { Component, Input, EventEmitter, Output, AfterViewInit, OnInit, ElementRef, trigger, state, style, transition, animate } from '@angular/core';
import { ResultDefaultData, RequestParam, RequestParamFilter, ResultDataItems } from "app/services/models";
import { DateFormat, GlobalUtil } from "app/services/util.service";
import * as _ from 'lodash'
import { LocalStorageService, AlgorithmService } from "app/services/angular.service";
import { BehaviorSubject, Observable, ReplaySubject, Subject, AsyncSubject, Subscription } from "rxjs/Rx";
import { FilterStartEvent, FilterCompleteEvent } from "app/theme/components/ipsosComponent/component.event";
import { FilterService, Dependency } from "app/theme/components/filter/filter.service";
import { ChartFactory } from "app/theme/components/ipsosComponent/chart.factory.service";

@Component({
  selector: 'filter',
  templateUrl: './filter.html',
  styleUrls:['./filter.scss'],
  animations:[
    trigger('statusChanged', [
      state('warning' , style({ opacity: 1 ,"background-color":"red"})),
      state('done', style({ opacity: 0 ,"background-color":"green"})),
      transition('* => *', animate('2s')),
    ])
  ],
  providers:[FilterService]
})
export class Filter implements OnInit  {
  
  @Input() inputStream: BehaviorSubject<FilterCompleteEvent>;
  @Input() config:any;
  @Input() component:any;
  hidden:boolean=true;
  click:Subject<any>=new Subject();
  reply:AsyncSubject<RequestParam>=new AsyncSubject<RequestParam>();
 
  clear:BehaviorSubject<any>=new BehaviorSubject<any>("");
  
  //init:{}={dateStart:new Date()};
  
  get FWDate():string{
    var projects=this.local.get("projects");
    var pjId=this.local.get("id");
    return projects.find(p=>p.PjId==pjId).FWDate;
  }
  get recentYears():number[]{
    var years:number[]=[];
    var current=new Date().getFullYear();
    for(var i=0;i<3;i++) {
      years.push(current-i);
    }
    return years;
  }
   //recents:string[]=["最近一年","最近六月","最近三月","最近一月"];
   //确定之后触发外部更新事件
   @Output() change:EventEmitter<FilterStartEvent>=new EventEmitter<FilterStartEvent>();

  constructor(private chartFactory:ChartFactory,private globalUtil :GlobalUtil,private local :LocalStorageService,private element:ElementRef,private algorithmService:AlgorithmService,private filterService:FilterService) {
  }
  //  private setDefaultTime(filter:RequestParamFilter){
  //      //最近一年
  //      filter.OptionValues=[0];
  //      let today=new Date();
  //      let past=new Date();
  //        filter.DateEnd=past;
  //        filter.DateStart=today;
  //        today.setMonth(0);
  //        today.setDate(1); 
  //  }
   ngOnInit(): void {
   
   var filterStart:FilterStartEvent=this.globalUtil.appendGlobal(new FilterStartEvent("",{}),this.component.ComponentId);
   var once= this.algorithmService.GetResults(filterStart.filter)
                         .map((response:any)=>this.chartFactory.build(this.component,response))
                         .do(data=>{
                                 data.Filters.map(f => {
                                  //  if(f.ShowType=="datetime"){
                                  //    //最近一年
                                  // this.setDefaultTime(f);
                                  //  } 
                                   if(f.ShowType=="dependencies"){f.dependency=this.filterService.dependenciesAdapter(f,this.clear)}
                                  })
                                this.reply.next(data);
                                 this.reply.complete();
                          })
                         .subscribe(_=>once.unsubscribe());
             
     this.click.combineLatest(this.reply)
               .map(m=>{
                var filter=_.cloneDeep(m[1]); 
                //bad pattern merge dependencies
                var depFilters=_.flatten(filter.Filters.filter(f=>!!f.dependency).map(f=>f.dependency)).map(d=>d.item);
                filter.Filters=filter.Filters.concat(depFilters);
                return filter
                })
               .filter(filter=>this.filterService.varifyAndTriggerNotNull(filter))
               .do(filter=>this.change.emit(new FilterStartEvent("",this.filterService.sysdateAdapter(filter))))
               .subscribe(e=>console.log(e));
   
  }

  //change the range.
  rangeChange(event:any,item:RequestParamFilter){
    this.filterService.mapDate(event,item); 
  }

  //single date change.
  singleRangeChange(event:any,item:RequestParamFilter){
    item.DateStart=(event+"-01-01");
    item.DateEnd=(event+"-12-31");
  }
  dateVerify(event:any,item:RequestParamFilter){
     if((item.DateStart as Date).getFullYear()!=(item.DateEnd as Date).getFullYear())
     return false;
  }
  //single change event.
  optionChange(item:any){
    // if(!item.change){
    //   return;
    // }
    if(item.item && _.isFunction(item.item.change)){
      item.item.change(item);
    }
    if(item.changeEvent){
      item.changeEvent.emit(item.item.OptionValues[0]);
    }
  }
  filterChange(){
    this.click.next();
  }
  reset(){
   let reset= this.reply.asObservable()
              .takeLast(1)
              .map(e=>{
               e.Filters.forEach(f=>{
              //  if(f.ShowType=="datetime"){
                  //最近一年
                //  this.setDefaultTime(f);
              //  }else{
                  f.OptionValues=f.Default.length?_.cloneDeep(f.Default):[];f.DateStart=null;f.DateEnd=null;
                  if(f.dependency){
                    f.dependency.forEach(d => {
                      if(d.item.Default){
                        d.item.OptionValues=d.item.Default.length?_.cloneDeep(d.item.Default):[];
                      }else{
                        d.item.OptionValues=[];
                      }
                    });
                  }
               // } 
                })
                return e})
              .subscribe((filter:RequestParam)=>{
                this.reply.next(filter);
              });


    //clear choosed text.
     this.clear.next("");
  }
  selectAll(item){
    item.OptionValues=item.options.map(o=>o.value);
  }
  clearAll(item){
    item.OptionValues=[];
  }

  // flatten(filter:RequestParamFilter){
  //   var texts=[];

  //   //is date filter.
  //   if(filter.DateStart && filter.DateEnd){
  //    texts.push(DateFormat.toLocalString(filter.DateStart as Date)) 
  //    texts.push(DateFormat.toLocalString(filter.DateEnd as Date)) 
  //   }
  //   else{
  //     (filter.OptionValues as string[]).forEach(option=>{
  //       (option)
  //         if(option){
  //           var result=(filter as RequestParamFilter).options.find(o=>o.value==option) as any;
  //           if(result){
  //             texts.push(result.text);
  //           }
  //         }
  //     });
  //   }

  //   if(texts.length)
  //     return texts.join(',')+" >";

  // }

 ngOnDestory(){
 } 
}

