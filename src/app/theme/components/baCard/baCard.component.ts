import { Component, Input, EventEmitter, Output, OnInit, AfterViewInit, AfterContentInit, DoCheck, ElementRef } from '@angular/core';

@Component({
  selector: 'ba-card',
  templateUrl: './baCard.html',
})
export class BaCard implements AfterViewInit{
  constructor(private el:ElementRef){}
 
  ngAfterViewInit(): void {
    this.styleInitialised.emit(true); 
  }


  @Input() title: String;
  @Input() baCardClass:String;
  @Input() cardType:String;
   @Output() zoom:EventEmitter<null>=new EventEmitter<null>();
   @Output() styleInitialised:EventEmitter<boolean>=new EventEmitter<boolean>(false);
   changeInner(){
     this.zoom.emit();
   }
}
