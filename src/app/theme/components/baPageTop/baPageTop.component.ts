import { Component, Input } from '@angular/core';
import {Router,ActivatedRoute, Params } from '@angular/router';
import {GlobalState} from '../../../global.state';
import { LocalStorageService } from "../../../services/angular.service";
import 'style-loader!./baPageTop.scss';
import { Angular2TokenService } from 'angular-token-report';
@Component({
  selector: 'ba-page-top',
  templateUrl: './baPageTop.html',
})
export class BaPageTop {
  public pjId:string;
  public isScrolled:boolean = false;
  public isMenuCollapsed:boolean = false;

  public User: any;

  constructor(private local:LocalStorageService, private _state:GlobalState, private router: ActivatedRoute,private route:Router, private _lService: LocalStorageService,private _tokenService: Angular2TokenService) {

    this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
      this.isMenuCollapsed = isCollapsed;
    });

    this.User = _lService.getAuth();

  }

  public toggleMenu() {
    this.isMenuCollapsed = !this.isMenuCollapsed;
    this._state.notifyDataChanged('menu.isCollapsed', this.isMenuCollapsed);
    return false;
  }

  public scrolledChanged(isScrolled) {
    this.isScrolled = isScrolled;
  }
  public gotoCircle(){
    location.href=`/runtimextend/gotourl.html?userid=${this.User.id}&pjId=${this.local.get("id")}`
  }  
  public siginOut(){
      this.route.navigate(['/login']);
 //  this._tokenService.signOut().subscribe(()=>{
 //     this.route.navigate(['/login']);
 //  }) 
  }
}
