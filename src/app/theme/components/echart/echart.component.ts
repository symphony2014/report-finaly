
import {
  Component,Renderer,
  Directive, ElementRef, Input, OnInit, HostBinding, HostListener, OnChanges, OnDestroy, AfterContentChecked, AfterViewInit, AfterViewChecked, AfterContentInit, ViewChild, Output, EventEmitter
} from '@angular/core';
import {Http,Response} from '@angular/http';
import { Subject, Subscription, BehaviorSubject } from "rxjs";
import * as echarts from 'echarts';
import ECharts = echarts.ECharts;
import EChartOption = echarts.EChartOption;
import { Observable } from "rxjs/Observable";
import {EchartEventConvert} from "./echart.event.convert";
import { ComponentEvent, ChartEvent, ExportCompleteEvent, FilterStartEvent, FilterCompleteEvent, EventStatus, ExportEvent, StyleInitilizedEvent } from "app/theme/components/ipsosComponent/component.event";

import  * as _ from "lodash";
import { Status } from 'app/theme/components/ipsosComponent/status';
@Component({
  selector: 'echart',
  template: `
  <div  class="echart"  #echart style="width:100%;"></div>
  `,
  styleUrls:['./echart.scss'],

})

export class EChart implements OnInit,  OnDestroy {
   constructor(private el:ElementRef, private renderer:Renderer,private http:Http ) {

}
  


 
  private _init = false;
  private chart: ECharts;
  private sizeCheckInterval = null;
  private reSize$ = new Subject<string>();
  private onResize: Subscription;
  private dataLoaded=new BehaviorSubject<Status>(Status.Init);

  @ViewChild("echart") echart :ElementRef;
  @Input() inputStream:BehaviorSubject<ComponentEvent>;
  @Input() outputStream:BehaviorSubject<ComponentEvent>;
  @Input() isMap:boolean;

  @HostListener('window:resize', ['$event'])
  OnResize(event){
       //TODO:quick fix 
      // let json=_.cloneDeep(this.inputStream.getValue() as any);
      //   this.resizeGraphic(json);
      //   this.chart.resize()
  }
   ngOnInit(): void {
            this.dispatchEvent();
  }
  // private resizeGraphic(json){
  //       if(json.data && json.data.graphic)
  //         {
  //           var winWidth=window.innerWidth;
  //           json.data.graphic[0].children.forEach((element) => {
  //              element.children.forEach((sub,i)=>{
  //                 sub.left=(winWidth*sub.left/1600)
  //              })
  //           });
  //          this.chart.setOption(json.data);
  //         }
  // }
  private configEvent(event:ChartEvent){
      if(event){
        this.chart.on(event.chartEvent.type,e=>{
          this.outputStream.next(new ChartEvent(event.uniqueId));
        });
      }
  }
  private dispatchEvent(){
      this.inputStream.asObservable()
                      .filter(e=>!!e && e instanceof FilterStartEvent)
                      .do(()=>{
                        this.echart.nativeElement.style.height="400px";
                        this.chart=echarts.init(this.echart.nativeElement, 'vintage')
                        this.chart.showLoading()
                      })
                      //.do(()=>this.dataLoaded.next(Status.Loading))
                      .subscribe();
      this.dataLoaded.asObservable()
                      .combineLatest(this.inputStream)
                      .filter(c=>c[0]==Status.Complete)
                      .filter(c=>!!c[1] && c[1] instanceof ExportEvent)
                      .map(c=>c[1])
                      .do((e)=>{
                        let ratio=this.chart.getWidth()/this.chart.getHeight();
                        let dataUrl= this.chart.getDataURL({ type:"jpeg", pixelRatio: 3, backgroundColor: '#fff' });
                        this.outputStream.next(new ExportCompleteEvent(e.uniqueId,{imgData:dataUrl,ratio:ratio}));
                      })
                      .subscribe();

      this.inputStream.asObservable()
                      .filter(e=>!!e && e instanceof FilterCompleteEvent)
                      .do((e)=>{
                        this.chart.showLoading();
                        let data=(e as FilterCompleteEvent).data;
                        if (data.series[0] && (data.series[0] as any).type == 'map') {
                            this.http.request("/assets/jsons/china.json").subscribe((res: Response) => {
                                echarts.registerMap('china', res.json());
                                this.chart.setOption(data);
                            });
                        }

                        this.chart.setOption(data);
                      //  this.chart.resize();
                      //    this.resizeGraphic(data);
                      //  clearInterval(this.sizeCheckInterval);
                        this.chart.hideLoading();
                        //config the chart event.
                        this.configEvent(new ChartEvent(e.uniqueId,{}));
                        this.dataLoaded.next(Status.Complete);
                      })
                      .subscribe();

     this.inputStream.asObservable()
                      .filter(e=>!!e && e instanceof StyleInitilizedEvent)
                      .do(()=>{
                      //  this.chart=echarts.init(this.echart.nativeElement, 'vintage')
                      })
                      .subscribe();

      this.inputStream.asObservable()
                      .filter(e=>!!e && e instanceof ChartEvent)
                      .do((e:ChartEvent)=>{
                        if(e && e.chartEvent && e.uniqueId==e.config.to.id){
                            console.log(EchartEventConvert.datazoom(e.chartEvent));;
                            this.chart.dispatchAction(EchartEventConvert.datazoom(e.chartEvent));
                        }
                      })
                      .subscribe();
                      
  }




   ngOnDestroy() {
          if (this.sizeCheckInterval) {
            clearInterval(this.sizeCheckInterval)
          }
          this.reSize$.complete();
          if (this.onResize) {
            this.onResize.unsubscribe();
          }
         this.inputStream.unsubscribe();
         this.outputStream.unsubscribe();
        //  if(!this.dataLoaded.closed)
        //  this.dataLoaded.unsubscribe();
  }
}



 
