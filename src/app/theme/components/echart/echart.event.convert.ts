export class EchartEventConvert{
//     {
//     type: 'datazoom',
//     // 缩放的开始位置的百分比，0 - 100
//     start: number
//     // 缩放的结束位置的百分比，0 - 100
//     end: number
//     // 缩放的开始位置的数值，只有在工具栏的缩放行为的事件中存在。
//     startValue?: number
//     // 缩放的结束位置的数值，只有在工具栏的缩放行为的事件中存在。
//     endValue?: number
// }
// ``````````````````````````TO:``````````````````````````````
// {
//     type: 'dataZoom',
//     // 可选，dataZoom 组件的 index，多个 dataZoom 组件时有用，默认为 0
//     dataZoomIndex: number,
//     // 开始位置的百分比，0 - 100
//     start: number,
//     // 结束位置的百分比，0 - 100
//     end: number,
//     // 开始位置的数值
//     startValue: number,
//     // 结束位置的数值
//     endValue: number
// }
     static datazoom(action){
         return {type:"dataZoom",start:action.start,end:action.end,dataZoomIndex:0,startValue:action.start,endValue:action.end};
     }
}