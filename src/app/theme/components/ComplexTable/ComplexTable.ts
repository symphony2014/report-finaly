import { Component, Input, EventEmitter, Output, AfterViewInit, OnInit, ViewEncapsulation, ChangeDetectorRef, SimpleChange, ElementRef, ViewChild } from '@angular/core';
import { LocalStorageService, AlgorithmService, TaskService } from "app/services/angular.service";
import { ResultDefaultData, FormatType, ComponentInfo, RequestParam, Result } from "app/services/models";
import { BehaviorSubject, Observable, Subject, Subscription } from "rxjs/Rx";
import * as _ from "lodash";
import {Format} from "app/services/util.service";
import { FilterCompleteEvent, ComponentEvent, FilterStartEvent } from "app/theme/components/ipsosComponent/component.event";
import { ChartFactory } from "app/theme/components/ipsosComponent/chart.factory.service";
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { BaThemeSpinner } from 'app/theme/services';

@Component({
  selector: 'ComplexTable',
  templateUrl: './ComplexTable.html',
  styleUrls: [
    './ComplexTable.scss',
  ],
})
export class ComplexTable implements OnInit  {
  @Input() inputStream:Subject<ComponentEvent>;
  @Input() component:ComponentInfo;
  
  get config():any{
     var option=new Function("var  " +this.component.Schema +"\n  return option;");
     return option();
  }
  @ViewChild("table") table:ElementRef;
  @ViewChild("side") side:ElementRef;
  @ViewChild("header") header:ElementRef;
  items:Observable<Result>[]=[];
  status:BehaviorSubject<string>=new BehaviorSubject<string>("init"); 
  exportClick:Subject<string>=new Subject<string>(); 
  exportData:Subject<FilterStartEvent>=new Subject<FilterStartEvent>();
  joinRequest:Subscription;
  completedCount:number=0;
  public tableData:{rowData:any[],columnDefs:any[],sideData:any[]}={rowData:[],columnDefs:[],sideData:[]};
  public results:any[]=[];
  public pagingData:any;
  public sideData:any[];
  public pageSize:number=10;
  public pageIndex:number=1;
  public totalSize:number;
 constructor(public taskService:TaskService,
  private router: Router,
  private cdr: ChangeDetectorRef, 
  private chartFactory:ChartFactory,
  private algorithmService:AlgorithmService,
  private local:LocalStorageService,
  public _spinner: BaThemeSpinner
) {

 }
 ngOnInit(){
   //bad pattern.
   //it seems like inputStream will store last value, and trigger the data loading.
   this.router.events.filter(e=>e instanceof NavigationEnd)
                     .subscribe(_=>this.inputStream.unsubscribe());


    this.inputStream.asObservable()
                     .filter(e=>!!e && e instanceof FilterStartEvent )
                     .subscribe((filter:any)=>{
                       if(filter.filter && filter.filter.Filters){
                         this.items=[];
                         //ui 
                         this.completedCount=0;
                         this.status.next("loading");

                         //bad pattern , but the withLatestFrom doesnot work for inputStream.
                         this.exportData.next(filter);
                         
                         let group=filter.filter.Filters.find(f=>f.ParamName=="target");
                         if(group && group.OptionValues && group.OptionValues.length>0){
                           this.results=[];
                           group.OptionValues.forEach((id,index) => {
                             let newIndex=index;
                             let newFilters=_.cloneDeep(filter.filter);
                              _.remove(newFilters.Filters,(f:any)=>f.ParamName=='target');
                             newFilters.AlgorithmGroupId=id;
                             this.algorithmService.GetResults(newFilters)
                                                                  .subscribe(result=>{
                                                                    this.completedCount+=1*100/group.OptionValues.length;
                                                                   if(!result){
                                                                    this.status.next("empty");
                                                                    }
                                                                    else{
                                                                      this.results[newIndex]=(result);
                                                                      for(let x=0;x<this.results.length;x++){
                                                                        if(!this.results[x])
                                                                        {
                                                                          this.results[x]=result;
                                                                        }
                                                                      }
                                                                      this.tableData=this.chartFactory.build(this.component,this.results);
                                                                      this.updatePage(this.pageIndex,false); 
                                                                     }
                                                                  });
                           });
                          //   this.joinRequest=Observable.forkJoin(this.items).subscribe(result=>{
                          //    // this.result=result.map(r=>r.Datas); 
                          //     if(!result || result.length==0){
                          //      this.status.next("empty");
                          //      }
                          //      else{
                          //        this.tableData=this.chartFactory.build(this.component,result);
                          //        this.updatePage(this.pageIndex); 
                          //       }
                               
                          //  });
                            
                         }
                         else{
                           this.status.next("empty");
                         }
                       }
                     })


   //export
   this.exportClick.asObservable()
                   .do(_=>this._spinner.show())
                   .withLatestFrom(this.exportData)
                    .map((e : [string,ComponentEvent])=>(e[1] as FilterStartEvent).filter)
                    .switchMap((filter:RequestParam)=> this.taskService.Add(filter))
                    .map(taskId=>{
                      let interval=Observable.interval(2000).switchMap(()=>this.taskService.GetNewTaskLog(taskId)).distinctUntilChanged(null,x=>x.length).publish().refCount();
                      let downloading= interval.filter(message=>message[0].Flag)
                                              .do(m=>console.log(m))
                                              .switchMap(message=>this.taskService.Get(taskId))
                                              .subscribe((task:any)=>{
                                                  downloading.unsubscribe();
                                                  this._spinner.hide(); 
                                                  location.href=`res/${this.local.get("id")}/${task.TaskData}`;
                                                });
                           }).subscribe();
                   
 }
 format(value:string,type:FormatType):string{
   switch(type){
      case FormatType.String:
      return value;
      default:
      return  Format.percent(value);;
   }
 }
 ngOndestory(){
   this.inputStream.unsubscribe();
 }
 
 ngAfterViewInit() {
   this.cdr.detectChanges(); 
 }
 export(type:string){
    this.exportClick.next("start"); 
 }

 calcWidth(value){
   value=value.replace("%","");
   return (344/2+(Number(value)*113/100)-9.5)+'px';
   //return (Number(value)+ 100) / 2 - 2.8 + '%' ;
 }
//  indexChange(index){
//    this.updatePage(index);
//  }
 scrollTable(e){
  this.side.nativeElement.scrollTop=this.table.nativeElement.scrollTop;
  this.header.nativeElement.scrollLeft=this.table.nativeElement.scrollLeft;
 }
 search(isFirst:boolean){
   this.updatePage(1,isFirst); 
 }
 private updatePage(index,isFirst){
  
  var filterValue=this.filterByColumns(this.tableData.rowData,isFirst);
  this.totalSize=filterValue.rows.length; 
  this.pageSize=this.totalSize;
  this.pageIndex=index;
  this.pagingData=_.chain(filterValue.rows)
  .drop((index-1)*this.pageSize)
  .take(this.pageSize)
  .value();
  this.sideData=filterValue.sideData;
 }
  filterByColumns(rowData:any,isFirst:boolean){
      var initData=_.cloneDeep(rowData);
      var sideData=_.cloneDeep(this.tableData.sideData)
      this.tableData.columnDefs.forEach((col,i) => {
       if(col.SearchKey){
         initData=initData.filter((row,j)=>{
           if(!isFirst){
             return row[i].value.indexOf(col.SearchKey)>-1
            }else{
              var condition=this.tableData.sideData[j].indexOf(col.SearchKey)>-1;
           
             return condition;
            }
           }
          )
       }
     });
     if(isFirst){
       sideData=_.cloneDeep(this.tableData.sideData.filter(s=>s.indexOf(this.tableData.columnDefs[0].SearchKey)>-1));
     }
    return {rows:initData,sideData:sideData};
 }
 ngOnDestroy() {
   if(this.joinRequest)
  this.joinRequest.unsubscribe();
 }
}