import { Injectable, Inject } from '@angular/core';
import { EChartOption, EchartGraphicChildren } from 'echarts';
import { TemplateInfo, ComponentInfo, ResultDefaultData, RequestParam } from '../../../services/models/IpsosReport.ModelBiz';
import { JsonUtil } from "app/services/util.service";
import * as _ from "lodash";
import { GlobalVars } from "app/services/global.vars";
import {Format}  from "app/services/util.service";
import * as echarts from "echarts";
import { LocalStorageService } from "app/services/angular.service";
import { Result } from 'app/services/models';
@Injectable()
export class ChartFactory {
    component:ComponentInfo;
    result:ResultDefaultData[];
    constructor(private local:LocalStorageService ){
    }
     
    build(component:ComponentInfo,result:Result | any){
        //init
        this.component=component;
        this.result=result.Datas || result;
    
        try {
            if (!this.component.Schema) {
                return null;
            }
            if(!this.result || this.result.length==0)
            {
                return null;
            }
                //avoid javascript compress.
                //var option=()=>console.log("");
                //bad pattern
                echarts.getInstanceByDom(document.getElementsByTagName("div")[0]);
                var option=new Function("var "+this.component.Schema +"\n  return option;")();
                if(!this[this.component.Type]){
                    if(this.component.BindStrategy){
                        var strategy=new Function("option","result",this.component.BindStrategy);
                        return strategy(option,this.result);
                    }
                    return null;
                }
                return this[this.component.Type](option);
        }
        catch (e) {
            console.log(JsonUtil.format(this.component.Schema));
            console.log(this.result);
            console.log(this.component);
            console.log(e);
            throw e;
        }
    }
    Filter(schema:EChartOption):RequestParam{
        //convert api data to Filter data.
        let result: RequestParam = {
            AlgorithmGroupId: "",
            UserId: "",
            Pjid: "",
            Filters: []
        };
        schema.series.map((s)=>{
            var filter={
                 TextValue:s.OptionText || s.ParamName,
                 ParamName:s.ParamName,
                 ShowType:s.ShowType,
                 Multiple:s.Multiple,
                 IsSearch:s.IsSearch==undefined?true:s.IsSearch,
                 NotNull:s.NotNull,
                 options:s.Options?s.Options:[],
                 OptionValues:s.default?s.default():[],
                 Default:s.default?s.default():[],
                 change:s.Change,
                 DateEnd:null,
                 DateStart:null,
                 NotCrossYear:s.NotCrossYear,
                 DisableQuick:s.DisableQuick
            };
            switch (s.ShowType) {
                case "dependencies":
                    s.Items.forEach(element => {
                       if(element.default){
                           element.OptionValues=element.default();
                           element.Default=element.default();
                       }
                    });
                    _.extend(filter,{ async:{Items:s.Items} });
                    break;
               case "datetime":
                  if(filter.OptionValues.length){
                      switch(filter.OptionValues[0]){
                          case 0://当年
                           filter.DateStart=new Date(new Date().getFullYear(),0,1);
                           break;
                           case 1://当月
                           var currentMonth=new Date();
                            currentMonth.setDate(1);
                           filter.DateStart=currentMonth;
                           break;
                      }
                      filter.DateEnd=new Date();
                  }
                   break;
                default:
                    let current = this.result.find((r) => s.AlgorithmId == r.AlgorithmId);
                    if(current)
                        {
                             //异步数据的默认值
                             var defaultV=current.Series.length?[current.Series[0].Values[0]]:filter.Default;
                            _.extend(filter,{
                                VarId: current.XAxis?current.XAxis[1]:"",//(bad pattern)
                                options:current.Series.length? current.Series.map(sub => ({ text: sub.Name, value: sub.Values[0] })):filter.options,//时间选项虽然没有异步数据，但是却属于异步类型，这个方面杨志应该改一下。
                                OptionValues:s.ShowDefault? defaultV:[],
                                TextValue: current.Text,
                            });
                        }
                    break;
            }
          result.Filters.push(filter);
        });
        return result;
    }
    Gauge(schema:EChartOption){
        schema.series[0].name=this.result[0].Series[0].Name;
        schema.series[0].data[0].value=Number(this.result[0].Series[0].Values[0])*100;
        schema.series[0].data[0].name=schema.series[0].ipsosNameShow ? this.result[0].Series[0].Name : "";
        let text=_.cloneDeep(this.component.ShowText);
        schema.series[0].detail.formatter=val=>{
            return `${text}${Math.round(val*100)}%`;
        };
        return schema;
    }

   Pie(schema:EChartOption){
         //schema.legend.data=this.result.map(r=>r.Legend[0]);
         schema.legend.data=this.result[0].XAxis;
         schema.series.forEach((s,i)=>{
               let current=this.result.find((r)=>r.AlgorithmId==s.AlgorithmId);
               let sub=Object.assign({},s);
               let data=[];
               current.Series.forEach((item,index)=>{
                  data.push({
                      name:this.result[0].XAxis[index],
                      value:item.Values[0]
                  });
               });
                s.data=data;
            });
            return schema;
    }
    Cord(schema:EChartOption){
        // order  series.
        this.result=this.order(schema.series); 
        if(schema.Sort){
        this.result[0].Series.sort((a,b)=>Number(a.Values[0])-Number(b.Values[0]));
        }
        let buildlegend=(r:ResultDefaultData[])=>r.length>1?_.flatten(r.map(r=>r.Legend)):r[0].Legend;
        let newSeries=[];
  
        schema.legend.data=buildlegend(this.result);
        if(schema.xAxis[0].data){
         schema.xAxis[0].data=this.result[0].Series.map(s=>s.Name);
        }
        else{
             //bad pattern
             schema.yAxis[0].data=[].concat.apply([],this.result.map(r=>r.Series.map(s=>s.Name)));
             schema.yAxis[0].data=[].concat.apply([],schema.yAxis[0].data);
         }
         schema.series.forEach((s,i)=>{
               let current=this.result.find((r)=>r.AlgorithmId==s.AlgorithmId);
               if(current.Legend.length>0){

               current.Legend.forEach((data,index)=>{
                  let sub=Object.assign({},s);

                  sub.name=data;
                  sub.data=current.Series.map(r=>r.Values[index])
                  newSeries.push(sub);
               });
               }else{
                    let sub=Object.assign({},s);
                    sub.name=schema.legend.data[i];
                    sub.data=current.Series.map(r=>r.Values[0])
                    newSeries.push(sub);
               }
            });
        
          schema.series=newSeries;
          //bad pattern
          if(schema.yAxis[0].data){
              
             schema.series[0].data=[].concat.apply([],this.result.map(r=>r.Series.map(s=>s.Values)));
             schema.series[0].data=[].concat.apply([],schema.series[0].data);
             schema.series.splice(1,schema.series.length-1);
          }
         return schema;
         
    };
    Map(schema:EChartOption){
        return schema;
    }
    Radar(schema:EChartOption){
       
            let datas=[];
   
         schema.legend.data=this.result.map(r=>r.Legend[0]);
          schema.radar.indicator=this.result[0].XAxis.map((s)=>{return {name:s}});
    
        this.result.forEach((s,i)=>{
                       let current=this.result.find((r)=>r.AlgorithmId==s.AlgorithmId); 
                       datas.push(Object.assign(_.cloneDeep(schema.series[0].data[0]),{name:s.Legend[0],value:s.Series.map(s1=>s1.Values[0])}));
        });
        
           Object.assign(schema.series[0],{data:datas});
           schema.series.splice(1,schema.series.length-1);
          return schema;
    }
    Heatmap(schema:EChartOption){
        let result=[];
         schema.yAxis.data=this.result.map((s)=>{return s.Text});
         schema.xAxis.data=this.result[0].XAxis;
         schema.series.forEach((s,pi)=>{
               let current=this.result.find((r)=>r.AlgorithmId==s.AlgorithmId);
                  current.Series.map((r,i)=>result.push([i,pi,r.Values[0]]));
            });
        
          schema.series[0].data=result;
          schema.series.splice(1,schema.series.length-1);
         return schema;
 
    }

    Graphic(schema:EChartOption){

      let newGraphic=[];
      let maxFontSize=(children)=>{
            let maxFontSize=0;
         children.filter(c=>c.type=='text').map(n=>n.style.font.match(/\d+/g).map(f=>{
                                              if (f>maxFontSize)
                                              maxFontSize=Number(f);
                                          }));
        return maxFontSize;
      }
      let merged=this.mergeAlgorithms(this.result);

      schema.graphic.forEach(g=>{
               merged.Series.forEach((s,i)=>{
                var subGraphic=_.cloneDeep(g);
                let fontHeight=maxFontSize(g.children);
                subGraphic.top=i*fontHeight*g.LineHeight;
                subGraphic.children[0].style.text=s.Name;
                subGraphic.children[1].style.text=Format.percent(s.Values[0]);
                let diff=Number(s.Values[0])-Number(s.Values[1]);
                if(diff){
                    subGraphic.children.splice(2,1); 
                }
                else{
                    subGraphic.children.splice(3,1); 
                }
                subGraphic.children[3].style.text=Format.percent(diff.toString());
                newGraphic.push(subGraphic);
               });

      })
    //   schema.graphic=newGraphic;
      schema.graphic[0].children=newGraphic;
      return schema;
    }

    Table(schema:EChartOption){
        //return this.mergeAlgorithms(this.order(schema.series)); 
        // let result=this.order(schema.series);
        let newData=[];
        schema.series.forEach((element:ResultDefaultData[]|ResultDefaultData) => {
         let newSerie=_.cloneDeep(this.result[1]);
         if(_.isArray(element))
            {
          let first=this.result.find(r=>r.AlgorithmId==element[0].AlgorithmId);
          let second=this.result.find(r=>r.AlgorithmId==element[1].AlgorithmId);
          newSerie=_.cloneDeep(first);
          newSerie.Legend=_.union(first.Legend,second.Legend);
          newSerie.XAxis=_.union(first.XAxis,second.XAxis);
          newSerie.Series=_.concat(first.Series,second.Series);
            }
        else {
         newSerie=this.result.find(r=>r.AlgorithmId==element.AlgorithmId);
        }
          newData.push(newSerie);
        });
        return newData;
    }

    Html(schema:EChartOption):EChartOption{
        schema.series[0].data=this.result[0].Series.map(s=>{s.Values.push(s.Name); return s.Values});
        return schema;
    }
    SummaryHeader(schema:any):any{
       let newResult=this.order(schema.series); 
       return _.zip(newResult,schema.series); 
    }

    //merge algorithms data to one.
    private mergeAlgorithms(result:ResultDefaultData[]):ResultDefaultData{
    
    let newData:ResultDefaultData=_.cloneDeep(result[0]);
    //merge row
      result.map((r,index)=>{
         if(index){
        let temp=_.cloneDeep(r);
        if(_.every(r.Series,(s,i)=>s.Name===newData.Series[i].Name))
        {
         newData.Series.map((s,i)=>{
              s.Values=s.Values.concat(r.Series[i].Values);
             })
         newData.Legend=newData.Legend.concat(r.Legend);
        }
         }
      });
     //merge column
    //   result.map(r=>{
    //     let temp=_.cloneDeep(r);
    //     if(_.every(r.Series,(s,i)=>s.Name===newData.Series[i].Name))
    //     {
    //      newData.Series.forEach((s,i)=>{ s.Values.concat(r.Series[i].Values);})
    //      newData.Legend.concat(r.Legend);
    //     }
    //   });
      
     return newData;
    }
    Export(){}
    Customize(schema:any){
      
    }
    private order(series){
        let newResult=[];
        //order by schema.
        series.forEach(element => {
            let current=this.result.find(r=>r.AlgorithmId==element.AlgorithmId);
            newResult.push(current);
        });
        return newResult;
    }
}