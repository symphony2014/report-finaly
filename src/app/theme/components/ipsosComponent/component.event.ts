import {RequestParam} from "../../../services/models/IpsosReport.ModelBiz";
import { EChartOption } from "echarts";
export enum ExportType {
    PDF,
    EXCEL
}
export enum EventStatus{
    NOTSTARTED,
    LOADING,
    COMPLETE
}
export abstract class ComponentEvent{
 constructor(public uniqueId?:string,public status?:EventStatus){}
}
export  class DataSyncEvent extends ComponentEvent{
     value:Object;
}
export  class StyleInitilizedEvent extends ComponentEvent{

     constructor(public uniqueId?:string,public initilized?:boolean){
         super(uniqueId);
     }
}

export  class FilterStartEvent extends ComponentEvent{

     constructor(public uniqueId?:string,public filter?:RequestParam){
         super(uniqueId);
     }
}
export class AfterComponentInitEvent extends ComponentEvent{
    constructor(public uniqueId:string,public data?:any){
        super(uniqueId);
    }
}
export class FilterCompleteEvent extends ComponentEvent{
    constructor(public uniqueId:string,public data:any){
        super(uniqueId);
    }
}
export class ChartEvent extends ComponentEvent{
    public constructor(public uniqueId:string,public chartEvent?:any,public config?:any){
        super(uniqueId);
    }
}
export class ExportEvent extends ComponentEvent {
    public constructor(public uniqueId:string,public type?:ExportType){
        super(uniqueId);
    }
}
export class ExportCompleteEvent extends ComponentEvent{
    public constructor(public uniqueId:string,public data?:any,status?:EventStatus,){
        super(uniqueId,status);
    }
}