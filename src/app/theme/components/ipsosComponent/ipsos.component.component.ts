import { Component, Input, OnInit, Output, EventEmitter, Inject, NgZone } from '@angular/core';
import { ComponentInfo, ResultDefaultData, RequestParam, ValueShowType } from '../../../services/models/IpsosReport.ModelBiz';
import { ChartFactory } from './chart.factory.service';
import { AlgorithmService, BroadcastService, TaskService } from '../../../services/angular.service';
import { Angular2TokenService } from 'angular-token-report';
import { GlobalVars } from '../../../services/global.vars';
import { ComponentStyleType } from "app/services/models";
import { DBMock } from "app/fake.db.service";
import { MD_DIALOG_DATA, MdDialogRef, MdDialog } from "@angular/material";
import { Observable } from "rxjs/Observable";
import { Subject, BehaviorSubject,Subscription } from "rxjs/Rx";

import { FilterStartEvent, ExportEvent, ComponentEvent, ExportCompleteEvent, FilterCompleteEvent, StyleInitilizedEvent, AfterComponentInitEvent } from "app/theme/components/ipsosComponent/component.event";
import * as _ from "lodash";
import { Http } from "@angular/http";
import { ComponentState } from "app/theme/components/ipsosComponent/types";
import { DialogZoom } from "app/theme/components/ipsosComponent/components/dialog.zoom.component";
@Component({
  selector: 'ipsos-component',
  templateUrl: './ipsos.component.html',
  styleUrls:['./ipsos.component.scss']
})
export class IpsosComponent implements OnInit {

  options:BehaviorSubject<ECharts.EChartOption>=new BehaviorSubject<ECharts.EChartOption>(null);
  cancelService:Subscription;
  loading:BehaviorSubject<boolean>=new BehaviorSubject<boolean>(true);
  inputStream:Subject<ComponentEvent>=new Subject<ComponentEvent>();
  outputStream:BehaviorSubject<ComponentEvent>=new BehaviorSubject<ComponentEvent>(null);
  exportPipe:BehaviorSubject<ComponentEvent>=new BehaviorSubject<ComponentEvent>(null);
  interval:number=2000;
  //baCard style was initialized .
  styleInitiedEvent:BehaviorSubject<boolean>=new BehaviorSubject<boolean>(null);

 
  @Input() component: ComponentInfo;
  @Input() blockInPipe:BehaviorSubject<ComponentEvent>;
  @Input() blockOutPipe:BehaviorSubject<ComponentEvent>;
  @Input() blockData:Observable<any>;
  @Input() config:any={OK:"运行",Reset:"重置"};

  constructor(private taskService:TaskService,private http:Http,public dialog: MdDialog, private algorithmService:
   AlgorithmService, private tokenService: Angular2TokenService,private chartFactory:ChartFactory) { }
  ngOnInit(): void {

 
    //init
    //this.blockInPipe.next(new AfterComponentInitEvent(this.component.ComponentId));
    
    //accumulate event.
    if(this.blockOutPipe)
    this.outputStream.subscribe(e=>this.blockOutPipe.next(e));
    
    //handle block event.
    this.blockInPipe.asObservable()
                    .filter(e=>!!e && e instanceof ComponentEvent)
                    .combineLatest(this.styleInitiedEvent)
                    .filter(c=>c[1])
                    .subscribe(c => {
                         let event=c[0];
                          if(event instanceof FilterStartEvent){
                                  this.inputStream.next(event);
                                  let newEvent=this.appendGlobal(event);
                                  this.update(newEvent);
                          }
                          else if(event instanceof ExportEvent)
                          {
                              this.inputStream.next(new ExportEvent(this.component.ShowText,(event as ExportEvent).type));
                          }
                    });

  }

  change(event:ComponentEvent) {
    // emit event to block.
    if(event instanceof  FilterStartEvent) 
    {
     event=this.appendGlobal(event);
    }
    this.blockInPipe.next(event);
  }
  update(event: FilterStartEvent) {
     let request=this.component.AsyncUrl?this.http.get(this.component.AsyncUrl,{params:event.filter}):this.algorithmService.GetResults(event.filter);
     this.cancelService=request.map((response:any)=>this.chartFactory.build(this.component,response))
                               .subscribe(option=>{
                                        this.inputStream.next(new FilterCompleteEvent(this.component.ComponentId,option));
                               });
  }


 styleInited($event:boolean){
   this.inputStream.next(new StyleInitilizedEvent());
    if($event){
      this.styleInitiedEvent.next(true);
    }
 }
  zoom(component:ComponentInfo){
      let newComponent=_.cloneDeep(component);
      newComponent.Style=newComponent.Style.replace(/\d{1,2}/g,'20');
      newComponent.Style+=" no-border ";
      this.dialog.open(DialogZoom,{width:'70%', data:{component:_.cloneDeep(newComponent),pipeIn:this.blockInPipe}});
  }
  appendGlobal(event:FilterStartEvent):FilterStartEvent{
    event.filter.Pjid=this.component.Pjid;
    event.filter.UserId= GlobalVars.getAuth().id;
    event.filter.AlgorithmGroupId= this.component.ComponentId;
    return event;
  }
   ngOnDestroy() {
        if(this.options)this.options.unsubscribe();
        //if(this.inputStream)this.inputStream.unsubscribe();(bad pattern)
        if(this.cancelService) this.cancelService.unsubscribe();
  }
}



