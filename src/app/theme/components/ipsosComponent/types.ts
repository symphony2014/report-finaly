/**
 * Bitmask of states
 */
export const enum ComponentState {
    BeforeCardInitlized = 1 << 0,
    CardInitlized = 1 << 1,
    Destroyed = 1 << 2,
  
   CanstartEventHandler=CardInitlized 
  }