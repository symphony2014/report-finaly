import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute, Params, NavigationEnd, NavigationStart } from '@angular/router';
import {GlobalState} from '../../../global.state';
import { LocalStorageService, ModuleService } from "../../../services/angular.service";
import * as _ from "lodash";
import 'style-loader!./baTopMenu.scss';
import { Angular2TokenService } from 'angular-token-report';
import { Observable } from "rxjs/Rx";
import { Subject } from 'rxjs/Subject';
import { GlobalVars } from 'app/services/global.vars';
@Component({
  selector: 'ba-top-menu',
  templateUrl: './baTopMenu.html',
})
export class BaTopMenu {
  
  public isScrolled:boolean = false;
  public isMenuCollapsed:boolean = false;
  public isCurrentRoute:Subject<boolean>=new Subject<boolean>();
  public User: any;
  public title:string;
  modules:Observable<any>;
  get pjId(){
    return location.href.match(/pjId=(\d{18})/)[1];
  }
  constructor(private moduleService:ModuleService, private local:LocalStorageService, private _state:GlobalState, private router: ActivatedRoute,private route:Router, private _lService: LocalStorageService,private _tokenService: Angular2TokenService) {
  this.modules=this.route.events.switchMap(_=>moduleService.GetProjectModules(this.local.get("id"), this._lService.getAuth().id))
                              .map(modules=>{
                                var href=location.href.match(/id=(\d{18})/g); 
                                 if(href.length>0)
                                 {
                                   var id=href[0].split('=')[1];
                                  var inChildren=_.filter(modules,((m,i)=>i>1)).some(m=>m.Id==id); 
                                  var inModules=this.inModules(id,modules);
                                  if(!inModules){
                                   location.href= location.href.replace(/id=\d{18}/g, "id=" + modules[0].Id)
                                   this.title="具体分析";
                                  }else{
                                    modules.forEach(m=>{
                                      m.active=id==m.Id;
                                    });
                                  }
                                  // modules.forEach(m=>{
                                  //   m.active=id==m.Id;
                                  //   if(m.active){
                                  //     if(!inChildren)
                                  //     {
                                  //       this.local.set("moduleTitle",this.local.get("projectName"));
                                  //     }
                                  //     else{
                                  //       this.local.set("moduleTitle",m.Title);
                                  //     }
                                  //   }
                                  // });
                                  return {modules:modules,rootActive:inChildren};
                                }
                              })
                               .map((obj:any)=>{
                                 if(obj)
                                return ([obj.modules[0],obj.modules[1],{Title:"具体分析",active:obj.rootActive,children:obj.modules.filter((m,i)=>i>1)}]);
                               });
    this.User = local.getAuth();
  }
  isCurrent(id:string){
     this.router.params.subscribe(params=>this.isCurrentRoute.next(true));
  }
  childrenChange(root:any,module:any){
    let route = "";
    switch (module.ContentType) {
      case 9: // 数据检索
        route = "/pages/datasearch";
        break;
      default: // 其他模块
        route = "/pages/dashboard";
      break;
    }
    
    this.route.navigate([route,{pjId:this.pjId,id:module.Id}])
    .then(_=>this.title=module.Title);
  }
  inModules(id:string,modules:any[]){
    return modules.some(m=>m.Id==id);
  }
}
