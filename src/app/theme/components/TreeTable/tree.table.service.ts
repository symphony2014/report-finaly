import { Injectable } from '@angular/core';
import {Http}    from '@angular/http';
import "rxjs/Rx";
import {Observable} from "rxjs";
import { IndicatorClassifyService, LocalStorageService } from 'app/services/angular.service';
import { Route, Router, ActivatedRoute } from "@angular/router";
@Injectable()
export class TreeTableService {
	constructor(private router:ActivatedRoute,private local:LocalStorageService,public http:Http,private indicator:IndicatorClassifyService) {

    }
    item_list = {
		id : 0,
		s : null,
		data : null
	};
    data_url = 'assets/data/data.json';
    data:any = [
        {
            id : 100,
            name : '预定环节',
            score : '1%',
            previousMonth : '2%',
            previousYear : '3%',
            rankImg : 'assets/img/qs-yellow1.png',
            monthImg : 'assets/img/qs-up.png',
            yearImg : 'assets/img/qs-down.png',
            
        },
        {
            id : 200,
            name : '到达环节',
            score : '4%',
            previousMonth : '5%',
            previousYear : '6%',
            rankImg : 'assets/img/qs-yellow1.png',
            monthImg : 'assets/img/qs-up.png',
            yearImg : 'assets/img/qs-down.png'
        },
        {
            ComponentId : 300,
            name : '客房环节',
            score : '7%',
            previousMonth : '8%',
            previousYear : '9%',
            rankImg : 'assets/img/qs-yellow1.png',
            monthImg : 'assets/img/qs-up.png',
            yearImg : 'assets/img/qs-down.png',
            Children : [
                {
                    ComponentId : 300100,
                    name : '客房设备设施',
                    score : '10%',
                    previousMonth : '11%',
                    previousYear : '12%',
                    rankImg : 'assets/img/qs-yellow1.png',
                    monthImg : 'assets/img/qs-up.png',
                    yearImg : 'assets/img/qs-down.png',
                    Children : [
                        {
                            ComponentId : 300100100,
                            name : '设施1',
                            score : '13%',
                            previousMonth : '14%',
                            previousYear : '15%',
                            rankImg : 'assets/img/qs-yellow1.png',
                            monthImg : 'assets/img/qs-up.png',
                            yearImg : 'assets/img/qs-down.png'
                        },
                        {
                            ComponentId : 300100200,
                            name : '设施2',
                            score : '16%',
                            previousMonth : '17%',
                            previousYear : '18%',
                            rankImg : 'assets/img/qs-yellow1.png',
                            monthImg : 'assets/img/qs-up.png',
                            yearImg : 'assets/img/qs-down.png'
                        },
                        {
                            ComponentId : 300100300,
                            name : '设施3',
                            score : '19%',
                            previousMonth : '20%',
                            previousYear : '21%',
                            rankImg : 'assets/img/qs-yellow1.png',
                            monthImg : 'assets/img/qs-up.png',
                            yearImg : 'assets/img/qs-down.png'
                        }
                    ]
                },
                {
                    ComponentId : 300200,
                    name : '客房环节',
                    score : '22%',
                    previousMonth : '23%',
                    previousYear : '24%',
                    rankImg : 'assets/img/qs-yellow1.png',
                    monthImg : 'assets/img/qs-up.png',
                    yearImg : 'assets/img/qs-down.png'
                },
                {
                    ComponentId : 300300,
                    name : '客房卫生',
                    score : '25%',
                    previousMonth : '29%',
                    previousYear : '30%',
                    rankImg : 'assets/img/qs-yellow1.png',
                    monthImg : 'assets/img/qs-up.png',
                    yearImg : 'assets/img/qs-down.png'
                }
            ]
        }
    ];
getTrendAnalysisComponentTableData(param) {
    let copy_value = (s, t) => {
        if (!t) {
            t = {
                ComponentId : s.ComponentId
            };
            if (s.Children) {
                t.Children = [];
            }
        }
        t.ShowText = s.ShowText;
        t.HasChildren=s.HasChildren;
        t.Children=s.Children;
        t.Data=s.Data;
        t.className=s.className;
        // t.score = s.score;
        // t.previousMonth = s.previousMonth;
        // t.previousYear = s.previousYear;
        // t.rankImg = s.rankImg;
        // t.monthImg = s.monthImg;
        // t.yearImg = s.yearImg;
        return t;
    }
    if (param) {
        let fill = (t, a) => {
            let s = a.find((e) => {
                return e.ComponentId === t.ComponentId;
            });
            if(s.HasChildren){
                if(s.Children && s.Children.length){
                    s.className="anticon anticon-minus-square";
                }
                else{
                    s.className="anticon anticon-plus-square";
                }
            }
            copy_value(s, t);
            
            if (t.Children) {
                if (t.Children === 'open') {
                    t.Children = s.Children.map((e) => {
                        return copy_value(e, null);
                    });
                    // this.indicator.GetIndicators(this.local.get("id"),this.router.snapshot.params["id"],t.ComponentId).subscribe(data=>{
                    //    t.Children=data; 
                    // });
                } else {
                    let tc = t.Children, sc = s.Children;
                    for (let i = 0, l = tc.length; i < l; i++) {
                        fill(tc[i], sc);
                    }
                }
            }
        }
        for (let i = 0, l = param.length; i < l; i++) {
            fill(param[i], this.data);
        }
        return param;
    } else {
        let res = [];
        for (let i = 0, l = this.data.length; i < l; i++) {
            var s=this.data[i];
            if(s.HasChildren){
                if(s.Children && s.Children.length){
                    s.className="anticon anticon-minus-square";
                }
                else{
                    s.className="anticon anticon-plus-square";
                }
            }
            res.push(copy_value(s, null));
        }
        return res;
    }
}
}