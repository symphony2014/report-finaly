import { Component, Input, EventEmitter, Output, AfterViewInit, OnInit, ViewEncapsulation, ChangeDetectorRef, SimpleChange } from '@angular/core';
import { LocalStorageService, AlgorithmService, TaskService, IndicatorClassifyService } from "app/services/angular.service";
import { ResultDefaultData, FormatType, ComponentInfo, RequestParam, Result } from "app/services/models";
import { BehaviorSubject, Observable, Subject } from "rxjs/Rx";
import * as _ from "lodash";
import { Format, GlobalUtil } from "app/services/util.service";
import { FilterCompleteEvent, ComponentEvent, FilterStartEvent } from "app/theme/components/ipsosComponent/component.event";
import { ChartFactory } from "app/theme/components/ipsosComponent/chart.factory.service";
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { BaThemeSpinner } from 'app/theme/services';
import { TreeTableService } from 'app/theme/components/TreeTable/tree.table.service';

@Component({
  selector: 'TreeTable',
  templateUrl: './TreeTable.html',
  styleUrls: [
    './TreeTable.scss',
  ],
})
export class TreeTable implements OnInit  {
  @Input() inputStream:BehaviorSubject<ComponentEvent>;
  @Input() component:ComponentInfo;
  
  get config():any{
     var option=new Function("var  " +this.component.Schema +"\n  return option;");
     return option();
  }
	table:Object[];
  tableData = null;
  completedCount=0;
  reply:BehaviorSubject<FilterStartEvent>=new BehaviorSubject<FilterStartEvent>(null);
 status:BehaviorSubject<string>=new BehaviorSubject<string>("init");  // init : not visiable, loading:show the loading icon, data:show the data
 constructor(
              public taskService:TaskService,
              private router: Router,
              private activeRoute:ActivatedRoute,
              private chartFactory:ChartFactory,
              private algorithmService:AlgorithmService,
              private local:LocalStorageService,
              public _spinner: BaThemeSpinner,
              private treeTableService:TreeTableService,
              private globalUtil :GlobalUtil,
              private indicator:IndicatorClassifyService
        ) {

 }
 ngOnInit(){

  this.inputStream.asObservable().filter(e=>!!e && e instanceof FilterStartEvent)
                                 .do(_=>this.status.next("loading"))
                                 .do(_=>this.reply.next(_))
                                 .combineLatest(this.indicator.GetIndicators(this.local.get("id"),this.activeRoute.snapshot.params["id"],"0"))
                                 .do(com=>{
                                   this.status.next("data");
                                   this.completedCount=0;
                                   var data=com[1];
                                   this.treeTableService.data=data;
                                   this.tableData =this.treeTableService.getTrendAnalysisComponentTableData(null);
                                   this.toTable(); 
                                  
                                  this.tableData.forEach(node=>{
                                    let event=(com[0] as FilterStartEvent);
                                    event.filter.AlgorithmGroupId=node.ComponentId;
                                    this.algorithmService.GetResults(event.filter).subscribe(data=>{
                                      node.Data=data;
                                      var snode=this.treeTableService.data.find(snode=>snode.ComponentId==node.ComponentId);
                                      snode.Data=data; 
                                      this.completedCount+=100/this.tableData.length;
                                      this.toTable();
                                      //加载二级指标
                                      this.toggle_subitem(node.ComponentId);
                                    });
                                  })                                   
                                 })
                                 .subscribe();
                   
 }
 
 toTable() {
  let table = [];
  let convert = (d, leave) => {
    d.leave = (leave++) + 'em';
    table.push(d);
    let a = d.Children;
    if (d.HasChildren) {
     
      }
        if(a)
          for (let i = 0, l = a.length; i < l; i++) {
            convert(a[i], leave);
          }
  }
  for (let i = 0, l = this.tableData.length; i < l; i++) {
    convert(this.tableData[i], 0);
  }
  this.table = table;
}
getCurrent(data:any){
if(data){

  var value = (data.filter(d=>d.Text=="得分")[0].Series[0].Values[0]*100).toFixed(1)
  if(value.toString()=='NaN'){
    return "N/A";
  }
  else{
    return value;
  }
}
}
toggle_subitem(ComponentId) {
  let get_query_param = (s) => {
    let t = _.cloneDeep(s);
    if (s.HasChildren) {
      let sc = s.Children, c;
      if (s.ComponentId === ComponentId) {
        if (sc && sc.length) {
          t.Children = [];
        } else {
          t.Children = 'open'
        }
      } else {
        t.Children = [];
        if(sc && sc.length>0)
        {
          for (let i = 0, l = sc.length; i < l; i++) {
            c = get_query_param(sc[i]);
            t.Children.push(c);
          }
        }
      }
      
    }
    return t;
  }
    let param = [];
  for (let i = 0, l = this.tableData.length; i < l; i++) {
    param.push(get_query_param(this.tableData[i]));
  }
  this.indicator.GetIndicators(this.local.get("id"),this.activeRoute.snapshot.params["id"],ComponentId)
  .combineLatest(this.reply)
  .do(com=>{
      this.completedCount=0;
     this.fill(this.treeTableService.data,com[0],null,ComponentId); 
     
       let data = this.treeTableService.getTrendAnalysisComponentTableData(param);
       this.tableData = data;
       this.toTable();
     
  }).do(com=>{
    (com[0]  as any).forEach(child=>{
      let event=(com[1] as FilterStartEvent);
      event.filter.AlgorithmGroupId=child.ComponentId;
      this.algorithmService.GetResults(event.filter).subscribe(data=>{
        this.fill(this.treeTableService.data,null,data,child.ComponentId);
        this.completedCount+=100/(com[0] as any).length;
        this.toTable();
      });
    })
  }).subscribe(); 
  

}
public compare(v:any,key:string){
try {
  
  if(v.Data && v.Data.Datas){
    if((v.Data.Datas.length && v.Data.Datas[0].Series && v.Data.Datas[0].Series && v.Data.Datas[0].Series.length>0 )){
    
     //var current=v.Data.Datas[2].Series[0].Values[0];
     var current=v.Data.Datas.filter(d=>d.Text=="得分")[0].Series[0].Values[0];
     
           var last=v.Data.Datas.filter(d=>d.Text==key)[0].Series[0].Values[0];
           
           var sub=current-last;
    if(sub==0){
      return "equal";
    }
    else if(sub > 0)
    {
      return "up";
    }
    else if(sub < 0)
    {
      return "down";
    }
    else{
      return 'unknown'
    }
  }
  }
  else {
    return "unknown";
  } 
} catch (error) { 
  console.log(v);
}
}
public compareValue(v:any,key:string){
  try {
    
    if(v.Data && v.Data.Datas){
      if((v.Data.Datas.length && v.Data.Datas[0].Series && v.Data.Datas[0].Series && v.Data.Datas[0].Series.length>0 )){
      
      //var current=v.Data.Datas[2].Series[0].Values[0];
      var current=v.Data.Datas.filter(d=>d.Text=="得分")[0].Series[0].Values[0];

      var last=v.Data.Datas.filter(d=>d.Text==key)[0].Series[0].Values[0];
      
     // var sub=current-last;

      return (last*100).toFixed(1);
    }
    }
    else {
      return "unknown";
    } 
  } catch (error) { 
    console.log(v);
  }
  }
  private  fill(nodes:any[],children:any,data:any,ComponentId:string){
    if(nodes)
    nodes.forEach(node=>{
      if(node.ComponentId==ComponentId){
        //fill children
        if(children){

          if(node.Children && node.Children.length){
            node.Children=[];
          }
          else{
            node.Children=children;
          }
          }
          // fill data
          else{
             node.Data=data;
          }
         }
        else{
          this.fill(node.Children,children,data,ComponentId);
        }

    })
  } 
 
}