
import { Component, OnInit, Input } from '@angular/core';
import * as echarts from 'echarts';
import { ComponentInfo, ModuleType, RequestParam } from "app/services/models";
import { ActivatedRoute,Router,NavigationEnd } from "@angular/router";
import { ComponentService, LocalStorageService, BroadcastService, AlertService } from "app/services/angular.service";
import { PageBase } from "app/pages/page.base";
import {Observable,Subscription,BehaviorSubject } from "rxjs";
import { FilterStartEvent, ComponentEvent } from "app/theme/components/ipsosComponent/component.event";
import { GlobalUtil } from "app/services/util.service";
import { Http } from "@angular/http";

@Component({
  selector: 'AlertList', 
  templateUrl:'./AlertList.html',
  styleUrls:['./AlertList.scss']
})
export class AlertList   implements OnInit  {
 
      schema: Observable<any>;
      @Input() inputStream:BehaviorSubject<ComponentEvent>;
      status:BehaviorSubject<string>=new BehaviorSubject<string>("init");  // init : not visiable, loading:show the loading icon, data:show the data
        
     constructor(private http:Http,private util:GlobalUtil, private alertSerice:AlertService, public route:ActivatedRoute,public router:Router,public componentService:ComponentService,public local:LocalStorageService){
     }
        ngOnInit(): void {
        this.schema=this.inputStream.asObservable()
                    .filter(e=>!!e && e instanceof FilterStartEvent)
                    .do(_=>this.status.next("loading"))
                    .map((e:FilterStartEvent)=>this.util.appendGlobal(e).filter)
                    .switchMap(e=>this.alertSerice.GetAlertSituationForJRJHome(e))
                    .combineLatest(this.http.get('assets/data/data.json'))
                    .map(c=>{
                     this.status.next("data");
                    var schema=c[1].json();
                    var j=0;
                    var newArr=[c[0].All,c[0].New,c[0].Process,c[0].PastSevenDay,c[0].Complete];
                     for(var i in newArr) {
                      var item=schema.data.number[j++];
                      if(item){
                        item.num=newArr[i];
                      }
                     }
                     return schema;

                    });
       
       }
    }
