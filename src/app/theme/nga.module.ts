import { NgModule, ModuleWithProviders }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgUploaderModule } from 'ngx-uploader';
import '../../../node_modules/ng2-datetime/node_modules/bootstrap-datepicker/js/bootstrap-datepicker.js';
import { NKDatetimeModule } from 'ng2-datetime/ng2-datetime';
import {NzMenuModule, NzDropDownModule, NzDatePickerModule ,NzPaginationModule} from 'ng-zorro-antd';
import {  MdDatepickerModule, MdSelectModule, MdButtonModule, MdCheckboxModule, MdOptionModule, MdProgressSpinnerModule, MaterialModule, MdDialogModule, MdNativeDateModule } from '@angular/material';
import {GlobalUtil} from "../services/util.service";
import {
  BaThemeConfig
} from './theme.config';

import {
  BaThemeConfigProvider
} from './theme.configProvider';

import {
  BaBackTop,
  BaCard,
  BaContentTop,
  BaMenuItem,
  BaMenu,
  BaPageTop,
  BaTopMenu,
  BaSidebar,
  EChart,
  Filter,
  Table,
  SummaryHeader,
  FooterComponent,
  SimpleCompare,
  AlertList,
  IpsosComponent,
  Export,
  TreeTable,
} from './components';

import { BaCardBlur } from './components/baCard/baCardBlur.directive';

import {
  BaScrollPosition,
  BaSlimScroll,
  BaThemeRun
} from './directives';

import {
  BaAppPicturePipe,
  BaKameleonPicturePipe,
  BaProfilePicturePipe,
  BaFromPipe
} from './pipes';

import {
  BaImageLoaderService,
  BaMenuService,
  BaThemePreloader,
  BaThemeSpinner,
} from './services';

import {
  EmailValidator,
  EqualPasswordsValidator
} from './validators';
import { ComponentService, BroadcastService, AlertService, TaskService, IndicatorClassifyService } from "app/services/angular.service";
import { Html } from "app/theme/components/html";
import { Block } from "app/theme/components/block";
import { ChartFactory } from "app/theme/components/ipsosComponent/chart.factory.service";
import { PagingComponent } from "app/paging/paging.component";
import { ComplexTable } from "app/theme/components/ComplexTable";
import { TreeTableService } from 'app/theme/components/TreeTable/tree.table.service';
import { FromEventObservable } from '../../../node_modules/_rxjs@5.5.2@rxjs/observable/FromEventObservable';
const NGA_COMPONENTS = [
  BaBackTop,
  BaCard,
  BaContentTop,
  BaMenuItem,
  BaMenu,
  BaPageTop,
  BaTopMenu,
  BaSidebar,
  EChart,
  Filter,
  IpsosComponent,
  Html,
  SummaryHeader,
  SimpleCompare,
  AlertList,
  ComplexTable,
  Table,
  TreeTable,
  Export,
  Block,
  PagingComponent,
  FooterComponent,
];

const NGA_DIRECTIVES = [
  BaScrollPosition,
  BaSlimScroll,
  BaThemeRun,
  BaCardBlur
];

const NGA_PIPES = [
  BaAppPicturePipe,
  BaKameleonPicturePipe,
  BaProfilePicturePipe,
  BaFromPipe
];

const NGA_SERVICES = [
  BaImageLoaderService,
  BaThemePreloader,
  BaThemeSpinner,
  BaMenuService,
  ComponentService,
  BroadcastService,
  ChartFactory,
  GlobalUtil,
  AlertService,
  TaskService,
  TreeTableService,
  IndicatorClassifyService
];

const NGA_VALIDATORS = [
  EmailValidator,
  EqualPasswordsValidator
];

@NgModule({
  declarations: [
    ...NGA_PIPES,
    ...NGA_DIRECTIVES,
    ...NGA_COMPONENTS
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgUploaderModule,
    MdButtonModule,
    MdCheckboxModule,
    MdSelectModule,
    MdOptionModule,
    MdDatepickerModule,
    MdProgressSpinnerModule,
    MdNativeDateModule,
   NKDatetimeModule,
   NzDatePickerModule,
   NzPaginationModule,
   NzDropDownModule,
  NzMenuModule,
   MaterialModule,
  ],
  exports: [
    ...NGA_PIPES,
    ...NGA_DIRECTIVES,
    ...NGA_COMPONENTS
  ]
})
export class NgaModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders> {
      ngModule: NgaModule,
      providers: [
        BaThemeConfigProvider,
        BaThemeConfig,
        ...NGA_VALIDATORS,
        ...NGA_SERVICES
      ],
    };
  }
}
