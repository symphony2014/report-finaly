import {Pipe, PipeTransform} from '@angular/core';
import {layoutPaths} from '../../../theme';

@Pipe({name: 'from'})
export class BaFromPipe implements PipeTransform {

  transform(items:any[],index:number):any[] {
   return items.slice(index,items.length);
  }
}
