import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import {Login} from './login';
export const routes: Routes = [
   { path:'login',component:Login },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true });
