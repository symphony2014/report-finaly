import { ResultDefaultData } from "app/services/models";
import { MockBackend } from "@angular/http/testing";
import { ResponseOptions, Response } from "@angular/http";
import { Injectable } from "@angular/core";

@Injectable()
export class DBMock {
    constructor(private backend:MockBackend){}
     static testData:ResultDefaultData[]=[
       {
      Legend: ['xxx1', 'xxx2', 'xxx3'],
      AlgorithmId: 'x',
      Text: "城市",
      Series: [
        {
          Name: "xx1",
          Values: ["132", "22", "35"]
        },
        {
          Name: "xx2",
          Values: ["25", "30", "34"]
        },
        {
          Name: "xx3",
          Values: ["17", "32", "23"]
        }
      ],
      XAxis: ["xx1", "xx2", "xx3"]
    },
    {
      AlgorithmId: '0002',
      Text: "平均值",
      Legend: ["平均值"],
      Series: [
        {
          Name: "一月",
          Values: ["3"]
        },
        {
          Name: "二月",
          Values: ["3"]
        },
        {
          Name: "三月",
          Values: ["3"]
        }
      ]
    },
    {
      Legend: ['北京', '上海', '广州'],
      AlgorithmId: '0',
      Text: "城市",
      Series: [
        {
          Name: "一月",
          Values: ["132", "22", "35"]
        },
        {
          Name: "二月",
          Values: ["25", "30", "34"]
        },
        {
          Name: "三月",
          Values: ["17", "32", "23"]
        }
      ],
      XAxis: ["一月", "二月", "三月"]
    },
      {
      Legend: ['北京', '上海', '广州'],
      AlgorithmId: '0',
      Text: "城市",
      Series: [
        {
          Name: "一月",
          Values: ["132", "22", "35"]
        },
        {
          Name: "二月",
          Values: ["25", "30", "34"]
        },
        {
          Name: "三月",
          Values: ["17", "32", "23"]
        }
      ],
      XAxis: ["一月", "二月", "三月"]
    }
    ];

    

}