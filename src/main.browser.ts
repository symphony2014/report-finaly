/*
 * Angular bootstraping
 */
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { decorateModuleRef } from './app/environment';
import { bootloader } from '@angularclass/hmr';
import {enableProdMode} from '@angular/core';
/*
 * App Module
 * our top level module that holds all of our components
 */
import { AppModuleShared } from './app';

/*
 * Bootstrap our Angular app with a top level NgModule
 */
export function main(): Promise<any> {
  
  return platformBrowserDynamic()
    .bootstrapModule(AppModuleShared)
    .then(decorateModuleRef)
    .catch(err => console.error(err));

}

bootloader(main);
