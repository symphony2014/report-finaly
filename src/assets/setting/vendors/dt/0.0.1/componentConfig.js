/**
 * @file component默认配置
 * @author sushuang(sushuang@baidu.com)
 */

define_esl(function (require_esl) {

    var Component = require_esl('./ui/Component');

    // 可在tpl中使用的types。
    // 放在这里声明是为了模块加载。
    var cptClasses = Component.cptClasses;

    // common component
    cptClasses['Text'] = require_esl('./ui/Text');
    cptClasses['TextInput'] = require_esl('./ui/TextInput');
    cptClasses['CheckButton'] = require_esl('./ui/CheckButton');
    cptClasses['TreeList'] = require_esl('./ui/TreeList');
    cptClasses['WinPanel'] = require_esl('./ui/WinPanel');
    cptClasses['Button'] = require_esl('./ui/Button');
    cptClasses['Tab'] = require_esl('./ui/Tab');
    cptClasses['Foreach'] = require_esl('./ui/Foreach');
});
